package mms.dweb.base;

/**
 * Created by mormukutsinghji@gmail.com on 3/14/2018.
 */

public interface MainView {

    void initializeView();

    void showProgressBar();

    void hideProgressBar();
}

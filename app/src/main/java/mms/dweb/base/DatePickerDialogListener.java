package mms.dweb.base;

import android.widget.DatePicker;

/**
 * Created by mormukutsinghji@gmail.com on 3/14/2018.
 */

public interface DatePickerDialogListener {
    void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth);
}

package mms.dweb.base;

/**
 * Created by Mormukut singh R@J@W@T on 6/28/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public interface OnTaskCompletedListener<T> {

    void onTaskCompleted(T object);
}

package mms.dweb.base;

/**
 * Created by mukut on 27-04-2018.
 */
public interface OnTwoSpinnerSelectListener<T> {
    void onItemClick(T object, int position, int cat, int tag);
}

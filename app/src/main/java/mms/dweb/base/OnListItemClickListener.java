package mms.dweb.base;

/**
 * Created by mukut on 27-04-2018.
 */
public interface OnListItemClickListener<T> {
    void onItemClick(T object, int position);
}

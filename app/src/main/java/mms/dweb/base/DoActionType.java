package mms.dweb.base;

/**
 * Created by mormukutsinghji@gmail.com on 3/14/2018.
 */

public enum DoActionType {
    Camera,
    Gellery,
    Cancel,
    Ok,
    Submit,
    Retry,
    Save,
    Edit,
    Cart,
    CartEdit,
    Default,
    Horizontal,
    Vertical,
    Remove,
    ADD,
    SUB,
    Whishlist,
    Scroll;
}

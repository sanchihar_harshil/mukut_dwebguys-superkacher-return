package mms.dweb.base;

/**
 * Created by Mormukut singh R@J@W@T on 6/15/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public interface OnListItemClickWithTypeListener<T> {

    void onItemClick(T object, int position, DoActionType type);

}

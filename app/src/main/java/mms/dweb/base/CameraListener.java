package mms.dweb.base;

/**
 * Created by Mormukut singh R@J@W@T on 18/8/17.
 * Company Name: DecipherZoneSoftwares
 * URL: www.decipherzone.com
 */
public interface CameraListener {

    void onCameraSelect();
    void onGallerySelect();
}

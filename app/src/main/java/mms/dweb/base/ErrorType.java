package mms.dweb.base;

/**
 * Created by mormukutsinghji@gmail.com on 3/14/2018.
 */

public enum ErrorType {

    ERROR,
    ERROR500,
    NO_INTERNET,
    PARSING_ERROR,
    RequiredText,
    NotValidText

}

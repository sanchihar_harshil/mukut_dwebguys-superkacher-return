package mms.dweb.web_services;

import android.widget.Toast;

import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.utilities.Systemout;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.listener.PostResponseListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public class ApiListener {
    BaseAppCompatActivity activity;

    private static final ApiListener ourInstance = new ApiListener();

    public static ApiListener getInstance() {
        return ourInstance;
    }

    private ApiListener() {
    }

    public void onPostApiCall(BaseAppCompatActivity activity, Call<Object> call, boolean showDialog, PostResponseListener callBack) {
        this.activity = activity;
        if (!isOnline()) {
            callBack.onSuccess(false, null, null, ErrorType.NO_INTERNET, null);
            return;
        }

        if (showDialog) {
            progressON(null);
        }
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Systemout.println("-------------------------successStringListener-------------------------------");
                Systemout.println("---------" + response);
                Systemout.println("----------------------------successStringListener----------------------------");
                progressOFF();
                callBack.onSuccess(true, response, null, null, null);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                progressOFF();
                callBack.onSuccess(true, null, t, ErrorType.ERROR, null);
            }
        });
    }


    // It will check us the device is online, before calling service, and will respond accordingly.
    private boolean isOnline() {
        if (!Utilities.getInstance().isOnline(activity)) {
            showNoInternetConnection();
            return false;
        }
        return true;
    }


    public void progressON() {
        ApplicationContext.getInstance().progressON(activity, null);
    }

    public void progressON(String message) {
        ApplicationContext.getInstance().progressON(activity, message);
    }

    public void progressOFF() {
        ApplicationContext.getInstance().progressOFF();
    }

    private void showNoInternetConnection() {
        Toast.makeText(activity, activity.getString(R.string.noInternetAccess), Toast.LENGTH_SHORT).show();
    }
}

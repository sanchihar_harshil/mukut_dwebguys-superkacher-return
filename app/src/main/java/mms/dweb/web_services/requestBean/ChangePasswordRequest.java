package mms.dweb.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mormukut singh R@J@W@T on 6/27/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class ChangePasswordRequest {


    @Expose
    @SerializedName("old_password")
    private String oldpassword;
    @Expose
    @SerializedName("new_password")
    private String newpassword;

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }
}

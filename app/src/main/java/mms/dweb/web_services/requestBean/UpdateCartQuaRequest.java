package mms.dweb.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 6/27/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class UpdateCartQuaRequest {


    @Expose
    @SerializedName("update_cart")
    private List<Updatecart> updatecart;

    public List<Updatecart> getUpdatecart() {
        return updatecart;
    }

    public void setUpdatecart(List<Updatecart> updatecart) {
        this.updatecart = updatecart;
    }

    public static class Updatecart {
        @Expose
        @SerializedName("quantity")
        private String quantity;
        @Expose
        @SerializedName("id")
        private String id;

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}

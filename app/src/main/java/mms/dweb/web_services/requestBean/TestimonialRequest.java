package mms.dweb.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.MultipartBody;

/**
 * Created by Admin on 7/4/2018.
 */

public class TestimonialRequest {

    @Expose
    @SerializedName("admin")
    private String admin;

    @Expose
    @SerializedName("user_name")
    private String user_name;

    @Expose
    @SerializedName("image")
    private MultipartBody.Part image;

    @Expose
    @SerializedName("recv_email")
    private String recv_email;

    @Expose
    @SerializedName("title")
    private String title;

    @Expose
    @SerializedName("desc")
    private String desc;

    @Expose
    @SerializedName("rating")
    private String rating;

    public MultipartBody.Part getImage() {
        return image;
    }

    public void setImage(MultipartBody.Part image) {
        this.image = image;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getRecv_email() {
        return recv_email;
    }

    public void setRecv_email(String recv_email) {
        this.recv_email = recv_email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }


}

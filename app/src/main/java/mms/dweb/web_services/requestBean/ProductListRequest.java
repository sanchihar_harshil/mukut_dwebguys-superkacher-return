package mms.dweb.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mormukut singh R@J@W@T on 6/14/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class ProductListRequest {


    @Expose
    @SerializedName("orderby")
    private String orderby;
    @Expose
    @SerializedName("min_price")
    private String min_price;
    @Expose
    @SerializedName("max_price")
    private String max_price;
    @Expose
    @SerializedName("search")
    private String search;
    @Expose
    @SerializedName("tag")
    private String tag;
    @Expose
    @SerializedName("category_id")
    private String category_id;
    @Expose
    @SerializedName("page")
    private String page;

    public String getOrderby() {
        return orderby;
    }

    public void setOrderby(String orderby) {
        this.orderby = orderby;
    }

    public String getMin_price() {
        return min_price;
    }

    public void setMin_price(String min_price) {
        this.min_price = min_price;
    }

    public String getMax_price() {
        return max_price;
    }

    public void setMax_price(String max_price) {
        this.max_price = max_price;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}

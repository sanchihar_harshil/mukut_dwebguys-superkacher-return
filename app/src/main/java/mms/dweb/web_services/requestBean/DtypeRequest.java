package mms.dweb.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 7/4/2018.
 */

public class DtypeRequest {


    @Expose
    @SerializedName("d_type")
    private String d_type;

    public String getD_type() {
        return d_type;
    }

    public void setD_type(String d_type) {
        this.d_type = d_type;
    }
}

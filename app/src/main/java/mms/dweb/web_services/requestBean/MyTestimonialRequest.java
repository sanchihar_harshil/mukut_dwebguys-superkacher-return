package mms.dweb.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 7/6/2018.
 */

public class MyTestimonialRequest {


    @Expose
    @SerializedName("recv_email")
    private String recvEmail;

    public String getRecvEmail() {
        return recvEmail;
    }

    public void setRecvEmail(String recvEmail) {
        this.recvEmail = recvEmail;
    }
}

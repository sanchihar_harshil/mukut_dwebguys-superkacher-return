package mms.dweb.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.MultipartBody;

public class UploadImageProfile {
    @Expose
    @SerializedName("profile_pic")
    private MultipartBody.Part profilePic;

    public MultipartBody.Part getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(MultipartBody.Part profilePic) {
        this.profilePic = profilePic;
    }
}

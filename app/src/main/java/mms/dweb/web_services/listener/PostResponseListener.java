package mms.dweb.web_services.listener;

import mms.dweb.base.ErrorType;
import retrofit2.Response;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public interface PostResponseListener {
    void onSuccess(boolean isSuccess, Response<Object> response, Throwable t, ErrorType errorType, Object o);
}

package mms.dweb.web_services.listener;

import mms.dweb.base.ErrorType;
import mms.dweb.web_services.responseBean.LoginResponse;
import retrofit2.Response;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public interface LoginListener {
    void onSuccess(boolean isSuccess, Response<LoginResponse> response, Throwable t, ErrorType errorType);
}

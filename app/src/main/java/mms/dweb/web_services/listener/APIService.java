package mms.dweb.web_services.listener;

import mms.dweb.web_services.RetrofitBuilder;
import mms.dweb.web_services.requestBean.AddToCartRequest;
import mms.dweb.web_services.requestBean.AddToCartWithOptionRequest;
import mms.dweb.web_services.requestBean.AddWishlistProductRequest;
import mms.dweb.web_services.requestBean.BrandsListRequest;
import mms.dweb.web_services.requestBean.ChangePasswordRequest;
import mms.dweb.web_services.requestBean.ContactUsRequest;
import mms.dweb.web_services.requestBean.CouponRequest;
import mms.dweb.web_services.requestBean.CreateOrderRequest;
import mms.dweb.web_services.requestBean.DtypeRequest;
import mms.dweb.web_services.requestBean.EditProfileRequest;
import mms.dweb.web_services.requestBean.FacebookLoginRequest;
import mms.dweb.web_services.requestBean.GoogleLoginRequest;
import mms.dweb.web_services.requestBean.LoginRequest;
import mms.dweb.web_services.requestBean.MyOrdersRequest;
import mms.dweb.web_services.requestBean.OrderDetailsRequest;
import mms.dweb.web_services.requestBean.PasswordResetRequest;
import mms.dweb.web_services.requestBean.ProductDetailsRequest;
import mms.dweb.web_services.requestBean.ProductListRequest;
import mms.dweb.web_services.requestBean.RegistrationRequest;
import mms.dweb.web_services.requestBean.RemoveItemRequest;
import mms.dweb.web_services.requestBean.StripePaymentRequest;
import mms.dweb.web_services.requestBean.TestimonialRequest;
import mms.dweb.web_services.requestBean.UpdateCartQuaRequest;
import mms.dweb.web_services.requestBean.UpsShippingRequest;
import mms.dweb.web_services.responseBean.CartInfoResponse;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public interface APIService {

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.LOGIN_URL)
    Call<Object> postUserLogin(@Body LoginRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.GOOGLE_LOGIN_URL)
    Call<Object> postGoogleLogin(@Body GoogleLoginRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.FACEBOOK_LOGIN_URL)
    Call<Object> postFacebookLogin(@Body FacebookLoginRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.REGISTRATION_URL)
    Call<Object> postUserRegistration(@Body RegistrationRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.CATEGORIES_URL)
    Call<Object> postCategories();

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.PRODUCT_LIST_URL)
    Call<Object> postProductList(@Body ProductListRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.ALL_BRANDS_LIST_URL)
    Call<Object> postBrandsList(@Body BrandsListRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.HOLIDAYS_LIST_URL)
    Call<Object> postHolidaysList();

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.SLIDER_URL)
    Call<Object> postSlider();

    @POST(RetrofitBuilder.ADD_TO_CART_URL)
    Call<Object> postAddToCart(@Body AddToCartRequest bean);

    @POST(RetrofitBuilder.ADD_TO_CART_OPTION_URL)
    Call<Object> postAddToCartWithOption(@Body AddToCartWithOptionRequest bean);

    @POST(RetrofitBuilder.CART_URL)
    Call<Object> postCart();

    @POST(RetrofitBuilder.REMOVE_ITEM_URL)
    Call<Object> postRemoveProductFromCart(@Body RemoveItemRequest bean);

    @POST(RetrofitBuilder.CHECKOUT_URL)
    Call<Object> postCheckoutInfo();

    @POST(RetrofitBuilder.UPS_SHIPPING_URL)
    Call<Object> postUpsShipping(@Body UpsShippingRequest bean);

    @POST(RetrofitBuilder.COUPON_URL)
    Call<Object> postCoupon(@Body CouponRequest bean);

    @POST(RetrofitBuilder.ADD_WISHLIST_PRODUCT_URL)
    Call<Object> postAddWishlist(@Body AddWishlistProductRequest bean);

    @POST(RetrofitBuilder.DELETE_WISHLIST_PRODUCT_URL)
    Call<Object> postDeleteWishlist(@Body AddWishlistProductRequest bean);

    @POST(RetrofitBuilder.WISHLIST_PRODUCT_URL)
    Call<Object> postWishlist();

    @POST(RetrofitBuilder.SHORTING_PRODUCT_URL)
    Call<Object> postShortingProduct();

    @POST(RetrofitBuilder.PRODUCT_DETAILS_URL)
    Call<Object> postProductDetails(@Body ProductDetailsRequest bean);

    @POST(RetrofitBuilder.RELATED_PRODUCT_URL)
    Call<Object> postRelatedProduct(@Body ProductDetailsRequest bean);

    @POST(RetrofitBuilder.MY_ORDERS_URL)
    Call<Object> postMyOrders(@Body MyOrdersRequest bean);

    @POST(RetrofitBuilder.ORDER_DETAILS_URL)
    Call<Object> postOrderDetails(@Body OrderDetailsRequest bean);

    @POST(RetrofitBuilder.UPDATE_CART_QUENTITY_URL)
    Call<Object> postUpdateCartQuantity(@Body UpdateCartQuaRequest bean);

    @POST(RetrofitBuilder.CHANGE_PASSWORD_URL)
    Call<Object> postChangePassword(@Body ChangePasswordRequest bean);

    @POST(RetrofitBuilder.EDIT_PROFILE_URL)
    Call<Object> postEditProfile(@Body EditProfileRequest bean);

    @Multipart
    @POST(RetrofitBuilder.PROFILE_IMAGE_UPLOAD)
    Call<Object> postUpdateProfilePic(@Part MultipartBody.Part vFile);

    @Headers({"Content-Type: application/json;charset=UTF-8", "SECRET: 3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4"})
    @POST(RetrofitBuilder.RESET_PASSWORD_URL)
    Call<Object> postPasswordReset(@Body PasswordResetRequest bean);

    @POST(RetrofitBuilder.SHIPPING_POLICY)
    Call<Object> postShippingPolicy(@Body DtypeRequest bean);

    @POST(RetrofitBuilder.PRIVACY_POLICY)
    Call<Object> postPrivacyPolicy(@Body DtypeRequest bean);

    @POST(RetrofitBuilder.SECURE_PAYMENT)
    Call<Object> postSecurePayment(@Body DtypeRequest bean);

    @POST(RetrofitBuilder.GET_TERMS)
    Call<Object> postTerms(@Body DtypeRequest bean);

    @POST(RetrofitBuilder.ABOUT_US)
    Call<Object> postAboutUs(@Body DtypeRequest bean);

    @POST(RetrofitBuilder.CONTACT_US)
    Call<Object> postContactUs(@Body ContactUsRequest bean);

    @POST(RetrofitBuilder.TESTIMONIAL_URL)
    Call<Object> postUpdateTestApiCall(@Body TestimonialRequest bean);

    @POST(RetrofitBuilder.CREATE_URL)
    Call<Object> postCreateOrder(@Body CreateOrderRequest bean);

    @POST(RetrofitBuilder.CREATE_EPHEMERAL_URL)
    Call<Object> postCreateEphemeral();

    @POST(RetrofitBuilder.STRIPE_PAYMENT_URL)
    Call<Object> postStripePayment(@Body StripePaymentRequest bean);


//    @Multipart
//    @POST(RetrofitBuilder.TESTIMONIAL_URL)
//    Call<Object> postUpdateTestApiCall(@Part("recv_email") String recv_email);

    @FormUrlEncoded
    @POST(RetrofitBuilder.TESTIMONIAL_URL)
    Call<Object> postUpdateTestApiCall(
            @Field("admin") String admin,
            @Field("username") String user_name,
            @Field("recvemail") String recv_email,
            @Field("title") String title,
            @Field("desc") String desc,
            @Field("rating") String rating,
            @Field("image") MultipartBody.Part image);


   /*@Multipart
    @POST(RetrofitBuilder.TESTIMONIAL_URL)
    Call<Object> postUpdateTestApiCall(
            @Part("admin") String admin,
            @Part("username") String user_name,
            @Part("recvemail") String recv_email,
            @Part("title") String title,
            @Part("desc") String desc,
            @Part("rating") String rating,
            @Part MultipartBody.Part image);
*/

}

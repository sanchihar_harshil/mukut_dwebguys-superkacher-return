package mms.dweb.web_services;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.web_services.listener.APIService;
import mms.dweb.web_services.listener.PostResponseListener;
import mms.dweb.web_services.requestBean.AddToCartRequest;
import mms.dweb.web_services.requestBean.AddToCartWithOptionRequest;
import mms.dweb.web_services.requestBean.AddWishlistProductRequest;
import mms.dweb.web_services.requestBean.BrandsListRequest;
import mms.dweb.web_services.requestBean.ChangePasswordRequest;
import mms.dweb.web_services.requestBean.ContactUsRequest;
import mms.dweb.web_services.requestBean.CouponRequest;
import mms.dweb.web_services.requestBean.CreateOrderRequest;
import mms.dweb.web_services.requestBean.DtypeRequest;
import mms.dweb.web_services.requestBean.EditProfileRequest;
import mms.dweb.web_services.requestBean.FacebookLoginRequest;
import mms.dweb.web_services.requestBean.GoogleLoginRequest;
import mms.dweb.web_services.requestBean.LoginRequest;
import mms.dweb.web_services.requestBean.MyOrdersRequest;
import mms.dweb.web_services.requestBean.OrderDetailsRequest;
import mms.dweb.web_services.requestBean.PasswordResetRequest;
import mms.dweb.web_services.requestBean.ProductDetailsRequest;
import mms.dweb.web_services.requestBean.ProductListRequest;
import mms.dweb.web_services.requestBean.RegistrationRequest;
import mms.dweb.web_services.requestBean.RemoveItemRequest;
import mms.dweb.web_services.requestBean.StripePaymentRequest;
import mms.dweb.web_services.requestBean.TestimonialRequest;
import mms.dweb.web_services.requestBean.UpdateCartQuaRequest;
import mms.dweb.web_services.requestBean.UploadImageProfile;
import mms.dweb.web_services.requestBean.UpsShippingRequest;
import mms.dweb.web_services.responseBean.AboutUsResponse;
import mms.dweb.web_services.responseBean.ChangePasswordResponse;
import mms.dweb.web_services.responseBean.ContactusResponse;
import mms.dweb.web_services.responseBean.CreateOrderResponse;
import mms.dweb.web_services.responseBean.EphemeralKeyResponse;
import mms.dweb.web_services.responseBean.PasswordResetResponse;
import mms.dweb.web_services.responseBean.PrivacyPolicyResponse;
import mms.dweb.web_services.responseBean.ProfileImageResponce;
import mms.dweb.web_services.responseBean.SecurePaymentResponse;
import mms.dweb.web_services.responseBean.ShippingPolicyResponse;
import mms.dweb.web_services.responseBean.AddToCartResponce;
import mms.dweb.web_services.responseBean.AddWishlistProductResponse;
import mms.dweb.web_services.responseBean.BrandsListResponse;
import mms.dweb.web_services.responseBean.CartInfoResponse;
import mms.dweb.web_services.responseBean.CategoriesListResponse;
import mms.dweb.web_services.responseBean.CheckoutInfoResponse;
import mms.dweb.web_services.responseBean.CouponResponse;
import mms.dweb.web_services.responseBean.HolidaysListResponse;
import mms.dweb.web_services.responseBean.LoginResponse;
import mms.dweb.web_services.responseBean.MyOrdersResponse;
import mms.dweb.web_services.responseBean.OrderDetailsResponse;
import mms.dweb.web_services.responseBean.ProductDetailsResponse;
import mms.dweb.web_services.responseBean.ProductListResponse;
import mms.dweb.web_services.responseBean.RelatedProductResponse;
import mms.dweb.web_services.responseBean.RemoveItemResponse;
import mms.dweb.web_services.responseBean.ShortingProductResponse;
import mms.dweb.web_services.responseBean.SliderResponse;
import mms.dweb.web_services.responseBean.StripePaymentResponse;
import mms.dweb.web_services.responseBean.TermsResponse;
import mms.dweb.web_services.responseBean.TestimonialResponse;
import mms.dweb.web_services.responseBean.UpdateCartQuaResponse;
import mms.dweb.web_services.responseBean.UpsShippingResponse;
import mms.dweb.web_services.responseBean.WishlistResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public class ApiCall {

    APIService service;


    private static final ApiCall ourInstance = new ApiCall();

    public static ApiCall getInstance() {
        return ourInstance;
    }

    private ApiCall() {
    }

    private void setBasicInfo(int connectionTimeOut, int readTimeOut) {
        Log.e(ApplicationContext.TAG, "click");
        service = RetrofitBuilder.getInstance().getRetrofitInstance(connectionTimeOut, readTimeOut).create(APIService.class);
    }

    private void setBasicInfoAndId(int connectionTimeOut, int readTimeOut) {
        Log.e(ApplicationContext.TAG, "click");
        service = RetrofitBuilder.getInstance().getRetrofitGetID(connectionTimeOut, readTimeOut,
                PrefSetup.getInstance().getUserInfo() != null ?
                        PrefSetup.getInstance().getUserInfo().getResponsedata().getId() : "0").create(APIService.class);
    }

    private void setMultipleBasicInfoAndId(int connectionTimeOut, int readTimeOut) {
        Log.e(ApplicationContext.TAG, "click");
        service = RetrofitBuilder.getInstance().getMultipleRetrofitGetID(connectionTimeOut, readTimeOut,
                PrefSetup.getInstance().getUserInfo() != null ?
                        PrefSetup.getInstance().getUserInfo().getResponsedata().getId() : "0").create(APIService.class);
    }


    public void postUserLoginApiCall(BaseAppCompatActivity activity, LoginRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);
        Call<Object> call = service.postUserLogin(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    LoginResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), LoginResponse.class);
                    if (user != null && user.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        PrefSetup.getInstance().setUserInfo(user);
                        callBack.onSuccess(true, response, t, errorType, user);
                    } else if (user != null && user.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                user.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postUserLoginApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postGoogleLoginApiCall(BaseAppCompatActivity activity, GoogleLoginRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);
        Call<Object> call = service.postGoogleLogin(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    LoginResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), LoginResponse.class);
                    if (user != null && user.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        PrefSetup.getInstance().setUserInfo(user);
                        callBack.onSuccess(true, response, t, errorType, user);
                    } else if (user != null && user.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                user.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postGoogleLoginApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postFacebookLoginApiCall(BaseAppCompatActivity activity, FacebookLoginRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);
        Call<Object> call = service.postFacebookLogin(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    LoginResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), LoginResponse.class);
                    if (user != null && user.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        PrefSetup.getInstance().setUserInfo(user);
                        callBack.onSuccess(true, response, t, errorType, user);
                    } else if (user != null && user.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                user.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postFacebookLoginApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postEditProfileApiCall(BaseAppCompatActivity activity, EditProfileRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postEditProfile(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    LoginResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), LoginResponse.class);
                    if (user != null && user.getResponsecode() == 1) {
                        user.getResponsedata().setFirst_name(jsonObject1.get("body").getAsJsonObject().get("responsedata").getAsJsonObject().get("firstname").getAsString());
                        user.getResponsedata().setLast_name(jsonObject1.get("body").getAsJsonObject().get("responsedata").getAsJsonObject().get("lastname").getAsString());
                        user.getResponsedata().setUser_name(jsonObject1.get("body").getAsJsonObject().get("responsedata").getAsJsonObject().get("username").getAsString());
                        Log.e(ApplicationContext.TAG, user.toString());
                        PrefSetup.getInstance().setUserInfo(user);
                        callBack.onSuccess(true, response, t, errorType, user);
                    } else if (user != null && user.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                user.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postEditProfileApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


    public void postaddProfileImage(BaseAppCompatActivity activity, UploadImageProfile iFile, boolean showDialog, PostResponseListener callBack) {
        setMultipleBasicInfoAndId(1000, 1000);
        Call<Object> call = service.postUpdateProfilePic(iFile.getProfilePic());
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    ProfileImageResponce addToCartResponce = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), ProfileImageResponce.class);
                    if (addToCartResponce != null && addToCartResponce.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, addToCartResponce.toString());
                        LoginResponse userInfo = PrefSetup.getInstance().getUserInfo();
                        LoginResponse.Responsedata userReq = PrefSetup.getInstance().getUserInfo().getResponsedata();
                        userReq.setProfile_pic(addToCartResponce.getResponsedata().getProfilePic());
                        userInfo.setResponsedata(userReq);
                        PrefSetup.getInstance().setUserInfo(userInfo);
                        callBack.onSuccess(true, response, t, errorType, addToCartResponce);
                    } else if (addToCartResponce != null && addToCartResponce.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                addToCartResponce.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postaddProfileImage(activity, iFile, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


    public void postTestApiCall(BaseAppCompatActivity activity, TestimonialRequest iFile, boolean showDialog, PostResponseListener callBack) {
        setMultipleBasicInfoAndId(2000, 2000);
        Call<Object> call = service.postUpdateTestApiCall(iFile.getAdmin(), iFile.getUser_name(), iFile.getRecv_email(), iFile.getTitle(), iFile.getDesc(), iFile.getRating(), iFile.getImage());
        // Call<Object> call = service.postUpdateTestApiCall(iFile.getRecv_email());
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    TestimonialResponse addToCartResponce = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), TestimonialResponse.class);
                    if (addToCartResponce != null && addToCartResponce.getResponsecode() == 1) {
                        callBack.onSuccess(true, response, t, errorType, addToCartResponce);
                    } else if (addToCartResponce != null && addToCartResponce.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.testimonial),
                                addToCartResponce.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.testimonial),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.testimonial),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postTestApiCall(activity, iFile, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.testimonial),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


    public void postUserRegistrationApiCall(BaseAppCompatActivity activity, RegistrationRequest bean, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(200, 200);
//        Gson gson = new Gson();
//        String json = gson.toJson(bean);
        Call<Object> call = service.postUserRegistration(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    LoginResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), LoginResponse.class);
                    if (user != null && user.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        PrefSetup.getInstance().setUserInfo(user);
                        callBack.onSuccess(true, response, t, errorType, user);
                    } else if (user != null && user.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                user.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postUserRegistrationApiCall(activity, bean, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


    public void postChangePasswordApiCall(BaseAppCompatActivity activity, ChangePasswordRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postChangePassword(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    ChangePasswordResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), ChangePasswordResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(true, response, t, errorType, body);
                                });
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postChangePasswordApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


    public void postPasswordResetApiCall(BaseAppCompatActivity activity, PasswordResetRequest bean, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(200, 200);
//        Gson gson = new Gson();
//        String json = gson.toJson(bean);
        Call<Object> call = service.postPasswordReset(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    PasswordResetResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), PasswordResetResponse.class);
                    if (user != null && user.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        callBack.onSuccess(true, response, t, errorType, user);
                    } else if (user != null && user.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.passwordReset),
                                user.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.passwordReset),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postPasswordResetApiCall(activity, bean, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


    public void postUpdateCartQuantityApiCall(BaseAppCompatActivity activity, UpdateCartQuaRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postUpdateCartQuantity(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    UpdateCartQuaResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), UpdateCartQuaResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postUpdateCartQuantityApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postCategoriesApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(200, 200);
//        Gson gson = new Gson();
//        String json = gson.toJson(bean);
        Call<Object> call = service.postCategories();
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    CategoriesListResponse categoriesListResponse = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), CategoriesListResponse.class);
                    if (categoriesListResponse != null && categoriesListResponse.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, categoriesListResponse.toString());
                        callBack.onSuccess(true, response, t, errorType, categoriesListResponse);
                    } else if (categoriesListResponse != null && categoriesListResponse.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                categoriesListResponse.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCategoriesApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postProductApiCall(BaseAppCompatActivity activity, ProductListRequest bean, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(200, 200);
//        bean.setOrderby(PrefSetup.getInstance().getUserInfo() != null
//                ? PrefSetup.getInstance().getUserInfo().getResponsedata().getId() : "0");
        Call<Object> call = service.postProductList(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    ProductListResponse productListResponse = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), ProductListResponse.class);
                    if (productListResponse != null && productListResponse.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, productListResponse.toString());
                        callBack.onSuccess(true, response, t, errorType, productListResponse);
                    } else if (productListResponse != null && productListResponse.getResponsecode() == 0) {
//                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
//                                productListResponse.getResponsemessage(), (object, position) -> {
                        callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
//                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postProductApiCall(activity, bean, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postBrandsListApiCall(BaseAppCompatActivity activity, BrandsListRequest bean, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(200, 200);
        Call<Object> call = service.postBrandsList(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    BrandsListResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), BrandsListResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postBrandsListApiCall(activity, bean, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postHolidaysListApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(200, 200);
        Call<Object> call = service.postHolidaysList();
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    HolidaysListResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), HolidaysListResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postHolidaysListApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postSliderApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(200, 200);
        Call<Object> call = service.postSlider();
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    SliderResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), SliderResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postSliderApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postAddToCartApiCall(BaseAppCompatActivity activity, AddToCartRequest bean, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postAddToCart(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    AddToCartResponce addToCartResponce = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), AddToCartResponce.class);
                    if (addToCartResponce != null && addToCartResponce.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, addToCartResponce.toString());
                        callBack.onSuccess(true, response, t, errorType, addToCartResponce);
                    } else if (addToCartResponce != null && addToCartResponce.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                addToCartResponce.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCategoriesApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postAddToCartWithOptionApiCall(BaseAppCompatActivity activity, AddToCartWithOptionRequest bean, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postAddToCartWithOption(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    AddToCartResponce addToCartResponce = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), AddToCartResponce.class);
                    if (addToCartResponce != null && addToCartResponce.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, addToCartResponce.toString());
                        callBack.onSuccess(true, response, t, errorType, addToCartResponce);
                    } else if (addToCartResponce != null && addToCartResponce.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                addToCartResponce.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCategoriesApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postCartApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postCart();
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    CartInfoResponse cartInfoResponse = new Gson()
                            .fromJson(jsonObject1.getAsJsonObject("body").toString(), CartInfoResponse.class);
                    if (cartInfoResponse != null && cartInfoResponse.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, cartInfoResponse.toString());
                        callBack.onSuccess(true, response, t, errorType, cartInfoResponse);
                    } else if (cartInfoResponse != null && cartInfoResponse.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                cartInfoResponse.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCategoriesApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postCartCountApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postCart();
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    CartInfoResponse cartInfoResponse = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), CartInfoResponse.class);
                    if (cartInfoResponse != null && cartInfoResponse.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, cartInfoResponse.toString());
                        callBack.onSuccess(true, response, t, errorType, cartInfoResponse);
                    }else {
                        callBack.onSuccess(false, response, t, errorType, cartInfoResponse);
                    }
                } catch (Exception e) {
                    callBack.onSuccess(false, response, t, errorType, null);
                }
            }
        });
    }

    public void postRemoveProductFromCartApiCall(BaseAppCompatActivity activity, RemoveItemRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postRemoveProductFromCart(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    RemoveItemResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), RemoveItemResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCategoriesApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postCheckoutInfoApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postCheckoutInfo();
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    CheckoutInfoResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), CheckoutInfoResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCategoriesApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postUpsShippingApiCall(BaseAppCompatActivity activity, UpsShippingRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postUpsShipping(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    UpsShippingResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), UpsShippingResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCategoriesApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postCouponApiCall(BaseAppCompatActivity activity, CouponRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postCoupon(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    CouponResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), CouponResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCouponApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postAddWishlistProductApiCall(BaseAppCompatActivity activity, AddWishlistProductRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postAddWishlist(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    AddWishlistProductResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), AddWishlistProductResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postAddWishlistProductApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postDeleteWishlistProductApiCall(BaseAppCompatActivity activity, AddWishlistProductRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postDeleteWishlist(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    AddWishlistProductResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), AddWishlistProductResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postDeleteWishlistProductApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postWishlistProductApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postWishlist();
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    WishlistResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), WishlistResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postWishlistProductApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postShortingProductApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postShortingProduct();
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    ShortingProductResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), ShortingProductResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postShortingProductApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postProductDetailsApiCall(BaseAppCompatActivity activity, ProductDetailsRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postProductDetails(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    ProductDetailsResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), ProductDetailsResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postProductDetailsApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postRelatedProductsApiCall(BaseAppCompatActivity activity, ProductDetailsRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postRelatedProduct(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    RelatedProductResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), RelatedProductResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postProductDetailsApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postMyOrdersApiCall(BaseAppCompatActivity activity, MyOrdersRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postMyOrders(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    MyOrdersResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), MyOrdersResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postMyOrdersApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postOrdersDetailsApiCall(BaseAppCompatActivity activity, OrderDetailsRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postOrderDetails(request);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    OrderDetailsResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), OrderDetailsResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postOrdersDetailsApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


    public void postAboutUsApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        DtypeRequest bean = new DtypeRequest();
        bean.setD_type("android");
        Call<Object> call = service.postAboutUs(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    AboutUsResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), AboutUsResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postAboutUsApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

//for secure payment

    public void postSecurePaymentResponse(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        DtypeRequest bean = new DtypeRequest();
        bean.setD_type("android");
        Call<Object> call = service.postSecurePayment(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    SecurePaymentResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), SecurePaymentResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postAboutUsApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


    //for privacy policy api call

    public void postTermsApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        DtypeRequest bean = new DtypeRequest();
        bean.setD_type("android");
        Call<Object> call = service.postTerms(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    TermsResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), TermsResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    /*PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });*/
                    callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postTermsApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postPrivacyPolicyApiCall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        DtypeRequest bean = new DtypeRequest();
        bean.setD_type("android");
        Call<Object> call = service.postPrivacyPolicy(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    PrivacyPolicyResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), PrivacyPolicyResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postAboutUsApiCall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


//privacy policy api call


    public void postShippingPolicyApicall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        DtypeRequest bean = new DtypeRequest();
        bean.setD_type("android");
        Call<Object> call = service.postShippingPolicy(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    ShippingPolicyResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), ShippingPolicyResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postShippingPolicyApicall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postContactUsApicall(BaseAppCompatActivity activity, ContactUsRequest bean, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postContactUs(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    ContactusResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), ContactusResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postContactUsApicall(activity, bean, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postCreateEphemeralApicall(BaseAppCompatActivity activity, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(100, 100);
        Call<Object> call = service.postCreateEphemeral();
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    EphemeralKeyResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), EphemeralKeyResponse.class);
                    if (body != null) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
//                    } else if (body != null && body.getResponsecode() == 0) {
//                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
//                                body.getResponsemessage(), (object, position) -> {
//                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
//                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCreateEphemeralApicall(activity, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postStripePaymentApicall(BaseAppCompatActivity activity, StripePaymentRequest bean, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(100, 100);
        bean.setUserId(PrefSetup.getInstance().getUserInfo().getResponsedata().getId());
        Call<Object> call = service.postStripePayment(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    StripePaymentResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), StripePaymentResponse.class);
                    if (body != null) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
//                    } else if (body != null && body.getResponsecode() == 0) {
//                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
//                                body.getResponsemessage(), (object, position) -> {
//                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
//                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postStripePaymentApicall(activity, bean, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postCreateOrderApicall(BaseAppCompatActivity activity, CreateOrderRequest bean, boolean showDialog, PostResponseListener callBack) {
        setBasicInfoAndId(200, 200);
        Call<Object> call = service.postCreateOrder(bean);
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    CreateOrderResponse body = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), CreateOrderResponse.class);
                    if (body != null && body.getResponsecode() == 1) {
                        Log.e(ApplicationContext.TAG, body.toString());
                        callBack.onSuccess(true, response, t, errorType, body);
                    } else if (body != null && body.getResponsecode() == 0) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                body.getResponsemessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postCreateOrderApicall(activity, bean, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


}

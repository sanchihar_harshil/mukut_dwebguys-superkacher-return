package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 6/18/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class CheckoutInfoResponse {


    @Expose
    @SerializedName("responsedata")
    private Responsedata responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Responsedata {
        @Expose
        @SerializedName("states")
        private List<String> states;
        @Expose
        @SerializedName("billing_details")
        private BillingDetails billingDetails;
        @Expose
        @SerializedName("coupon")
        private List<Coupon> coupon;
        @Expose
        @SerializedName("total")
        private double total;
        @Expose
        @SerializedName("cart")
        private List<Cart> cart;
        @Expose
        @SerializedName("user_id")
        private String userId;

        public List<String> getStates() {
            return states;
        }

        public void setStates(List<String> states) {
            this.states = states;
        }

        public BillingDetails getBillingDetails() {
            return billingDetails;
        }

        public void setBillingDetails(BillingDetails billingDetails) {
            this.billingDetails = billingDetails;
        }

        public List<Coupon> getCoupon() {
            return coupon;
        }

        public void setCoupon(List<Coupon> coupon) {
            this.coupon = coupon;
        }

        public double getTotal() {
            return total;
        }

        public void setTotal(double total) {
            this.total = total;
        }

        public List<Cart> getCart() {
            return cart;
        }

        public void setCart(List<Cart> cart) {
            this.cart = cart;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }

    public static class BillingDetails {
        @Expose
        @SerializedName("billing_phone")
        private String billingPhone;
        @Expose
        @SerializedName("billing_email")
        private String billingEmail;
        @Expose
        @SerializedName("billing_postcode")
        private String billingPostcode;
        @Expose
        @SerializedName("billing_country")
        private String billingCountry;
        @Expose
        @SerializedName("billing_state")
        private String billingState;
        @Expose
        @SerializedName("billing_city")
        private String billingCity;
        @Expose
        @SerializedName("billing_address_2")
        private String billingAddress2;
        @Expose
        @SerializedName("billing_address_1")
        private String billingAddress1;
        @Expose
        @SerializedName("billing_company")
        private String billingCompany;
        @Expose
        @SerializedName("billing_last_name")
        private String billingLastName;
        @Expose
        @SerializedName("billing_first_name")
        private String billingFirstName;

        public String getBillingPhone() {
            return billingPhone;
        }

        public void setBillingPhone(String billingPhone) {
            this.billingPhone = billingPhone;
        }

        public String getBillingEmail() {
            return billingEmail;
        }

        public void setBillingEmail(String billingEmail) {
            this.billingEmail = billingEmail;
        }

        public String getBillingPostcode() {
            return billingPostcode;
        }

        public void setBillingPostcode(String billingPostcode) {
            this.billingPostcode = billingPostcode;
        }

        public String getBillingCountry() {
            return billingCountry;
        }

        public void setBillingCountry(String billingCountry) {
            this.billingCountry = billingCountry;
        }

        public String getBillingState() {
            return billingState;
        }

        public void setBillingState(String billingState) {
            this.billingState = billingState;
        }

        public String getBillingCity() {
            return billingCity;
        }

        public void setBillingCity(String billingCity) {
            this.billingCity = billingCity;
        }

        public String getBillingAddress2() {
            return billingAddress2;
        }

        public void setBillingAddress2(String billingAddress2) {
            this.billingAddress2 = billingAddress2;
        }

        public String getBillingAddress1() {
            return billingAddress1;
        }

        public void setBillingAddress1(String billingAddress1) {
            this.billingAddress1 = billingAddress1;
        }

        public String getBillingCompany() {
            return billingCompany;
        }

        public void setBillingCompany(String billingCompany) {
            this.billingCompany = billingCompany;
        }

        public String getBillingLastName() {
            return billingLastName;
        }

        public void setBillingLastName(String billingLastName) {
            this.billingLastName = billingLastName;
        }

        public String getBillingFirstName() {
            return billingFirstName;
        }

        public void setBillingFirstName(String billingFirstName) {
            this.billingFirstName = billingFirstName;
        }
    }

    public static class Coupon {
        @Expose
        @SerializedName("coupon_enabled")
        private String couponEnabled;
        @Expose
        @SerializedName("product_id")
        private int productId;

        public String getCouponEnabled() {
            return couponEnabled;
        }

        public void setCouponEnabled(String couponEnabled) {
            this.couponEnabled = couponEnabled;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }
    }

    public static class Cart {
        @Expose
        @SerializedName("discount")
        private String discount;
        @Expose
        @SerializedName("quantity")
        private int quantity;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("short_description")
        private String shortDescription;
        @Expose
        @SerializedName("sale_price")
        private String salePrice;
        @Expose
        @SerializedName("regular_price")
        private String regularPrice;
        @Expose
        @SerializedName("post_content")
        private String postContent;
        @Expose
        @SerializedName("featured_image")
        private String featuredImage;
        @Expose
        @SerializedName("post_title")
        private String postTitle;
        @Expose
        @SerializedName("id")
        private int id;

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getShortDescription() {
            return shortDescription;
        }

        public void setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
        }

        public String getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(String salePrice) {
            this.salePrice = salePrice;
        }

        public String getRegularPrice() {
            return regularPrice;
        }

        public void setRegularPrice(String regularPrice) {
            this.regularPrice = regularPrice;
        }

        public String getPostContent() {
            return postContent;
        }

        public void setPostContent(String postContent) {
            this.postContent = postContent;
        }

        public String getFeaturedImage() {
            return featuredImage;
        }

        public void setFeaturedImage(String featuredImage) {
            this.featuredImage = featuredImage;
        }

        public String getPostTitle() {
            return postTitle;
        }

        public void setPostTitle(String postTitle) {
            this.postTitle = postTitle;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}

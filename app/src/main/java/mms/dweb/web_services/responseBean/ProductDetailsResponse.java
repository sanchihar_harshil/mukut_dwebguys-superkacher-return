package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 6/21/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class ProductDetailsResponse {


    @Expose
    @SerializedName("responsedata")
    private Responsedata responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Responsedata {
        @Expose
        @SerializedName("wishlist")
        private String wishlist;
        @Expose
        @SerializedName("_links")
        private Links Links;
        @Expose
        @SerializedName("menu_order")
        private int menuOrder;
        @Expose
        @SerializedName("grouped_products")
        private List<String> groupedProducts;
        @Expose
        @SerializedName("variations")
        private List<Integer> variations;
        @Expose
        @SerializedName("variation_imgs")
        private List<String> variationImages = new ArrayList<>();
        @Expose
        @SerializedName("default_attributes")
        private List<String> defaultAttributes;
        @Expose
        @SerializedName("attributes")
        private List<AttributesClass> attributes;
        @Expose
        @SerializedName("images")
        private List<Images> images;
        @Expose
        @SerializedName("tags")
        private List<Tags> tags;
        @Expose
        @SerializedName("categories")
        private List<Categories> categories;
        @Expose
        @SerializedName("purchase_note")
        private String purchaseNote;
        @Expose
        @SerializedName("parent_id")
        private int parentId;
        @Expose
        @SerializedName("cross_sell_ids")
        private List<String> crossSellIds;
        @Expose
        @SerializedName("upsell_ids")
        private List<String> upsellIds;
        @Expose
        @SerializedName("related_ids")
        private List<Integer> relatedIds;
        @Expose
        @SerializedName("rating_count")
        private int ratingCount;
        @Expose
        @SerializedName("average_rating")
        private String averageRating;
        @Expose
        @SerializedName("reviews_allowed")
        private boolean reviewsAllowed;
        @Expose
        @SerializedName("shipping_class_id")
        private int shippingClassId;
        @Expose
        @SerializedName("shipping_class")
        private String shippingClass;
        @Expose
        @SerializedName("shipping_taxable")
        private boolean shippingTaxable;
        @Expose
        @SerializedName("shipping_required")
        private boolean shippingRequired;
        @Expose
        @SerializedName("dimensions")
        private Dimensions dimensions;
        @Expose
        @SerializedName("weight")
        private String weight;
        @Expose
        @SerializedName("sold_individually")
        private boolean soldIndividually;
        @Expose
        @SerializedName("backordered")
        private boolean backordered;
        @Expose
        @SerializedName("backorders_allowed")
        private boolean backordersAllowed;
        @Expose
        @SerializedName("backorders")
        private String backorders;
        @Expose
        @SerializedName("in_stock")
        private boolean inStock;
        @Expose
        @SerializedName("stock_quantity")
        private int stockQuantity;
        @Expose
        @SerializedName("manage_stock")
        private boolean manageStock;
        @Expose
        @SerializedName("tax_class")
        private String taxClass;
        @Expose
        @SerializedName("tax_status")
        private String taxStatus;
        @Expose
        @SerializedName("button_text")
        private String buttonText;
        @Expose
        @SerializedName("external_url")
        private String externalUrl;
        @Expose
        @SerializedName("download_expiry")
        private int downloadExpiry;
        @Expose
        @SerializedName("download_limit")
        private int downloadLimit;
        @Expose
        @SerializedName("downloads")
        private List<String> downloads;
        @Expose
        @SerializedName("downloadable")
        private boolean downloadable;
        @Expose
        @SerializedName("virtual")
        private boolean virtual;
        @Expose
        @SerializedName("total_sales")
        private int totalSales;
        @Expose
        @SerializedName("purchasable")
        private boolean purchasable;
        @Expose
        @SerializedName("on_sale")
        private boolean onSale;
        @Expose
        @SerializedName("price_html")
        private String priceHtml;
        @Expose
        @SerializedName("sale_price")
        private String salePrice;
        @Expose
        @SerializedName("regular_price")
        private String regularPrice;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("sku")
        private String sku;
        @Expose
        @SerializedName("short_description")
        private String shortDescription;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("catalog_visibility")
        private String catalogVisibility;
        @Expose
        @SerializedName("featured")
        private boolean featured;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("date_modified_gmt")
        private String dateModifiedGmt;
        @Expose
        @SerializedName("date_modified")
        private String dateModified;
        @Expose
        @SerializedName("date_created_gmt")
        private String dateCreatedGmt;
        @Expose
        @SerializedName("date_created")
        private String dateCreated;
        @Expose
        @SerializedName("permalink")
        private String permalink;
        @Expose
        @SerializedName("slug")
        private String slug;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getWishlist() {
            return wishlist;
        }

        public void setWishlist(String wishlist) {
            this.wishlist = wishlist;
        }

        public Links getLinks() {
            return Links;
        }

        public void setLinks(Links Links) {
            this.Links = Links;
        }

        public int getMenuOrder() {
            return menuOrder;
        }

        public void setMenuOrder(int menuOrder) {
            this.menuOrder = menuOrder;
        }

        public List<String> getGroupedProducts() {
            return groupedProducts;
        }

        public void setGroupedProducts(List<String> groupedProducts) {
            this.groupedProducts = groupedProducts;
        }

        public List<Integer> getVariations() {
            return variations;
        }

        public List<String> getVariationImages() {
            return variationImages;
        }

        public void setVariationImages(List<String> variationImages) {
            this.variationImages = variationImages;
        }

        public void setVariations(List<Integer> variations) {
            this.variations = variations;
        }

        public List<String> getDefaultAttributes() {
            return defaultAttributes;
        }

        public void setDefaultAttributes(List<String> defaultAttributes) {
            this.defaultAttributes = defaultAttributes;
        }

        public List<AttributesClass> getAttributes() {
            return attributes;
        }

        public void setAttributes(List<AttributesClass> attributes) {
            this.attributes = attributes;
        }

        public List<Images> getImages() {
            return images;
        }

        public void setImages(List<Images> images) {
            this.images = images;
        }

        public List<Tags> getTags() {
            return tags;
        }

        public void setTags(List<Tags> tags) {
            this.tags = tags;
        }

        public List<Categories> getCategories() {
            return categories;
        }

        public void setCategories(List<Categories> categories) {
            this.categories = categories;
        }

        public String getPurchaseNote() {
            return purchaseNote;
        }

        public void setPurchaseNote(String purchaseNote) {
            this.purchaseNote = purchaseNote;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public List<String> getCrossSellIds() {
            return crossSellIds;
        }

        public void setCrossSellIds(List<String> crossSellIds) {
            this.crossSellIds = crossSellIds;
        }

        public List<String> getUpsellIds() {
            return upsellIds;
        }

        public void setUpsellIds(List<String> upsellIds) {
            this.upsellIds = upsellIds;
        }

        public List<Integer> getRelatedIds() {
            return relatedIds;
        }

        public void setRelatedIds(List<Integer> relatedIds) {
            this.relatedIds = relatedIds;
        }

        public int getRatingCount() {
            return ratingCount;
        }

        public void setRatingCount(int ratingCount) {
            this.ratingCount = ratingCount;
        }

        public String getAverageRating() {
            return averageRating;
        }

        public void setAverageRating(String averageRating) {
            this.averageRating = averageRating;
        }

        public boolean getReviewsAllowed() {
            return reviewsAllowed;
        }

        public void setReviewsAllowed(boolean reviewsAllowed) {
            this.reviewsAllowed = reviewsAllowed;
        }

        public int getShippingClassId() {
            return shippingClassId;
        }

        public void setShippingClassId(int shippingClassId) {
            this.shippingClassId = shippingClassId;
        }

        public String getShippingClass() {
            return shippingClass;
        }

        public void setShippingClass(String shippingClass) {
            this.shippingClass = shippingClass;
        }

        public boolean getShippingTaxable() {
            return shippingTaxable;
        }

        public void setShippingTaxable(boolean shippingTaxable) {
            this.shippingTaxable = shippingTaxable;
        }

        public boolean getShippingRequired() {
            return shippingRequired;
        }

        public void setShippingRequired(boolean shippingRequired) {
            this.shippingRequired = shippingRequired;
        }

        public Dimensions getDimensions() {
            return dimensions;
        }

        public void setDimensions(Dimensions dimensions) {
            this.dimensions = dimensions;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public boolean getSoldIndividually() {
            return soldIndividually;
        }

        public void setSoldIndividually(boolean soldIndividually) {
            this.soldIndividually = soldIndividually;
        }

        public boolean getBackordered() {
            return backordered;
        }

        public void setBackordered(boolean backordered) {
            this.backordered = backordered;
        }

        public boolean getBackordersAllowed() {
            return backordersAllowed;
        }

        public void setBackordersAllowed(boolean backordersAllowed) {
            this.backordersAllowed = backordersAllowed;
        }

        public String getBackorders() {
            return backorders;
        }

        public void setBackorders(String backorders) {
            this.backorders = backorders;
        }

        public boolean getInStock() {
            return inStock;
        }

        public void setInStock(boolean inStock) {
            this.inStock = inStock;
        }

        public int getStockQuantity() {
            return stockQuantity;
        }

        public void setStockQuantity(int stockQuantity) {
            this.stockQuantity = stockQuantity;
        }

        public boolean getManageStock() {
            return manageStock;
        }

        public void setManageStock(boolean manageStock) {
            this.manageStock = manageStock;
        }

        public String getTaxClass() {
            return taxClass;
        }

        public void setTaxClass(String taxClass) {
            this.taxClass = taxClass;
        }

        public String getTaxStatus() {
            return taxStatus;
        }

        public void setTaxStatus(String taxStatus) {
            this.taxStatus = taxStatus;
        }

        public String getButtonText() {
            return buttonText;
        }

        public void setButtonText(String buttonText) {
            this.buttonText = buttonText;
        }

        public String getExternalUrl() {
            return externalUrl;
        }

        public void setExternalUrl(String externalUrl) {
            this.externalUrl = externalUrl;
        }

        public int getDownloadExpiry() {
            return downloadExpiry;
        }

        public void setDownloadExpiry(int downloadExpiry) {
            this.downloadExpiry = downloadExpiry;
        }

        public int getDownloadLimit() {
            return downloadLimit;
        }

        public void setDownloadLimit(int downloadLimit) {
            this.downloadLimit = downloadLimit;
        }

        public List<String> getDownloads() {
            return downloads;
        }

        public void setDownloads(List<String> downloads) {
            this.downloads = downloads;
        }

        public boolean getDownloadable() {
            return downloadable;
        }

        public void setDownloadable(boolean downloadable) {
            this.downloadable = downloadable;
        }

        public boolean getVirtual() {
            return virtual;
        }

        public void setVirtual(boolean virtual) {
            this.virtual = virtual;
        }

        public int getTotalSales() {
            return totalSales;
        }

        public void setTotalSales(int totalSales) {
            this.totalSales = totalSales;
        }

        public boolean getPurchasable() {
            return purchasable;
        }

        public void setPurchasable(boolean purchasable) {
            this.purchasable = purchasable;
        }

        public boolean getOnSale() {
            return onSale;
        }

        public void setOnSale(boolean onSale) {
            this.onSale = onSale;
        }

        public String getPriceHtml() {
            return priceHtml;
        }

        public void setPriceHtml(String priceHtml) {
            this.priceHtml = priceHtml;
        }

        public String getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(String salePrice) {
            this.salePrice = salePrice;
        }

        public String getRegularPrice() {
            return regularPrice;
        }

        public void setRegularPrice(String regularPrice) {
            this.regularPrice = regularPrice;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getShortDescription() {
            return shortDescription;
        }

        public void setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCatalogVisibility() {
            return catalogVisibility;
        }

        public void setCatalogVisibility(String catalogVisibility) {
            this.catalogVisibility = catalogVisibility;
        }

        public boolean getFeatured() {
            return featured;
        }

        public void setFeatured(boolean featured) {
            this.featured = featured;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDateModifiedGmt() {
            return dateModifiedGmt;
        }

        public void setDateModifiedGmt(String dateModifiedGmt) {
            this.dateModifiedGmt = dateModifiedGmt;
        }

        public String getDateModified() {
            return dateModified;
        }

        public void setDateModified(String dateModified) {
            this.dateModified = dateModified;
        }

        public String getDateCreatedGmt() {
            return dateCreatedGmt;
        }

        public void setDateCreatedGmt(String dateCreatedGmt) {
            this.dateCreatedGmt = dateCreatedGmt;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public String getPermalink() {
            return permalink;
        }

        public void setPermalink(String permalink) {
            this.permalink = permalink;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Links {
        @Expose
        @SerializedName("collection")
        private List<Collection> collection;
        @Expose
        @SerializedName("self")
        private List<Self> self;

        public List<Collection> getCollection() {
            return collection;
        }

        public void setCollection(List<Collection> collection) {
            this.collection = collection;
        }

        public List<Self> getSelf() {
            return self;
        }

        public void setSelf(List<Self> self) {
            this.self = self;
        }
    }

    public static class Collection {
        @Expose
        @SerializedName("href")
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Self {
        @Expose
        @SerializedName("href")
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Images {
        @Expose
        @SerializedName("position")
        private int position;
        @Expose
        @SerializedName("alt")
        private String alt;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("src")
        private String src;
        @Expose
        @SerializedName("date_modified_gmt")
        private String dateModifiedGmt;
        @Expose
        @SerializedName("date_modified")
        private String dateModified;
        @Expose
        @SerializedName("date_created_gmt")
        private String dateCreatedGmt;
        @Expose
        @SerializedName("date_created")
        private String dateCreated;
        @Expose
        @SerializedName("id")
        private int id;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getAlt() {
            return alt;
        }

        public void setAlt(String alt) {
            this.alt = alt;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSrc() {
            return src;
        }

        public void setSrc(String src) {
            this.src = src;
        }

        public String getDateModifiedGmt() {
            return dateModifiedGmt;
        }

        public void setDateModifiedGmt(String dateModifiedGmt) {
            this.dateModifiedGmt = dateModifiedGmt;
        }

        public String getDateModified() {
            return dateModified;
        }

        public void setDateModified(String dateModified) {
            this.dateModified = dateModified;
        }

        public String getDateCreatedGmt() {
            return dateCreatedGmt;
        }

        public void setDateCreatedGmt(String dateCreatedGmt) {
            this.dateCreatedGmt = dateCreatedGmt;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Tags {
        @Expose
        @SerializedName("slug")
        private String slug;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Categories {
        @Expose
        @SerializedName("slug")
        private String slug;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Dimensions {
        @Expose
        @SerializedName("height")
        private String height;
        @Expose
        @SerializedName("width")
        private String width;
        @Expose
        @SerializedName("length")
        private String length;

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getLength() {
            return length;
        }

        public void setLength(String length) {
            this.length = length;
        }
    }

    public static class AttributesClass {


        @Expose
        @SerializedName("options")
        private List<String> options;
        @Expose
        @SerializedName("variation")
        private boolean variation;
        @Expose
        @SerializedName("visible")
        private boolean visible;
        @Expose
        @SerializedName("position")
        private int position;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public List<String> getOptions() {
            return options;
        }

        public void setOptions(List<String> options) {
            this.options = options;
        }

        public boolean getVariation() {
            return variation;
        }

        public void setVariation(boolean variation) {
            this.variation = variation;
        }

        public boolean getVisible() {
            return visible;
        }

        public void setVisible(boolean visible) {
            this.visible = visible;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}

package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 7/9/2018.
 */

public class StripePaymentResponse {


    @Expose
    @SerializedName("responsedata")
    private Responsedata responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Responsedata {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("source")
        private Source source;
        @Expose
        @SerializedName("refunds")
        private Refunds refunds;
        @Expose
        @SerializedName("refunded")
        private boolean refunded;
        @Expose
        @SerializedName("paid")
        private boolean paid;
        @Expose
        @SerializedName("outcome")
        private Outcome outcome;
        @Expose
        @SerializedName("livemode")
        private boolean livemode;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("customer")
        private String customer;
        @Expose
        @SerializedName("currency")
        private String currency;
        @Expose
        @SerializedName("created")
        private int created;
        @Expose
        @SerializedName("captured")
        private boolean captured;
        @Expose
        @SerializedName("balance_transaction")
        private String balanceTransaction;
        @Expose
        @SerializedName("amount_refunded")
        private int amountRefunded;
        @Expose
        @SerializedName("amount")
        private String amount;
        @Expose
        @SerializedName("object")
        private String object;
        @Expose
        @SerializedName("id")
        private String id;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Source getSource() {
            return source;
        }

        public void setSource(Source source) {
            this.source = source;
        }

        public Refunds getRefunds() {
            return refunds;
        }

        public void setRefunds(Refunds refunds) {
            this.refunds = refunds;
        }

        public boolean getRefunded() {
            return refunded;
        }

        public void setRefunded(boolean refunded) {
            this.refunded = refunded;
        }

        public boolean getPaid() {
            return paid;
        }

        public void setPaid(boolean paid) {
            this.paid = paid;
        }

        public Outcome getOutcome() {
            return outcome;
        }

        public void setOutcome(Outcome outcome) {
            this.outcome = outcome;
        }

        public boolean getLivemode() {
            return livemode;
        }

        public void setLivemode(boolean livemode) {
            this.livemode = livemode;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getCreated() {
            return created;
        }

        public void setCreated(int created) {
            this.created = created;
        }

        public boolean getCaptured() {
            return captured;
        }

        public void setCaptured(boolean captured) {
            this.captured = captured;
        }

        public String getBalanceTransaction() {
            return balanceTransaction;
        }

        public void setBalanceTransaction(String balanceTransaction) {
            this.balanceTransaction = balanceTransaction;
        }

        public int getAmountRefunded() {
            return amountRefunded;
        }

        public void setAmountRefunded(int amountRefunded) {
            this.amountRefunded = amountRefunded;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Source {
        @Expose
        @SerializedName("last4")
        private String last4;
        @Expose
        @SerializedName("funding")
        private String funding;
        @Expose
        @SerializedName("fingerprint")
        private String fingerprint;
        @Expose
        @SerializedName("exp_year")
        private int expYear;
        @Expose
        @SerializedName("exp_month")
        private int expMonth;
        @Expose
        @SerializedName("cvc_check")
        private String cvcCheck;
        @Expose
        @SerializedName("customer")
        private String customer;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("brand")
        private String brand;
        @Expose
        @SerializedName("object")
        private String object;
        @Expose
        @SerializedName("id")
        private String id;

        public String getLast4() {
            return last4;
        }

        public void setLast4(String last4) {
            this.last4 = last4;
        }

        public String getFunding() {
            return funding;
        }

        public void setFunding(String funding) {
            this.funding = funding;
        }

        public String getFingerprint() {
            return fingerprint;
        }

        public void setFingerprint(String fingerprint) {
            this.fingerprint = fingerprint;
        }

        public int getExpYear() {
            return expYear;
        }

        public void setExpYear(int expYear) {
            this.expYear = expYear;
        }

        public int getExpMonth() {
            return expMonth;
        }

        public void setExpMonth(int expMonth) {
            this.expMonth = expMonth;
        }

        public String getCvcCheck() {
            return cvcCheck;
        }

        public void setCvcCheck(String cvcCheck) {
            this.cvcCheck = cvcCheck;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Refunds {
        @Expose
        @SerializedName("url")
        private String url;
        @Expose
        @SerializedName("total_count")
        private int totalCount;
        @Expose
        @SerializedName("has_more")
        private boolean hasMore;
        @Expose
        @SerializedName("data")
        private List<String> data;
        @Expose
        @SerializedName("object")
        private String object;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public boolean getHasMore() {
            return hasMore;
        }

        public void setHasMore(boolean hasMore) {
            this.hasMore = hasMore;
        }

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }
    }

    public static class Outcome {
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("seller_message")
        private String sellerMessage;
        @Expose
        @SerializedName("risk_level")
        private String riskLevel;
        @Expose
        @SerializedName("network_status")
        private String networkStatus;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSellerMessage() {
            return sellerMessage;
        }

        public void setSellerMessage(String sellerMessage) {
            this.sellerMessage = sellerMessage;
        }

        public String getRiskLevel() {
            return riskLevel;
        }

        public void setRiskLevel(String riskLevel) {
            this.riskLevel = riskLevel;
        }

        public String getNetworkStatus() {
            return networkStatus;
        }

        public void setNetworkStatus(String networkStatus) {
            this.networkStatus = networkStatus;
        }
    }
}

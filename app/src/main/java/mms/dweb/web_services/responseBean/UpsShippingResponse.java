package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 6/19/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class UpsShippingResponse {


    @Expose
    @SerializedName("responsedata")
    private Responsedata responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Responsedata {
        @Expose
        @SerializedName("fee")
        private Fee fee;
        @Expose
        @SerializedName("ups_charges")
        private List<Upscharges> upscharges;

        public Fee getFee() {
            return fee;
        }

        public void setFee(Fee fee) {
            this.fee = fee;
        }

        public List<Upscharges> getUpscharges() {
            return upscharges;
        }

        public void setUpscharges(List<Upscharges> upscharges) {
            this.upscharges = upscharges;
        }
    }

    public static class Fee {
        @Expose
        @SerializedName("cost")
        private int cost;
        @Expose
        @SerializedName("lable")
        private String lable;
        @Expose
        @SerializedName("id")
        private String id;

        public int getCost() {
            return cost;
        }

        public void setCost(int cost) {
            this.cost = cost;
        }

        public String getLable() {
            return lable;
        }

        public void setLable(String lable) {
            this.lable = lable;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Upscharges {
        @Expose
        @SerializedName("sort")
        private int sort;
        @Expose
        @SerializedName("cost")
        private double cost;
        @Expose
        @SerializedName("label")
        private String label;
        @Expose
        @SerializedName("id")
        private String id;

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }

        public double getCost() {
            return cost;
        }

        public void setCost(double cost) {
            this.cost = cost;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}

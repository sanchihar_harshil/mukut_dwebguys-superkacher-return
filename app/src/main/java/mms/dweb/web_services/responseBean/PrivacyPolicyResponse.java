package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 6/29/2018.
 */

public class PrivacyPolicyResponse {

    @SerializedName("responsecode")
    @Expose
    private Integer responsecode;
    @SerializedName("responsemessage")
    @Expose
    private String responsemessage;
    @SerializedName("responsedata")
    @Expose
    private String responsedata;

    public Integer getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(Integer responsecode) {
        this.responsecode = responsecode;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public String getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(String responsedata) {
        this.responsedata = responsedata;
    }

}

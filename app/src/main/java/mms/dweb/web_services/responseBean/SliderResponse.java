package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 6/19/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class SliderResponse {


    @Expose
    @SerializedName("responsedata")
    private List<Responsedata> responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public List<Responsedata> getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(List<Responsedata> responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Responsedata {
        @Expose
        @SerializedName("category_id")
        private int categoryId;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("main_text")
        private String mainText;

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMainText() {
            return mainText;
        }

        public void setMainText(String mainText) {
            this.mainText = mainText;
        }
    }
}

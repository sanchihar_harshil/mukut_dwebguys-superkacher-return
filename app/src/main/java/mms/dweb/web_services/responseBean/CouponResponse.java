package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 6/19/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class CouponResponse {


    @Expose
    @SerializedName("responsedata")
    private List<Responsedata> responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public List<Responsedata> getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(List<Responsedata> responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Responsedata {
        @Expose
        @SerializedName("cost")
        private double cost;
        @Expose
        @SerializedName("_links")
        private Links Links;
        @Expose
        @SerializedName("used_by")
        private List<String> usedBy;
        @Expose
        @SerializedName("email_restrictions")
        private List<String> emailRestrictions;
        @Expose
        @SerializedName("maximum_amount")
        private String maximumAmount;
        @Expose
        @SerializedName("minimum_amount")
        private String minimumAmount;
        @Expose
        @SerializedName("exclude_sale_items")
        private boolean excludeSaleItems;
        @Expose
        @SerializedName("excluded_product_categories")
        private List<String> excludedProductCategories;
        @Expose
        @SerializedName("product_categories")
        private List<String> productCategories;
        @Expose
        @SerializedName("free_shipping")
        private boolean freeShipping;
        @Expose
        @SerializedName("usage_limit_per_user")
        private int usageLimitPerUser;
        @Expose
        @SerializedName("excluded_product_ids")
        private List<String> excludedProductIds;
        @Expose
        @SerializedName("product_ids")
        private List<String> productIds;
        @Expose
        @SerializedName("individual_use")
        private boolean individualUse;
        @Expose
        @SerializedName("usage_count")
        private int usageCount;
        @Expose
        @SerializedName("date_expires_gmt")
        private String dateExpiresGmt;
        @Expose
        @SerializedName("date_expires")
        private String dateExpires;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("discount_type")
        private String discountType;
        @Expose
        @SerializedName("date_modified_gmt")
        private String dateModifiedGmt;
        @Expose
        @SerializedName("date_modified")
        private String dateModified;
        @Expose
        @SerializedName("date_created_gmt")
        private String dateCreatedGmt;
        @Expose
        @SerializedName("date_created")
        private String dateCreated;
        @Expose
        @SerializedName("amount")
        private String amount;
        @Expose
        @SerializedName("code")
        private String code;
        @Expose
        @SerializedName("id")
        private int id;

        public double getCost() {
            return cost;
        }

        public void setCost(double cost) {
            this.cost = cost;
        }

        public Links getLinks() {
            return Links;
        }

        public void setLinks(Links Links) {
            this.Links = Links;
        }

        public List<String> getUsedBy() {
            return usedBy;
        }

        public void setUsedBy(List<String> usedBy) {
            this.usedBy = usedBy;
        }

        public List<String> getEmailRestrictions() {
            return emailRestrictions;
        }

        public void setEmailRestrictions(List<String> emailRestrictions) {
            this.emailRestrictions = emailRestrictions;
        }

        public String getMaximumAmount() {
            return maximumAmount;
        }

        public void setMaximumAmount(String maximumAmount) {
            this.maximumAmount = maximumAmount;
        }

        public String getMinimumAmount() {
            return minimumAmount;
        }

        public void setMinimumAmount(String minimumAmount) {
            this.minimumAmount = minimumAmount;
        }

        public boolean getExcludeSaleItems() {
            return excludeSaleItems;
        }

        public void setExcludeSaleItems(boolean excludeSaleItems) {
            this.excludeSaleItems = excludeSaleItems;
        }

        public List<String> getExcludedProductCategories() {
            return excludedProductCategories;
        }

        public void setExcludedProductCategories(List<String> excludedProductCategories) {
            this.excludedProductCategories = excludedProductCategories;
        }

        public List<String> getProductCategories() {
            return productCategories;
        }

        public void setProductCategories(List<String> productCategories) {
            this.productCategories = productCategories;
        }

        public boolean getFreeShipping() {
            return freeShipping;
        }

        public void setFreeShipping(boolean freeShipping) {
            this.freeShipping = freeShipping;
        }

        public int getUsageLimitPerUser() {
            return usageLimitPerUser;
        }

        public void setUsageLimitPerUser(int usageLimitPerUser) {
            this.usageLimitPerUser = usageLimitPerUser;
        }

        public List<String> getExcludedProductIds() {
            return excludedProductIds;
        }

        public void setExcludedProductIds(List<String> excludedProductIds) {
            this.excludedProductIds = excludedProductIds;
        }

        public List<String> getProductIds() {
            return productIds;
        }

        public void setProductIds(List<String> productIds) {
            this.productIds = productIds;
        }

        public boolean getIndividualUse() {
            return individualUse;
        }

        public void setIndividualUse(boolean individualUse) {
            this.individualUse = individualUse;
        }

        public int getUsageCount() {
            return usageCount;
        }

        public void setUsageCount(int usageCount) {
            this.usageCount = usageCount;
        }

        public String getDateExpiresGmt() {
            return dateExpiresGmt;
        }

        public void setDateExpiresGmt(String dateExpiresGmt) {
            this.dateExpiresGmt = dateExpiresGmt;
        }

        public String getDateExpires() {
            return dateExpires;
        }

        public void setDateExpires(String dateExpires) {
            this.dateExpires = dateExpires;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDiscountType() {
            return discountType;
        }

        public void setDiscountType(String discountType) {
            this.discountType = discountType;
        }

        public String getDateModifiedGmt() {
            return dateModifiedGmt;
        }

        public void setDateModifiedGmt(String dateModifiedGmt) {
            this.dateModifiedGmt = dateModifiedGmt;
        }

        public String getDateModified() {
            return dateModified;
        }

        public void setDateModified(String dateModified) {
            this.dateModified = dateModified;
        }

        public String getDateCreatedGmt() {
            return dateCreatedGmt;
        }

        public void setDateCreatedGmt(String dateCreatedGmt) {
            this.dateCreatedGmt = dateCreatedGmt;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Links {
        @Expose
        @SerializedName("collection")
        private List<Collection> collection;
        @Expose
        @SerializedName("self")
        private List<Self> self;

        public List<Collection> getCollection() {
            return collection;
        }

        public void setCollection(List<Collection> collection) {
            this.collection = collection;
        }

        public List<Self> getSelf() {
            return self;
        }

        public void setSelf(List<Self> self) {
            this.self = self;
        }
    }

    public static class Collection {
        @Expose
        @SerializedName("href")
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Self {
        @Expose
        @SerializedName("href")
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Value {
        @Expose
        @SerializedName("vc_grid_id")
        private List<String> vcGridId;

        public List<String> getVcGridId() {
            return vcGridId;
        }

        public void setVcGridId(List<String> vcGridId) {
            this.vcGridId = vcGridId;
        }
    }
}

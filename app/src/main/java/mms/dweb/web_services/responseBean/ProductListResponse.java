package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 6/14/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class ProductListResponse {


    @Expose
    @SerializedName("responsedata")
    private List<Responsedata> responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public List<Responsedata> getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(List<Responsedata> responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Responsedata implements Serializable{
        @Expose
        @SerializedName("wishlist")
        private int wishlist;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("price")
        private String price;
        @Expose
        @SerializedName("short_description")
        private String short_description;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("product_type")
        private String productType;
        @Expose
        @SerializedName("id")
        private int id;
        @Expose
        @SerializedName("quantity")
        private int quantity = 1;

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getWishlist() {
            return wishlist;
        }

        public void setWishlist(int wishlist) {
            this.wishlist = wishlist;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getShort_description() {
            return short_description;
        }

        public void setShort_description(String short_description) {
            this.short_description = short_description;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getProductType() {
            return productType;
        }

        public void setProductType(String productType) {
            this.productType = productType;
        }
    }
}

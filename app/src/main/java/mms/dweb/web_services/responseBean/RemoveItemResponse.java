package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 6/15/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class RemoveItemResponse {


    @Expose
    @SerializedName("responsedata")
    private List<Object> responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public List<Object> getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(List<Object> responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }
}

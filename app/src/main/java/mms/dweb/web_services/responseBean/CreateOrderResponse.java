package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 7/6/2018.
 */

public class CreateOrderResponse {


    @Expose
    @SerializedName("responsedata")
    private Responsedata responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Responsedata {
        @Expose
        @SerializedName("payment_url")
        private String paymentUrl;
        @Expose
        @SerializedName("_links")
        private Links Links;
        @Expose
        @SerializedName("refunds")
        private List<String> refunds;
        @Expose
        @SerializedName("couponlines")
        private List<Couponlines> couponlines;
        @Expose
        @SerializedName("feelines")
        private List<String> feelines;
        @Expose
        @SerializedName("shippinglines")
        private List<Shippinglines> shippinglines;
        @Expose
        @SerializedName("taxlines")
        private List<String> taxlines;
        @Expose
        @SerializedName("lineitems")
        private List<Lineitems> lineitems;
        @Expose
        @SerializedName("cart_hash")
        private String cartHash;
        @Expose
        @SerializedName("transaction_id")
        private String transactionId;
        @Expose
        @SerializedName("payment_method_title")
        private String paymentMethodTitle;
        @Expose
        @SerializedName("payment_method")
        private String paymentMethod;
        @Expose
        @SerializedName("shipping")
        private Shipping shipping;
        @Expose
        @SerializedName("billing")
        private Billing billing;
        @Expose
        @SerializedName("customer_note")
        private String customerNote;
        @Expose
        @SerializedName("customer_user_agent")
        private String customerUserAgent;
        @Expose
        @SerializedName("customer_ip_address")
        private String customerIpAddress;
        @Expose
        @SerializedName("customer_id")
        private int customerId;
        @Expose
        @SerializedName("prices_include_tax")
        private boolean pricesIncludeTax;
        @Expose
        @SerializedName("total_tax")
        private String totalTax;
        @Expose
        @SerializedName("total")
        private double total;
        @Expose
        @SerializedName("cart_tax")
        private String cartTax;
        @Expose
        @SerializedName("shipping_tax")
        private String shippingTax;
        @Expose
        @SerializedName("shipping_total")
        private String shippingTotal;
        @Expose
        @SerializedName("discount_tax")
        private String discountTax;
        @Expose
        @SerializedName("discount_total")
        private String discountTotal;
        @Expose
        @SerializedName("date_modified_gmt")
        private String dateModifiedGmt;
        @Expose
        @SerializedName("date_modified")
        private String dateModified;
        @Expose
        @SerializedName("date_created_gmt")
        private String dateCreatedGmt;
        @Expose
        @SerializedName("date_created")
        private String dateCreated;
        @Expose
        @SerializedName("currency")
        private String currency;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("version")
        private String version;
        @Expose
        @SerializedName("created_via")
        private String createdVia;
        @Expose
        @SerializedName("order_key")
        private String orderKey;
        @Expose
        @SerializedName("number")
        private String number;
        @Expose
        @SerializedName("parent_id")
        private int parentId;
        @Expose
        @SerializedName("id")
        private int id;

        public String getPaymentUrl() {
            return paymentUrl;
        }

        public void setPaymentUrl(String paymentUrl) {
            this.paymentUrl = paymentUrl;
        }

        public Links getLinks() {
            return Links;
        }

        public void setLinks(Links Links) {
            this.Links = Links;
        }

        public List<String> getRefunds() {
            return refunds;
        }

        public void setRefunds(List<String> refunds) {
            this.refunds = refunds;
        }

        public List<Couponlines> getCouponlines() {
            return couponlines;
        }

        public void setCouponlines(List<Couponlines> couponlines) {
            this.couponlines = couponlines;
        }

        public List<String> getFeelines() {
            return feelines;
        }

        public void setFeelines(List<String> feelines) {
            this.feelines = feelines;
        }

        public List<Shippinglines> getShippinglines() {
            return shippinglines;
        }

        public void setShippinglines(List<Shippinglines> shippinglines) {
            this.shippinglines = shippinglines;
        }

        public List<String> getTaxlines() {
            return taxlines;
        }

        public void setTaxlines(List<String> taxlines) {
            this.taxlines = taxlines;
        }

        public List<Lineitems> getLineitems() {
            return lineitems;
        }

        public void setLineitems(List<Lineitems> lineitems) {
            this.lineitems = lineitems;
        }

        public String getCartHash() {
            return cartHash;
        }

        public void setCartHash(String cartHash) {
            this.cartHash = cartHash;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getPaymentMethodTitle() {
            return paymentMethodTitle;
        }

        public void setPaymentMethodTitle(String paymentMethodTitle) {
            this.paymentMethodTitle = paymentMethodTitle;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public Shipping getShipping() {
            return shipping;
        }

        public void setShipping(Shipping shipping) {
            this.shipping = shipping;
        }

        public Billing getBilling() {
            return billing;
        }

        public void setBilling(Billing billing) {
            this.billing = billing;
        }

        public String getCustomerNote() {
            return customerNote;
        }

        public void setCustomerNote(String customerNote) {
            this.customerNote = customerNote;
        }

        public String getCustomerUserAgent() {
            return customerUserAgent;
        }

        public void setCustomerUserAgent(String customerUserAgent) {
            this.customerUserAgent = customerUserAgent;
        }

        public String getCustomerIpAddress() {
            return customerIpAddress;
        }

        public void setCustomerIpAddress(String customerIpAddress) {
            this.customerIpAddress = customerIpAddress;
        }

        public int getCustomerId() {
            return customerId;
        }

        public void setCustomerId(int customerId) {
            this.customerId = customerId;
        }

        public boolean getPricesIncludeTax() {
            return pricesIncludeTax;
        }

        public void setPricesIncludeTax(boolean pricesIncludeTax) {
            this.pricesIncludeTax = pricesIncludeTax;
        }

        public String getTotalTax() {
            return totalTax;
        }

        public void setTotalTax(String totalTax) {
            this.totalTax = totalTax;
        }

        public double getTotal() {
            return total;
        }

        public void setTotal(double total) {
            this.total = total;
        }

        public String getCartTax() {
            return cartTax;
        }

        public void setCartTax(String cartTax) {
            this.cartTax = cartTax;
        }

        public String getShippingTax() {
            return shippingTax;
        }

        public void setShippingTax(String shippingTax) {
            this.shippingTax = shippingTax;
        }

        public String getShippingTotal() {
            return shippingTotal;
        }

        public void setShippingTotal(String shippingTotal) {
            this.shippingTotal = shippingTotal;
        }

        public String getDiscountTax() {
            return discountTax;
        }

        public void setDiscountTax(String discountTax) {
            this.discountTax = discountTax;
        }

        public String getDiscountTotal() {
            return discountTotal;
        }

        public void setDiscountTotal(String discountTotal) {
            this.discountTotal = discountTotal;
        }

        public String getDateModifiedGmt() {
            return dateModifiedGmt;
        }

        public void setDateModifiedGmt(String dateModifiedGmt) {
            this.dateModifiedGmt = dateModifiedGmt;
        }

        public String getDateModified() {
            return dateModified;
        }

        public void setDateModified(String dateModified) {
            this.dateModified = dateModified;
        }

        public String getDateCreatedGmt() {
            return dateCreatedGmt;
        }

        public void setDateCreatedGmt(String dateCreatedGmt) {
            this.dateCreatedGmt = dateCreatedGmt;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public void setDateCreated(String dateCreated) {
            this.dateCreated = dateCreated;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getCreatedVia() {
            return createdVia;
        }

        public void setCreatedVia(String createdVia) {
            this.createdVia = createdVia;
        }

        public String getOrderKey() {
            return orderKey;
        }

        public void setOrderKey(String orderKey) {
            this.orderKey = orderKey;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Links {
        @Expose
        @SerializedName("customer")
        private List<Customer> customer;
        @Expose
        @SerializedName("collection")
        private List<Collection> collection;
        @Expose
        @SerializedName("self")
        private List<Self> self;

        public List<Customer> getCustomer() {
            return customer;
        }

        public void setCustomer(List<Customer> customer) {
            this.customer = customer;
        }

        public List<Collection> getCollection() {
            return collection;
        }

        public void setCollection(List<Collection> collection) {
            this.collection = collection;
        }

        public List<Self> getSelf() {
            return self;
        }

        public void setSelf(List<Self> self) {
            this.self = self;
        }
    }

    public static class Customer {
        @Expose
        @SerializedName("href")
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Collection {
        @Expose
        @SerializedName("href")
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Self {
        @Expose
        @SerializedName("href")
        private String href;

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Couponlines {
        @Expose
        @SerializedName("discount_tax")
        private String discountTax;
        @Expose
        @SerializedName("discount")
        private String discount;
        @Expose
        @SerializedName("code")
        private String code;
        @Expose
        @SerializedName("id")
        private int id;

        public String getDiscountTax() {
            return discountTax;
        }

        public void setDiscountTax(String discountTax) {
            this.discountTax = discountTax;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Shippinglines {
        @Expose
        @SerializedName("taxes")
        private List<String> taxes;
        @Expose
        @SerializedName("total_tax")
        private String totalTax;
        @Expose
        @SerializedName("total")
        private String total;
        @Expose
        @SerializedName("method_id")
        private String methodId;
        @Expose
        @SerializedName("method_title")
        private String methodTitle;
        @Expose
        @SerializedName("id")
        private int id;

        public List<String> getTaxes() {
            return taxes;
        }

        public void setTaxes(List<String> taxes) {
            this.taxes = taxes;
        }

        public String getTotalTax() {
            return totalTax;
        }

        public void setTotalTax(String totalTax) {
            this.totalTax = totalTax;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getMethodId() {
            return methodId;
        }

        public void setMethodId(String methodId) {
            this.methodId = methodId;
        }

        public String getMethodTitle() {
            return methodTitle;
        }

        public void setMethodTitle(String methodTitle) {
            this.methodTitle = methodTitle;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Lineitems {
        @Expose
        @SerializedName("price")
        private double price;
        @Expose
        @SerializedName("sku")
        private String sku;
        @Expose
        @SerializedName("taxes")
        private List<String> taxes;
        @Expose
        @SerializedName("total_tax")
        private String totalTax;
        @Expose
        @SerializedName("total")
        private String total;
        @Expose
        @SerializedName("subtotal_tax")
        private String subtotalTax;
        @Expose
        @SerializedName("subtotal")
        private String subtotal;
        @Expose
        @SerializedName("tax_class")
        private String taxClass;
        @Expose
        @SerializedName("quantity")
        private int quantity;
        @Expose
        @SerializedName("variation_id")
        private int variationId;
        @Expose
        @SerializedName("product_id")
        private int productId;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public List<String> getTaxes() {
            return taxes;
        }

        public void setTaxes(List<String> taxes) {
            this.taxes = taxes;
        }

        public String getTotalTax() {
            return totalTax;
        }

        public void setTotalTax(String totalTax) {
            this.totalTax = totalTax;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getSubtotalTax() {
            return subtotalTax;
        }

        public void setSubtotalTax(String subtotalTax) {
            this.subtotalTax = subtotalTax;
        }

        public String getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(String subtotal) {
            this.subtotal = subtotal;
        }

        public String getTaxClass() {
            return taxClass;
        }

        public void setTaxClass(String taxClass) {
            this.taxClass = taxClass;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getVariationId() {
            return variationId;
        }

        public void setVariationId(int variationId) {
            this.variationId = variationId;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Shipping {
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("postcode")
        private String postcode;
        @Expose
        @SerializedName("state")
        private String state;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("address_2")
        private String address2;
        @Expose
        @SerializedName("address_1")
        private String address1;
        @Expose
        @SerializedName("company")
        private String company;
        @Expose
        @SerializedName("last_name")
        private String lastName;
        @Expose
        @SerializedName("first_name")
        private String firstName;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPostcode() {
            return postcode;
        }

        public void setPostcode(String postcode) {
            this.postcode = postcode;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }
    }

    public static class Billing {
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("postcode")
        private String postcode;
        @Expose
        @SerializedName("state")
        private String state;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("address_2")
        private String address2;
        @Expose
        @SerializedName("address_1")
        private String address1;
        @Expose
        @SerializedName("company")
        private String company;
        @Expose
        @SerializedName("last_name")
        private String lastName;
        @Expose
        @SerializedName("first_name")
        private String firstName;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPostcode() {
            return postcode;
        }

        public void setPostcode(String postcode) {
            this.postcode = postcode;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }
    }
}

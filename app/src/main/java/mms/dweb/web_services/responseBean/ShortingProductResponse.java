package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 6/20/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class ShortingProductResponse {


    @Expose
    @SerializedName("tags")
    private List<Tags> tags;
    @Expose
    @SerializedName("categories")
    private List<Categories> categories;
    @Expose
    @SerializedName("responsedata")
    private List<String> responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public List<Tags> getTags() {
        return tags;
    }

    public void setTags(List<Tags> tags) {
        this.tags = tags;
    }

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public List<String> getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(List<String> responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Tags {
        @Expose
        @SerializedName("filter")
        private String filter;
        @Expose
        @SerializedName("count")
        private int count;
        @Expose
        @SerializedName("parent")
        private int parent;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("taxonomy")
        private String taxonomy;
        @Expose
        @SerializedName("term_taxonomy_id")
        private int termTaxonomyId;
        @Expose
        @SerializedName("term_group")
        private int termGroup;
        @Expose
        @SerializedName("slug")
        private String slug;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("term_id")
        private int termId;

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getParent() {
            return parent;
        }

        public void setParent(int parent) {
            this.parent = parent;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTaxonomy() {
            return taxonomy;
        }

        public void setTaxonomy(String taxonomy) {
            this.taxonomy = taxonomy;
        }

        public int getTermTaxonomyId() {
            return termTaxonomyId;
        }

        public void setTermTaxonomyId(int termTaxonomyId) {
            this.termTaxonomyId = termTaxonomyId;
        }

        public int getTermGroup() {
            return termGroup;
        }

        public void setTermGroup(int termGroup) {
            this.termGroup = termGroup;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTermId() {
            return termId;
        }

        public void setTermId(int termId) {
            this.termId = termId;
        }
    }

    public static class Categories {
        @Expose
        @SerializedName("meta_value")
        private String metaValue;
        @Expose
        @SerializedName("filter")
        private String filter;
        @Expose
        @SerializedName("count")
        private int count;
        @Expose
        @SerializedName("parent")
        private int parent;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("taxonomy")
        private String taxonomy;
        @Expose
        @SerializedName("term_taxonomy_id")
        private int termTaxonomyId;
        @Expose
        @SerializedName("term_group")
        private int termGroup;
        @Expose
        @SerializedName("slug")
        private String slug;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("term_id")
        private int termId;

        public String getMetaValue() {
            return metaValue;
        }

        public void setMetaValue(String metaValue) {
            this.metaValue = metaValue;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getParent() {
            return parent;
        }

        public void setParent(int parent) {
            this.parent = parent;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTaxonomy() {
            return taxonomy;
        }

        public void setTaxonomy(String taxonomy) {
            this.taxonomy = taxonomy;
        }

        public int getTermTaxonomyId() {
            return termTaxonomyId;
        }

        public void setTermTaxonomyId(int termTaxonomyId) {
            this.termTaxonomyId = termTaxonomyId;
        }

        public int getTermGroup() {
            return termGroup;
        }

        public void setTermGroup(int termGroup) {
            this.termGroup = termGroup;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTermId() {
            return termId;
        }

        public void setTermId(int termId) {
            this.termId = termId;
        }
    }
}

package mms.dweb.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 7/6/2018.
 */

public class EphemeralKeyResponse {

    @Expose
    @SerializedName("secret")
    private String secret;
    @Expose
    @SerializedName("livemode")
    private boolean livemode;
    @Expose
    @SerializedName("expires")
    private double expires;
    @Expose
    @SerializedName("created")
    private double created;
    @Expose
    @SerializedName("associated_objects")
    private List<Associatedobjects> associatedobjects;
    @Expose
    @SerializedName("object")
    private String object;
    @Expose
    @SerializedName("id")
    private String id;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public boolean getLivemode() {
        return livemode;
    }

    public void setLivemode(boolean livemode) {
        this.livemode = livemode;
    }

    public double getExpires() {
        return expires;
    }

    public void setExpires(double expires) {
        this.expires = expires;
    }

    public double getCreated() {
        return created;
    }

    public void setCreated(double created) {
        this.created = created;
    }

    public List<Associatedobjects> getAssociatedobjects() {
        return associatedobjects;
    }

    public void setAssociatedobjects(List<Associatedobjects> associatedobjects) {
        this.associatedobjects = associatedobjects;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class Associatedobjects {
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("id")
        private String id;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}

package mms.dweb.web_services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import mms.dweb.superkacher.util.PrefSetup;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public class RetrofitBuilder {

    public static final String IP = "https://superkacher.com/";
    public static final String BASE_API_URL = IP + "api/";
    public static final String LOGIN_URL = "login.php";
    public static final String GOOGLE_LOGIN_URL = "googlepluse_login.php";
    public static final String FACEBOOK_LOGIN_URL = "facebook_login.php";
//    public static final String FACEBOOK_LOGIN_URL = "FacebookLoginRequest.php";
    public static final String REGISTRATION_URL = "register.php";
    public static final String CATEGORIES_URL = "categories.php";
    public static final String PRODUCT_LIST_URL = "product_by_filter.php";
    public static final String BRANDS_LIST_URL = "brands.php";
    public static final String ALL_BRANDS_LIST_URL = "allbrands.php";
    public static final String HOLIDAYS_LIST_URL = "holidays.php";
    public static final String ADD_TO_CART_URL = "add_to_cart.php";
    public static final String ADD_TO_CART_OPTION_URL = "add_to_cart_option.php";
    public static final String CART_URL = "cart.php";
    public static final String REMOVE_ITEM_URL = "remove_product_from_cart.php";
    public static final String CHECKOUT_URL = "checkout.php";
    public static final String UPS_SHIPPING_URL = "ups_shipping.php";
    public static final String COUPON_URL = "coupon.php";
    public static final String SLIDER_URL = "slider.php";
    public static final String ADD_WISHLIST_PRODUCT_URL = "add_wishlist_product.php";
    public static final String DELETE_WISHLIST_PRODUCT_URL = "delete_wishlist_product.php";
    public static final String WISHLIST_PRODUCT_URL = "wishlist.php";
    public static final String SHORTING_PRODUCT_URL = "sorting_product.php";
    public static final String PRODUCT_DETAILS_URL = "product_detail.php";
    public static final String ORDER_DETAILS_URL = "order_details.php";
    public static final String RELATED_PRODUCT_URL = "related_product.php";
    public static final String MY_ORDERS_URL = "orders.php";
    public static final String UPDATE_CART_QUENTITY_URL = "update_cart_quentity.php";
    public static final String CHANGE_PASSWORD_URL = "change_password.php";
    public static final String EDIT_PROFILE_URL = "edit_profile.php";
    public static final String PROFILE_IMAGE_UPLOAD = "upload_profilepic.php";
    public static final String RESET_PASSWORD_URL = "reset_password.php";
    public static final String TESTIMONIAL_URL = "testimonials_and.php";
    public static final String CREATE_URL = "create_order.php";
    public static final String CREATE_EPHEMERAL_URL = "ephemeral_key.php";
    public static final String STRIPE_PAYMENT_URL = "stripe_payment.php";
//    public static final String STRIPE_PAYMENT_URL = "stripe_payment_test.php";
    public static final String ABOUT_US = "about.php";
    public static final String CONTACT_US = "contact.php";
    public static final String SHIPPING_POLICY = "shipping.php";
    public static final String PRIVACY_POLICY = "privacy.php";
    public static final String SECURE_PAYMENT = "secure.php";
    public static final String GET_TERMS = "get_terms.php";

    private Retrofit retrofit;

    private static final RetrofitBuilder ourInstance = new RetrofitBuilder();

    public static RetrofitBuilder getInstance() {
        return ourInstance;
    }

    private RetrofitBuilder() {
    }

    public Retrofit getRetrofitInstance(int connectionTimeOut, int readTimeOut) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };


            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = builder
                    .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
                    .readTimeout(readTimeOut, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build();


            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_API_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Retrofit getRetrofitGetID(int connectionTimeOut, int readTimeOut, String userId) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };


            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();


            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header("Content-Type", "application/json;charset=UTF-8")
                            .header("SECRET", "3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4")
//                            .header("USERID", "35")
                            .header("USERID", userId)
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                }
            });

            httpClient.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            httpClient.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

//                    OkHttpClient client = httpClient.build();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = httpClient
                    .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
                    .readTimeout(readTimeOut, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build();

//            OkHttpClient client = builder
//                    .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
//                    .readTimeout(readTimeOut, TimeUnit.SECONDS).build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_API_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Retrofit getRetrofitHeaderWithoutID(int connectionTimeOut, int readTimeOut) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };


            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();


            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header("Content-Type", "application/json;charset=UTF-8")
                            .header("SECRET", "3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4")
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                }
            });

            httpClient.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            httpClient.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

//                    OkHttpClient client = httpClient.build();

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


            OkHttpClient client = httpClient
                    .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
                    .readTimeout(readTimeOut, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build();

//            OkHttpClient client = builder
//                    .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
//                    .readTimeout(readTimeOut, TimeUnit.SECONDS).build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_API_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Retrofit getMultipleRetrofitGetID(int connectionTimeOut, int readTimeOut, String userId) {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };


            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

//                OkHttpClient.Builder builder = new OkHttpClient.Builder();
//                builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
//                builder.hostnameVerifier(new HostnameVerifier() {
//                    @Override
//                    public boolean verify(String hostname, SSLSession session) {
//                        return true;
//                    }
//                });
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header("Content-Type", "application/json;charset=UTF-8")
                            .header("SECRET", "3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4")
                            .header("USERID", userId)
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                }
            });

            httpClient.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            httpClient.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

//                    OkHttpClient client = httpClient.build();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = httpClient
                    .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
                    .readTimeout(readTimeOut, TimeUnit.SECONDS).
                            addInterceptor(interceptor).
                            build();
//                OkHttpClient client = builder
//                        .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
//                        .readTimeout(readTimeOut, TimeUnit.SECONDS).build();
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_API_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            return retrofit;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}



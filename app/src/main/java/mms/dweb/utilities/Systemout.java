package mms.dweb.utilities;

// This class is used to root log on a particular output device or any other media
// For now its show logs on log window.
public class Systemout {
    private static String appMark = "******************* - ";

    public static void println(Object string) {
        System.out.println(appMark + string);
    }
}

package mms.dweb.superkacher.Google;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import mms.dweb.superkacher.R;

public class ProfileActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textName, textEmail;
    FirebaseAuth mAuth;
    Button btn;

    //a constant for detecting the login intent result
    private static final int RC_SIGN_IN = 234;

    //Tag for the logs optional
    private static final String TAG = "SMI with FCM";

    //creating a GoogleSignInClient object
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mAuth = FirebaseAuth.getInstance();

//        imageView = findViewById(R.id.imageView);
        textName = findViewById(R.id.textViewName);
//        textEmail = findViewById(R.id.textViewEmail);
        btn=findViewById(R.id.btnso);


        FirebaseUser user = mAuth.getCurrentUser();


        //Then we need a GoogleSignInOptions object
        //And we need to build it as below
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        //Then we will get the GoogleSignInClient object from GoogleSignIn class
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);



//        Picasso.get().load(user.getPhotoUrl()).into(imageView);
//        String phone=user.getPhoneNumber();
//        Toast.makeText(this, phone, Toast.LENGTH_SHORT).show();
//        Log.i("Phone Number",phone);

        textName.setText(user.getDisplayName());
//        textEmail.setText(user.getEmail());

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGoogleSignInClient.signOut()
                        .addOnCompleteListener(ProfileActivity.this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                mAuth.signOut();

                                Toast.makeText(ProfileActivity.this, "Sign Out Sucess", Toast.LENGTH_SHORT).show();
                                Intent i=new Intent(ProfileActivity.this,GG.class);
                                startActivity(i);

                            }
                        });

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        //if the user is not logged in
        //opening the login activity
        if (mAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, GG.class));
        }
    }
}
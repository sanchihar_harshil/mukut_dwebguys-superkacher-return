package mms.dweb.superkacher.Activity.Stripe;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.stripe.android.CustomerSession;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Customer;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;

import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.ErrorType;
import mms.dweb.stripe.SampleStoreEphemeralKeyProvider;
import mms.dweb.stripe.StoreCart;
import mms.dweb.stripe.StoreUtils;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Activity.checkout.PaymentActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.base.FragmentNames;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.utilities.Constant;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.CreateOrderRequest;
import mms.dweb.web_services.requestBean.StripePaymentRequest;
import mms.dweb.web_services.responseBean.CreateOrderResponse;
import mms.dweb.web_services.responseBean.EphemeralKeyResponse;
import retrofit2.Response;

public class StripePaymentActivity extends BaseAppCompatActivity {

    CardMultilineWidget cmw;
    Card cardToSave;
    TextView txt;
    EditText edt;
    Button pay;
    Button stripebutton;
    /*
     * Change this to your publishable key.
     *
     * You can get your key here: https://dashboard.stripe.com/account/apikeys
     */
    private static final String PUBLISHABLE_KEY = Constant.PUBLISHABLE_KEY;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripe_payment);
        PaymentConfiguration.init(PUBLISHABLE_KEY);
        setupCustomerSession();
    }

    private void setupCustomerSession() {
        // CustomerSession only needs to be initialized once per app.
        CustomerSession.initCustomerSession(
                new SampleStoreEphemeralKeyProvider(
                        new SampleStoreEphemeralKeyProvider.ProgressListener() {
                            @Override
                            public void onStringResponse(String string) {
                                if (string.startsWith("Error: ")) {
                                    new AlertDialog.Builder(StripePaymentActivity.this)
                                            .setMessage(string).show();
                                }
                            }
                        }));
    }

    private CreateOrderRequest createOrderRequest;

    @Override
    protected void onStart() {
        super.onStart();
        Bundle bundle = getIntent().getExtras();
        if (bundle.getSerializable("Order") != null) {
            createOrderRequest = (CreateOrderRequest) bundle.getSerializable("Order");
        }

        initializeView();
    }

    @Override
    public void initializeView() {
        super.initializeView();

        cmw = findViewById(R.id.cardmultilineWidget);
        pay = findViewById(R.id.paybutton);
        stripebutton = findViewById(R.id.stripebutton);
        txt = findViewById(R.id.txt);
        edt = findViewById(R.id.edt);

        stripebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StripePaymentActivity.this, PaymentActivity.class);
                intent.putExtra("Order", createOrderRequest);
                startActivity(intent);
            }
        });

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                getDetailsofCard();

            }
        });
    }

    public void getDetailsofCard() {

        cardToSave = cmw.getCard();
        if (cardToSave == null) {
//            mErrorDialogHandler.showError("Invalid Card Data");
            Toast.makeText(this, "Invalid Card Data", Toast.LENGTH_SHORT).show();
        } else {
            Log.i("Card Data Filled : ", "Successful");


            if (cardToSave != null) {
                cardToSave.getNumber();
                cardToSave.getExpMonth();
                cardToSave.getExpYear();
                cardToSave.getCVC();


                if (!cardToSave.validateCard()) {
                    // Show errors
                    Toast.makeText(this, "Card is Not Valid and/or Can't Validate Card Data", Toast.LENGTH_SHORT).show();
                    Log.i("Card Validation : ", "unSuccessful");
                } else {

                    Toast.makeText(this, "Card is Valid", Toast.LENGTH_SHORT).show();
                    Log.i("Card Validation : ", "Successful");
                    generateToken();

                }
            }

        }

    }


    private Token mToken;

    public void generateToken() {

//        TODO Replace PublishKey from Test key with Live key before making the application live and before final build of apk
        Stripe stripe = new Stripe(this, Constant.PUBLISHABLE_KEY);
        stripe.createToken(
                cardToSave,
                new TokenCallback() {
                    public void onError(Exception error) {
                        // Show localized error message

                        Toast.makeText(StripePaymentActivity.this, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        Log.i("Token Data", " Generation unSuccessful");
                    }

                    @Override
                    public void onSuccess(Token token) {
                        mToken = token;
                        token.getId();
                        Toast.makeText(StripePaymentActivity.this, "Token Generated Successfuly Token Data : " + token, Toast.LENGTH_SHORT).show();
                        Log.e("Token", " Normal Generated Successfuly : " + token.getId());
                        Log.e("Token", " Card Generated Successfuly : " + token.getCard());
                        Log.e("Token", " Created Generated Successfuly : " + token.getCreated());
                        Log.e("Token", " Type Generated Successfuly : " + token.getType());
                        Log.e("Token", " Bank Account Generated Successfuly : " + token.getBankAccount());
                        Log.e("Token", " Live Mode Generated Successfuly : " + token.getLivemode());
                        Log.e("Token", "Used Generated Successfuly : " + token.getUsed());
                        Log.e("Token", "Card Id Generated Successfuly : " + token.getCard().getId());

                        edt.setText("Token Card Id : " + token.getCard().getId().toString());
                        txt.setText("Token Id : " + token.getId().toString());
//                                chargingAcard(token);
//                                tok_1CeMB6I9McAD9Rj3ARsdAH2H

                        postCreateEphemeralApicall();
                    }
                });


    }

    private void postCreateEphemeralApicall() {
        ApiCall.getInstance().postCreateEphemeralApicall(this, true, this::onEphemeralResponse);
    }

    private EphemeralKeyResponse ephemeralKeyResponse;

    private void onEphemeralResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType type, Object o) {

        if (isSuccess) {
            ephemeralKeyResponse = (EphemeralKeyResponse) o;
            postStripePaymentApicall();
        }
    }

    private StripePaymentRequest stripePaymentRequest;

    private void postStripePaymentApicall() {
        if (stripePaymentRequest == null)
            stripePaymentRequest = new StripePaymentRequest();
        stripePaymentRequest.setAmount(createOrderRequest.getCost());
        stripePaymentRequest.setCustomerid(ephemeralKeyResponse.getAssociatedobjects().get(0).getId());
        stripePaymentRequest.setToken(mToken.getCard().getId());
        ApiCall.getInstance().postStripePaymentApicall(this, stripePaymentRequest, true, this::onStripePayResponse);
    }

    private void onStripePayResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType type, Object o) {
        if (isSuccess) {
            postCreateOrderApicall();
        }
    }

    private void postCreateOrderApicall() {
        ApiCall.getInstance().postCreateOrderApicall(this, createOrderRequest, true, this::onOrderCreate);
    }

    CreateOrderResponse createOrderResponse;

    private void onOrderCreate(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType type, Object o) {
        if (isSuccess) {
            createOrderResponse = (CreateOrderResponse) o;
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.checkout),
                    createOrderResponse.getResponsemessage(), (object, position) -> {
                        PrefSetup.getInstance().setOrderInfo(createOrderResponse);
                        Log.e(ApplicationContext.TAG, PrefSetup.getInstance().getOrderInfo().toString());
                        startActivity();
                    });
        }
    }

    private void startActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("fragmentName", FragmentNames.MyOrdersFragment);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

}

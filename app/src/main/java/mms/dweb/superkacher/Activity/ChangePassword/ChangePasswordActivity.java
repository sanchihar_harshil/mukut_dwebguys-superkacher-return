package mms.dweb.superkacher.Activity.ChangePassword;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.ChangePasswordRequest;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseAppCompatActivity {
    @BindView(R.id.et_old_password_cpa)
    EditText et_old_password_cpa;
    @BindView(R.id.et_new_password_cpa)
    EditText et_new_password_cpa;
    @BindView(R.id.et_confirm_password_cpa)
    EditText et_confirm_password_cpa;

    @OnClick(R.id.txt_save_action_cpa)
    void click_txt_save_action_cpa() {
        if (isValid())
            changePasswordApiCall();
    }

    private void changePasswordApiCall() {
        ApiCall.getInstance().postChangePasswordApiCall(this, changePasswordRequest, true, this::OnPasswordChangeResponse);
    }

    private void OnPasswordChangeResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            onBackPressed();
        }
    }

    private ChangePasswordRequest changePasswordRequest;

    private boolean isValid() {
        if (et_old_password_cpa.getText().toString().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.old_password), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (et_new_password_cpa.getText().toString().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.new_password), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (et_confirm_password_cpa.getText().toString().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.old_password), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (!et_confirm_password_cpa.getText().toString().trim().equals(et_new_password_cpa.getText().toString().trim())) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.old_password), getString(R.string.password_match_error), (object, position) -> {

            });
            return false;
        }

        if (changePasswordRequest == null)
            changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setNewpassword(et_new_password_cpa.getText().toString().trim());
        changePasswordRequest.setOldpassword(et_old_password_cpa.getText().toString().trim());
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_ca);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        }
        toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });
    }
}

package mms.dweb.superkacher.Activity.Login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.PasswordResetRequest;
import mms.dweb.web_services.requestBean.RegistrationRequest;
import mms.dweb.web_services.responseBean.LoginResponse;
import mms.dweb.web_services.responseBean.PasswordResetResponse;
import retrofit2.Response;

public class RegistrationActivity extends BaseAppCompatActivity {

    @BindView(R.id.et_confirmPassword_RA)
    EditText et_confirmPassword_RA;

    @BindView(R.id.et_password_RA)
    EditText et_password_RA;

    @BindView(R.id.et_email_RA)
    EditText et_email_RA;

    @BindView(R.id.et_username_RA)
    EditText et_username_RA;

    @BindView(R.id.et_lastName_RA)
    EditText et_lastName_RA;

    @BindView(R.id.et_firstName_RA)
    EditText et_firstName_RA;

    @BindView(R.id.layout_signIn_RA)
    LinearLayout layout_signIn_RA;

    @BindView(R.id.txt_login_action_RA)
    TextView txt_login_action_RA;





    @OnClick(R.id.layout_signIn_RA)
    void click_layout_signIn_RA() {
        start_login_activity();
    }


    @OnClick(R.id.txt_login_action_RA)
    void click_txt_login_action_RA() {
        if (isValid())
            postUserRegistrationApiCall();

    }

    private void postUserRegistrationApiCall() {
        ApiCall.getInstance().postUserRegistrationApiCall(this, registrationRequest, true, this::OnuserRegistrationResponse);
    }

    private void OnuserRegistrationResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType type, Object o) {
        if (isSuccess){
            startMainActivity();

        }

    }

    private void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        initializeView();

    }

    @Override
    public void initializeView() {
        super.initializeView();

    }

    private RegistrationRequest registrationRequest;

    private boolean isValid() {
        if (et_firstName_RA.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.first_name), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        }else if (et_lastName_RA.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.last_name), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        }else if (et_username_RA.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.user_name), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        }else if (et_email_RA.getText().toString().trim().isEmpty()&& Utilities.getInstance().isValidEmail(et_email_RA.getText().toString()) ) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.emailid), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        }else if (et_password_RA.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.password), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        }else if (et_confirmPassword_RA.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.confirm_password), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        }else if (!et_confirmPassword_RA.getText().toString().equals(et_password_RA.getText().toString())) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.password), getString(R.string.password_match_error), (object, position) -> {

            });
            return false;
        }

        if (registrationRequest == null)
            registrationRequest = new RegistrationRequest();
        registrationRequest.setFirst_name(et_firstName_RA.getText().toString().trim());
        registrationRequest.setLast_name(et_lastName_RA.getText().toString().trim());
        registrationRequest.setUser_name(et_username_RA.getText().toString().trim());
        registrationRequest.setUser_email(et_email_RA.getText().toString().trim());
        registrationRequest.setPassword(et_password_RA.getText().toString().trim());
        return true;

    }

    public void start_login_activity(){
        Intent intent=new Intent(RegistrationActivity.this,LoginUserActivity.class);
        startActivity(intent);
    }

}

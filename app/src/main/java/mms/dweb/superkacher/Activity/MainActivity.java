package mms.dweb.superkacher.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import mms.dweb.Converter;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.ErrorType;
import mms.dweb.base.MarshMallowPermission;
import mms.dweb.model.MenuModel;
import mms.dweb.superkacher.Activity.CartInfo.CartInfoActivity;
import mms.dweb.superkacher.Activity.Login.LoginUserActivity;
import mms.dweb.superkacher.Activity.filter.FilterActivity;
import mms.dweb.superkacher.Activity.wishlist.WishlistActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.AboutUs.AboutUsFragment;
import mms.dweb.superkacher.Fragment.Brands.BrandsFragment;
import mms.dweb.superkacher.Fragment.ContactUs.ContactUsFragment;
import mms.dweb.superkacher.Fragment.Holidays.HolidaysFragment;
import mms.dweb.superkacher.Fragment.MyOrders.MyOrdersFragment;
import mms.dweb.superkacher.Fragment.MyOrders.OrderDetailsFragment;
import mms.dweb.superkacher.Fragment.PrivacyPolicy.PrivacyPolicy;
import mms.dweb.superkacher.Fragment.ProductList.ProductListFragment;
import mms.dweb.superkacher.Fragment.SecurePayment.SecurePaymentFragment;
import mms.dweb.superkacher.Fragment.ShippingPolicy.ShippingPolicy;
import mms.dweb.superkacher.Fragment.TermsConditionsFragment;
import mms.dweb.superkacher.Fragment.Testimonial.TestimonialFragment;
import mms.dweb.superkacher.Fragment.cart.CartInfoFragment;
import mms.dweb.superkacher.Fragment.main.MainFragment;
import mms.dweb.superkacher.Fragment.MyAccount.MyAccountFragment;
import mms.dweb.superkacher.Fragment.ShopNow.ShopNowFragment;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.Fragment.base.FragmentNames;
import mms.dweb.superkacher.Manifest;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.ExpandableListAdapter;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.responseBean.CartInfoResponse;
import mms.dweb.web_services.responseBean.LoginResponse;
import mms.dweb.web_services.responseBean.ShortingProductResponse;
import retrofit2.Response;

public class MainActivity extends BaseAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.txt_heading_name_ma)
    TextView txt_heading_name_ma;

    @BindView(R.id.img_heading_ma)
    ImageView img_heading_ma;

//    @BindView(R.id.navigation_MA)
//    BottomNavigationView navigation_MA;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.expandableListView)
    ExpandableListView expandableListView;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.navigation_demo)
    DemoNavigation navigation_demo;
    private View header;
    private TextView txt_heading_mail, txt_heading_name;
    private ImageView imageView;
    ExpandableListAdapter expandableListAdapter;
    int cart_count = 0;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    private ShortingProductResponse shortingProductResponse;
    private int orderBy = 0;

    public int getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(int orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigation_demo.enableItemShiftingMode(false);
        navigation_demo.enableAnimation(false);
        navigation_demo.enableShiftingMode(false);
        prepareMenuData();

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toggle.getDrawerArrowDrawable().setColor(getColor(R.color.colorPrimary));
        } else {
            toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorPrimary));
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);

        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(FragmentNames.MyAccountFragment, false, null, false, false);
                navigationDrawClose();
            }
        });

        txt_heading_mail = header.findViewById(R.id.txt_heading_mail);
        txt_heading_name = header.findViewById(R.id.txt_heading_name);
        imageView = header.findViewById(R.id.imageView);

        replaceFragment(FragmentNames.MainFragment, false, null, true, true);


    }

    List<String> menuItemList;
    List<String> childmenuItemList;

    public void updateCartcount(int cart_count) {

        this.cart_count = cart_count;
        invalidateOptionsMenu();
    }

    private void prepareMenuData() {
        menuItemList = Arrays.asList(getResources().getStringArray(R.array.menu_array));
        childmenuItemList = Arrays.asList(getResources().getStringArray(R.array.child_menu_array));
        List<MenuModel> childModelsList = new ArrayList<>();
        for (String submenuname : childmenuItemList) {
            MenuModel menuModel = new MenuModel(submenuname, true, false);
            childModelsList.add(menuModel);
        }

        for (String menuname : menuItemList) {
            MenuModel menuModel = null;
            switch (menuname) {
                case "Terms & Conditions":
                    menuModel = new MenuModel(menuname, true, true);
                    childList.put(menuModel, childModelsList);
                    break;
                case "Logout":
                    if (PrefSetup.getInstance().getUserInfo() == null)
                        menuname = "Login";
                    menuModel = new MenuModel(menuname, true, false);
                    break;
                default:
                    menuModel = new MenuModel(menuname, true, false);
                    break;
            }
            addModelInHeaderList(menuModel, menuname);
//            headerList.add(menuModel);
        }

        populateExpandableList();

    }

    private void addModelInHeaderList(MenuModel menuModel, String menuname) {
        if (PrefSetup.getInstance().getUserInfo() == null && (menuname.equals(menuItemList.get(6)) || menuname.equals(menuItemList.get(7)) || menuname.equals(menuItemList.get(8))))
            return;
        headerList.add(menuModel);
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long position) {
                if (headerList.get(groupPosition).isGroup && !headerList.get(groupPosition).hasChildren) {
                    navigationDrawClose();
                    MenuModel data = headerList.get((int) position);
                    switch (data.getMenuName()) {
                        case "Home":
                            replaceFragment(FragmentNames.MainFragment, true, null, true, true);
                            return true;
                        case "Shop Now":
                            openShopNowProductList();
                            setBottomNavigationItemSelected(1);
                            return true;
                        case "Shop By Brands":
                            replaceFragment(FragmentNames.BrandsFragment, true, null, false, false);
                            return true;
                        case "Holidays":
                            replaceFragment(FragmentNames.HolidaysFragment, true, null, false, false);
                            return true;
                        case "Search Products":
                            setOrderBy(0);
                            if (ApplicationContext.getInstance().getLiveFragment() instanceof ProductListFragment) {
                                fragment.getNewProduct();
                            } else {
                                Bundle bundle = new Bundle();
                                bundle.putInt("data", 0);
                                replaceFragment(FragmentNames.ProductListFragment, false, bundle, false, false);
                            }
                            return true;
                        case "New Arrivals":
                            setOrderBy(3);
                            if (ApplicationContext.getInstance().getLiveFragment() instanceof ProductListFragment) {
                                fragment.getNewProduct();
                            } else {
                                Bundle bundle = new Bundle();
                                bundle.putInt("data", 0);
                                replaceFragment(FragmentNames.ProductListFragment, false, bundle, false, false);
                            }
                            return true;
                        case "Wishlist":
                            if (isValidUser()) {
                                startWishListActivity();
                            }
                            return true;
                        case "Cart":
                            replaceFragment(FragmentNames.CartInfoFragment, true, null, false, false);
                            return true;
                        case "My Orders":
                            replaceFragment(FragmentNames.MyOrdersFragment, true, null, false, false);
                            return true;
                        case "Terms &amp; Conditions":

                            return true;
                        case "About Us":
                            replaceFragment(FragmentNames.AboutUsFragment, true, null, false, false);
                            return true;
                        case "Contact Us":
                            replaceFragment(FragmentNames.ContactUsFragment, true, null, false, false);
                            return true;
                        case "Testimonial":
                            replaceFragment(FragmentNames.TestimonialFragment, true, null, false, false);
                            return true;
                        case "Logout":
                            Logout();
                            return true;
                        case "Login":
                            Logout();
                            return true;


                    }
                    //listener based on items position, count starts from zero(0)
                }

                return false;
            }


        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);

                    if (id == 0) {
                        replaceFragment(FragmentNames.TermsConditionsFragment, true, null, false, false);
                        navigationDrawClose();
                        Log.e("position clicked", String.valueOf(id));

                    } else if (id == 1) {
                        replaceFragment(FragmentNames.SecurityPayment, true, null, false, false);
                        navigationDrawClose();
                        Log.e("position clicked", String.valueOf(id));

                    } else if (id == 2) {
                        replaceFragment(FragmentNames.ShippingPolicy, true, null, false, false);
                        navigationDrawClose();
                        Log.e("position clicked", String.valueOf(id));

                    } else {
                        replaceFragment(FragmentNames.PrivacyPolicy, true, null, false, false);
                        navigationDrawClose();
                        Log.e("position clicked", String.valueOf(id));

                    }

                }

                return false;
            }
        });


        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);

                    if (id == 0) {
                        replaceFragment(FragmentNames.TermsConditionsFragment, true, null, false, false);
                        navigationDrawClose();
                        Log.e("position clicked", String.valueOf(id));

                    } else if (id == 1) {
                        replaceFragment(FragmentNames.SecurityPayment, true, null, false, false);
                        navigationDrawClose();
                        Log.e("position clicked", String.valueOf(id));

                    } else if (id == 2) {
                        replaceFragment(FragmentNames.ShippingPolicy, true, null, false, false);
                        navigationDrawClose();
                        Log.e("position clicked", String.valueOf(id));

                    } else {
                        replaceFragment(FragmentNames.PrivacyPolicy, true, null, false, false);
                        navigationDrawClose();
                        Log.e("position clicked", String.valueOf(id));

                    }

                }

                return false;
            }
        });
        navigationDrawClose();
    }

    private void navigationDrawClose() {
        if (drawer != null)
            drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeView();
        if (PrefSetup.getInstance().getOrderInfo() != null) {
            /*replaceFragment(FragmentNames.MyOrdersFragment, false, null, false, false);
            PrefSetup.getInstance().setOrderInfo(null);*/
            replaceFragment(FragmentNames.MainFragment, false, null, true, true);
            replaceFragment(FragmentNames.OrderDetailsFragment, false, null, false, false);
        }
    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.nav_cart_main);
//        MenuItem item = menu.findItem(R.id.search);
//        materialSearchView.setMenuItem(item);

        menuItem.setIcon(Converter.convertLayoutToImage(MainActivity.this, cart_count, R.drawable.ic_shopping_cart_black_24dp));
        return super.onCreateOptionsMenu(menu);
//        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.nav_wishlist_main:
                if (isValidUser())
                    startWishListActivity();
                Log.e(ApplicationContext.TAG, "Done");
                return true;
//            case R.id.nav_filter_main:
////                makeToast("Filter");
//                Log.e(ApplicationContext.TAG, "Filter Done");
//                return true;
            case R.id.nav_cart_main:
                startCarfInfoActivity();
//                replaceFragment(FragmentNames.CartInfoFragment, true, null, false, false);
                Log.e(ApplicationContext.TAG, "Done");
                return true;
        }

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.nav_wishlist) {
//            return true;
//        } else if (id == R.id.nav_cart) {
//            return true;
//        }
//
        return super.onOptionsItemSelected(item);
    }

    private void startWishListActivity() {
        startActivity(new Intent(this, WishlistActivity.class));
    }

    private void startCarfInfoActivity() {
        startActivity(new Intent(this, CartInfoActivity.class));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_home:
                replaceFragment(FragmentNames.MainFragment, true, null, true, true);
                break;
            case R.id.nav_shop_now:
                replaceFragment(FragmentNames.ShopNowFragment, true, null, false, false);
                break;
            case R.id.nav_brands:
                replaceFragment(FragmentNames.BrandsFragment, true, null, false, false);
                break;
            case R.id.nav_holidays:
                replaceFragment(FragmentNames.HolidaysFragment, true, null, false, false);
                break;
            case R.id.nav_search_products:
                replaceFragment(FragmentNames.MyAccountFragment, true, null, false, false);
                break;
            case R.id.nav_new_arrival:
                Log.e(ApplicationContext.TAG, "Done");
                break;
            case R.id.nav_wishlist:
                Log.e(ApplicationContext.TAG, "Done");
                break;
            case R.id.nav_cart:
                Log.e(ApplicationContext.TAG, "Done");
                break;
            case R.id.nav_my_orders:
                replaceFragment(FragmentNames.MyOrdersFragment, false, null, false, false);
                Log.e(ApplicationContext.TAG, "Done");
                break;
            case R.id.nav_abouts_us:
                Log.e(ApplicationContext.TAG, "Done");
                break;
            case R.id.nav_contacts_us:
                Log.e(ApplicationContext.TAG, "Done");
                break;
            case R.id.nav_testimonial:
                Log.e(ApplicationContext.TAG, "Done");
                break;
            case R.id.nav_logout:
                Logout();
                Log.e(ApplicationContext.TAG, "Done");
                break;
        }
        navigationDrawClose();
        return true;
    }

    private FirebaseAuth mAuth;
    private GoogleSignInClient gAuth;

    public void Logout() {
        PrefSetup.getInstance().clearPrefSetup();
        mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        gAuth = GoogleSignIn.getClient(this, gso);
        gAuth.signOut();
//        LoginManager.getInstance().logOut();
        ApplicationContext.getInstance().clearFragment();
        startLoginActivity();
    }

    public void startLoginActivity() {
        startActivity(new Intent(this, LoginUserActivity.class));
        finish();
    }

    @Override
    public void initializeView() {
        super.initializeView();
        navigation_demo.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.bottom_nav_home:
                    replaceFragment(FragmentNames.MainFragment, false, null, true, true);
                    return true;
                case R.id.bottom_nav_shop_now:
//                    replaceFragment(FragmentNames.ShopNowFragment, false, null, false, false);
//                    replaceFragment(FragmentNames.ProductListFragment, false, null, false, false);
                    openShopNowProductList();
                    return true;
                case R.id.bottom_nav_brands:
                    replaceFragment(FragmentNames.BrandsFragment, false, null, false, false);
                    return true;
                case R.id.bottom_nav_holidays:
                    replaceFragment(FragmentNames.HolidaysFragment, false, null, false, false);
                    return true;
                case R.id.bottom_nav_my_account:
                    replaceFragment(FragmentNames.MyAccountFragment, false, null, false, false);
                    return true;
            }
            return false;
        });

        setNavigationHeader();
        postCartApiCall();          // this method update your cart value on toolbar
    }

    private void openShopNowProductList() {
        setOrderBy(0);
        if (ApplicationContext.getInstance().getLiveFragment() instanceof ProductListFragment) {
            fragment.getShopNowProduct();
        } else {
            Bundle bundle = new Bundle();
            bundle.putInt("data", 0);
            replaceFragment(FragmentNames.ProductListFragment, false, bundle, false, false);
        }

    }

    /**
     * Method use to check BottomNavigation selected item.
     *
     * @param position to which you want it to show.
     *                 position 0 : show home page
     *                 positon 1 : show shop now page
     *                 positon 2 : show all brands page
     *                 positon 3 : show holidays page
     *                 positon 4 : show account details page
     * @return void
     */
    private void setBottomNavigationItemSelected(int position) {
        switch (position) {
            case 0:
                navigation_demo.setSelectedItemId(R.id.bottom_nav_home);
                break;
            case 1:
                navigation_demo.setSelectedItemId(R.id.bottom_nav_shop_now);
                break;
            case 2:
                navigation_demo.setSelectedItemId(R.id.bottom_nav_brands);
                break;
            case 3:
                navigation_demo.setSelectedItemId(R.id.bottom_nav_holidays);

                break;
            case 4:
                navigation_demo.setSelectedItemId(R.id.bottom_nav_my_account);
                break;
        }
    }

    private BaseFragment fragment;

    /**
     * Method use to replace fragmet.
     *
     * @param fragmentName     to which you want it to show.
     * @param isBottomSelected to highlight bottom navigation item
     *                         position 0 : show home page
     *                         positon 1 : show shop now page
     *                         positon 2 : show all brands page
     *                         positon 3 : show holidays page
     *                         positon 4 : show account details page
     * @param bundle           to set value of bundle
     * @param isFirst          to set fragment on first
     * @param clearBackStack   to clear back stack
     * @return void
     */
    public void replaceFragment(FragmentNames fragmentName, boolean isBottomSelected, Bundle bundle, boolean isFirst, boolean clearBackStack) {
        Utilities.getInstance().hideKeyboard(this);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (clearBackStack) {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        switch (fragmentName) {
            case MainFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof MainFragment)
                    return;
                fragment = new MainFragment();
                if (isBottomSelected)
                    setBottomNavigationItemSelected(0);

                break;
            case ShopNowFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof ShopNowFragment)
                    return;
                fragment = new ShopNowFragment();
                if (isBottomSelected)
                    setBottomNavigationItemSelected(1);
                break;
            case BrandsFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof BrandsFragment)
                    return;
                fragment = new BrandsFragment();
                if (isBottomSelected)
                    setBottomNavigationItemSelected(2);

                break;
            case HolidaysFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof HolidaysFragment)
                    return;
                fragment = new HolidaysFragment();
                if (isBottomSelected)
                    setBottomNavigationItemSelected(3);
                break;
            case MyAccountFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof MyAccountFragment)
                    return;
                fragment = new MyAccountFragment();
                if (isBottomSelected)
                    setBottomNavigationItemSelected(4);
                break;
            case ProductListFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof ProductListFragment)
                    return;
                fragment = new ProductListFragment();
                break;
            case CartInfoFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof CartInfoFragment)
                    return;
                fragment = new CartInfoFragment();
                break;
            case MyOrdersFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof MyOrdersFragment)
                    return;
                fragment = new MyOrdersFragment();
                break;

            case AboutUsFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof AboutUsFragment)
                    return;
                fragment = new AboutUsFragment();
                break;

            case SecurityPayment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof SecurePaymentFragment)
                    return;
                fragment = new SecurePaymentFragment();
                break;

            case ShippingPolicy:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof ShippingPolicy)
                    return;
                fragment = new ShippingPolicy();
                break;

            case PrivacyPolicy:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof PrivacyPolicy)
                    return;
                fragment = new PrivacyPolicy();
                break;
            case ContactUsFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof ContactUsFragment)
                    return;
                fragment = new ContactUsFragment();
                break;
            case TestimonialFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof TestimonialFragment)
                    return;
                fragment = new TestimonialFragment();
                break;
            case OrderDetailsFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof OrderDetailsFragment)
                    return;
                fragment = new OrderDetailsFragment();
                break;
            case TermsConditionsFragment:
                if (ApplicationContext.getInstance().getLiveFragment() != null && ApplicationContext.getInstance().getLiveFragment() instanceof TermsConditionsFragment)
                    return;
                fragment = new TermsConditionsFragment();
                break;
        }

//        postCartApiCall();          // this method update your cart value on toolbar
        // if user passes the @bundle in not null, then can be added to the fragment
        if (bundle != null) {
            try {
                fragment.setArguments(bundle);
            } catch (Exception e) {
                System.out.println(fragment.getArguments());  // print exception
            }
        }

        // this is for the very first fragment not to be added into the back stack.
        if (!isFirst) {
            ft.addToBackStack(fragment.getClass().getName() + "");
        } else {
            // TODO Need to clear back stack
//            for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
//                fm.popBackStack();
//            }
        }

        fragment.fragmentType = fragmentName.getValue();

        ft.replace(R.id.rl_workSpace, fragment);
        ft.commit();

    }

    public void setNavigationHeader(String imagePath, String userName, String mailId) {
        if (imagePath.length() >= 1)
            ApplicationContext.getInstance().loadImage(imagePath, imageView, null, R.drawable.no_image);
        if (userName.length() >= 1)
            txt_heading_name.setText(userName);
        if (mailId.length() >= 1)
            txt_heading_mail.setText(mailId);

    }

    public void setNavigationHeader() {
        if (PrefSetup.getInstance().getUserInfo() == null || PrefSetup.getInstance().getUserInfo().getResponsedata() == null)
            return;
        try {
            LoginResponse.Responsedata userInfo = PrefSetup.getInstance().getUserInfo().getResponsedata();
            ApplicationContext.getInstance().loadImage(userInfo.getProfile_pic(), imageView, null, R.drawable.no_image);
            txt_heading_name.setText(userInfo.getFirst_name() + " " + userInfo.getLast_name());
            txt_heading_mail.setText(userInfo.getEmail());
        } catch (Exception e) {
            ApplicationContext.getInstance().catchException(e);
        }

    }

    public void setHeader(int image, String headingText) {
        if (image != 0) {
            img_heading_ma.setVisibility(View.VISIBLE);
            txt_heading_name_ma.setVisibility(View.GONE);
        } else {
            img_heading_ma.setVisibility(View.GONE);
            txt_heading_name_ma.setVisibility(View.VISIBLE);
            txt_heading_name_ma.setText(headingText);
        }
    }

    public boolean isValidUser() {
        if (PrefSetup.getInstance().getUserInfo() != null)
            return true;
        PopupUtils.getInstance().showTwoButtonDialogNoCancelable(this, getString(R.string.alert),
                getString(R.string.login_error), getString(R.string.cancel), getString(R.string.login), (object, position) -> {
                    if (position == 1)
                        Logout();
                });
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(ApplicationContext.TAG, "Done");
        if (ApplicationContext.getInstance().getLiveFragment() instanceof MyAccountFragment && (requestCode == CAPTURE_PHOTO || requestCode == SELECT_PHOTO)) {
            ApplicationContext.getInstance().getLiveFragment().onActivityResult(requestCode, resultCode, data);
        } else if (ApplicationContext.getInstance().getLiveFragment() instanceof TestimonialFragment && (requestCode == CAPTURE_PHOTO || requestCode == SELECT_PHOTO)) {
            ApplicationContext.getInstance().getLiveFragment().onActivityResult(requestCode, resultCode, data);
        }
    }

    public void postCartApiCall() {
        ApiCall.getInstance().postCartCountApiCall(this, false, this::onCartResponse);
    }

    private CartInfoResponse cartInfoResponse;
    private List<CartInfoResponse.Cart> cartList;

    private void onCartResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (cartList != null)
            cartList.clear();
        if (isSuccess) {
            cartInfoResponse = (CartInfoResponse) o;
            cartList = cartInfoResponse.getResponsedata().getCart();
        }
        if (cartList == null || cartList.size() == 0) {
            updateCartcount(0);
        } else {
            int cartSize = 0;
            for (CartInfoResponse.Cart cart : cartList) {
                cartSize += cart.getQuantity();
            }
            updateCartcount(cartSize);
        }
    }
}

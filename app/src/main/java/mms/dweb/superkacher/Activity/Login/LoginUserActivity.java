package mms.dweb.superkacher.Activity.Login;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.ErrorType;
import mms.dweb.base.MarshMallowPermission;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.FacebookLoginRequest;
import mms.dweb.web_services.requestBean.GoogleLoginRequest;
import mms.dweb.web_services.requestBean.LoginRequest;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_SHORT;

public class LoginUserActivity extends BaseAppCompatActivity {

//  START---  FACEBOOK & Google Login Initialization-----------------------------------------------------------------------------------------------------------------------------

    //a constant for detecting the login intent result
    private static final int RC_SIGN_IN = 234;
    private int google_request = 2;
    private int facebook_request = 1;
    //creating a GoogleSignInClient object
    GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private CallbackManager mCallbackManager;
    private int switcher = 0;


    @OnClick(R.id.txt_google_action_la)
    void click_txt_google_action_la() {
        // TODO: 6/14/2018 on google click
        Glogin();
        switcher = google_request;

    }

    @OnClick(R.id.txt_facebook_action_la)
    void click_txt_facebook_action_la() {
        // TODO: 6/14/2018 on facebook click
        fblogin();
        switcher = facebook_request;
    }

//   END---- FACEBOOK & Google Login Initialization-----------------------------------------------------------------------------------------------------------------------------


    @OnClick(R.id.txt_login_action_LA)
    void click_txt_login_action_LA() {
        if (isValidLogin())
            postUserLoginApiCall();
    }

    @Override
    public void showHashKey() {
        /*String sha1 = "B7:B2:49:D9:5B:58:F5:2D:8C:18:B4:0A:F9:F9:72:55:84:37:B7:77";
        String[] arr = sha1.split(":");
        byte[] byteArr = new byte[arr.length];

        for (int i = 0; i < arr.length; i++) {
            byteArr[i] = Integer.decode("0x" + arr[i]).byteValue();
        }

        Log.e("hashKey: ", Base64.encodeToString(byteArr, Base64.NO_WRAP));
        Log.e("hash : ", Base64.encodeToString(byteArr, Base64.NO_WRAP));*/

        // GOOGLE PLAY APP SIGNING SHA-1 KEY:- 65:5D:66:A1:C9:31:85:AB:92:C6:A2:60:87:5B:1A:DA:45:6E:97:EA
//        B7:B2:49:D9:  5B:58:F5:2D:                                                8C:18:B4:0A:                            F9:F9:72:55:                        84:37:B7:77
        byte[] sha1 = {
                (byte) 0xB7, (byte) 0xB2, 0x49, (byte)0xD9, (byte)0x5B, 0x58, (byte) 0xF5, (byte)0x2D, (byte)0x8C, 0x18, (byte)0xB4, (byte)0x0A, (byte) 0xF9, (byte) 0xF9, 0x72, 0x55, (byte) 0x84, 0x37, (byte)0xB7, (byte)0x77
        };
        System.out.println("keyhashGooglePlaySignIn:"+ Base64.encodeToString(sha1, Base64.NO_WRAP));

    }

    @BindView(R.id.et_password_LA)
    EditText et_password_LA;
    @BindView(R.id.et_username_LA)
    EditText et_username_LA;

    @BindView(R.id.txt_continue_action)
    TextView txt_continue_action;

    @OnClick(R.id.txt_continue_action)
    void click_txt_continue_action() {
        startMainActivity();
    }

    @BindView(R.id.txt_forgot_password_LA)
    TextView txt_forgot_password_action;

    @OnClick(R.id.txt_forgot_password_LA)
    void click_txt_forgot_password_action() {
        startForgot_Password_Activity();
    }

    @BindView(R.id.layout_signUp_LA)
    LinearLayout layout_signUp_action;

    @OnClick(R.id.layout_signUp_LA)
    void click_layout_signUp_action() {
        startRegistration_Activity();
    }

    private void startForgot_Password_Activity() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    private void startRegistration_Activity() {
        startActivity(new Intent(this, RegistrationActivity.class));
    }


    private void startMainActivity() {
        PrefSetup.getInstance().IsVirgin(false);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_user);
        ButterKnife.bind(this);

//  START---  FACEBOOK & Google Login Initialization-----------------------------------------------------------------------------------------------------------------------------
        mAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        //Then we need a GoogleSignInOptions object And we need to build it as below
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        //Then we will get the GoogleSignInClient object from GoogleSignIn class
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        iniFB();
//   END---- FACEBOOK & Google Login Initialization-----------------------------------------------------------------------------------------------------------------------------

        initializeView();

    }


    @Override
    protected void onStart() {
        super.onStart();

//        showHashKey();

    }

    @Override
    public void initializeView() {
        super.initializeView();
        ApplicationContext.getInstance().clearFragment();
        Utilities.getInstance().checkCameraPermission(this);
        if (!PrefSetup.getInstance().getIsVirgin()) {
            startMainActivity();
        }

    }

    private void postUserLoginApiCall() {
        ApiCall.getInstance().postUserLoginApiCall(this, loginRequest, true, this::onLoginResponse);
    }

    private void onLoginResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            startMainActivity();
        }
    }

    private LoginRequest loginRequest;

    private boolean isValidLogin() {
        if (et_username_LA.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.alert), getString(R.string.email_id_or_username_required), (object, position) -> {

            });
            return false;
        } else if (et_password_LA.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.alert), getString(R.string.password_required), (object, position) -> {

            });
            return false;
        }

        if (loginRequest == null)
            loginRequest = new LoginRequest();
        loginRequest.setPassword(et_password_LA.getText().toString().trim());
        loginRequest.setUsername(et_username_LA.getText().toString().trim());
        return true;
    }


//  START---  FACEBOOK & Google Login Classes------------------------------------------------------------------------------------------------------------------------------------

    public void iniFB() {

        LoginManager.getInstance().registerCallback(
                mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // Handle success
                        Log.d("facebook:onSuccess", " : " + loginResult);
                        handleFacebookAccessToken(loginResult.getAccessToken());

                    }

                    @Override
                    public void onCancel() {

                        Log.d("Login Class : ", "facebook:onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {

                        Log.d("Login Class : ", "facebook:onError", exception);
                    }
                }
        );
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // START on_activity_result
        super.onActivityResult(requestCode, resultCode, data);
        if (switcher == facebook_request) {


            // Pass the activity result back to the Facebook SDK
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (switcher == google_request) {


            //if the requestCode is the Google Sign In code that we defined at starting
            if (requestCode == RC_SIGN_IN) {

                //Getting the GoogleSignIn Task
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    //Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    if (account != null) {
                        String[] arr = account.getDisplayName().split(" ");
                        if (googleLoginRequest == null)
                            googleLoginRequest = new GoogleLoginRequest();
                        googleLoginRequest.setEmail(account.getEmail());
                        googleLoginRequest.setGooglepluseid(account.getId());
                        googleLoginRequest.setFirstname(arr[0]);
                        googleLoginRequest.setLastname(arr.length > 1 ? arr[1] : "");
                        postGoogleLoginApiCall();
//                        Toast.makeText(this, "Login Sucessful as : " + account.getDisplayName(), Toast.LENGTH_SHORT).show();
                    }

                    //authenticating with firebase
//                    firebaseAuthWithGoogle(account);

                } catch (ApiException e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
                    GoogleSignInClient gAuth = GoogleSignIn.getClient(this, gso);
                    gAuth.signOut();
                }
            }

        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        // START auth_with_facebook

        Log.d("Login Class : ", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Login Class : ", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            String[] arr = mAuth.getCurrentUser().getDisplayName().split(" ");
                            if (facebookLoginRequest == null)
                                facebookLoginRequest = new FacebookLoginRequest();
                            facebookLoginRequest.setFacebookid(mAuth.getCurrentUser().getUid());
                            facebookLoginRequest.setFirstname(arr[0]);
                            facebookLoginRequest.setLastname(arr.length > 1 ? arr[1] : "");
                            facebookLoginRequest.setEmail(mAuth.getCurrentUser().getEmail());
                            postFacebookLoginApiCall();
                            Toast.makeText(LoginUserActivity.this, "Login Sucessful as : " + user.getDisplayName(), Toast.LENGTH_SHORT).show();
                        } else {
                            PopupUtils.getInstance().showOkButtonDialogNoCancelable(LoginUserActivity.this, getString(R.string.alert), getString(R.string.Auth_failed), (object, position) -> {

                            });
                            // If sign in fails, display a message to the user.
//                            Log.w("Login Class : ", "signInWithCredential:failure", task.getException());
//                            Toast.makeText(getApplicationContext(), "Authentication failed.",
//                                    LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void fblogin() {

        LoginManager.getInstance().logInWithReadPermissions(this,
                Arrays.asList("email", "public_profile")
        );

    }


    private void Glogin() {

        //getting the google signin intent
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        //starting the activity for result
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private void Glogout() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mAuth.signOut();
                        Toast.makeText(LoginUserActivity.this, "Sign Out Sucess", LENGTH_SHORT).show();
                    }
                });
    }

    public void fblogout() {
        mAuth.signOut();
        LoginManager.getInstance().logOut();
    }

//   END---- FACEBOOK & Google Login Classes------------------------------------------------------------------------------------------------------------------------------

    private GoogleLoginRequest googleLoginRequest;

    private void postGoogleLoginApiCall() {

        ApiCall.getInstance().postGoogleLoginApiCall(this, googleLoginRequest, true, this::OnGoogleLoginResponse);

    }

    private FacebookLoginRequest facebookLoginRequest;

    private void postFacebookLoginApiCall() {

        ApiCall.getInstance().postFacebookLoginApiCall(this, facebookLoginRequest, true, this::OnFacebookResponse);

    }

    private void OnFacebookResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType type, Object o) {
        if (isSuccess) {
            startMainActivity();
        } else {
            LoginManager.getInstance().logOut();
        }
    }

    private void OnGoogleLoginResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            startMainActivity();
        } else {
            googlelogout();
        }
    }

    private void googlelogout() {
        mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        GoogleSignInClient gAuth = GoogleSignIn.getClient(this, gso);
        gAuth.signOut();
    }


}

package mms.dweb.superkacher.Activity.OrderDetails;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.MyOrderInfoAdapter;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.OrderDetailsRequest;
import mms.dweb.web_services.responseBean.MyOrdersResponse;
import mms.dweb.web_services.responseBean.OrderDetailsResponse;
import mms.dweb.web_services.responseBean.ProductListResponse;
import retrofit2.Response;

public class OrderDetailsActivity extends BaseAppCompatActivity {

    @BindView(R.id.txt_address_oda)
    TextView txt_address_oda;

    @BindView(R.id.rcv_orderlist_oda)
    RecyclerView rcv_orderlist_oda;

    @BindView(R.id.current_order_info)
    TextView current_order_info;

    @BindView(R.id.txt_total_oda)
    TextView txt_total_oda;
    @BindView(R.id.txt_discount_oda)
    TextView txt_discount_oda;
    @BindView(R.id.txt_shipping_oda)
    TextView txt_shipping_oda;
    @BindView(R.id.txt_subtotal_oda)
    TextView txt_subtotal_oda;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_ca);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        }
        toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });
        getSupportActionBar().setTitle(getString(R.string.order_details));
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeView();
    }

    private Intent intent;
    private MyOrdersResponse.Responsedata productData;

    @Override
    public void initializeView() {
        super.initializeView();
        Utilities.getInstance().setRecyclerViewUISpanCount(this, rcv_orderlist_oda, 1, DoActionType.Vertical);
        intent = this.getIntent();
        productData = (MyOrdersResponse.Responsedata) intent.getSerializableExtra("order");
        if (productData != null)
            postOrdersDetailsApiCall();
    }

    private void postOrdersDetailsApiCall() {
        OrderDetailsRequest orderDetailsRequest = new OrderDetailsRequest();
        orderDetailsRequest.setOrderId(String.valueOf(productData.getId()));
        ApiCall.getInstance().postOrdersDetailsApiCall(this, orderDetailsRequest, true, this::OnOrderDetailsResponse);
    }

    private OrderDetailsResponse orderDetailsResponse;

    private OrderDetailsResponse.Responsedata responsedata;

    private void OnOrderDetailsResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            orderDetailsResponse = (OrderDetailsResponse) o;
            manageData();
        }
    }

    private void manageData() {
        if (orderDetailsResponse == null)
            return;
        responsedata = orderDetailsResponse.getResponsedata();
        manageAdapter();
        OrderDetailsResponse.Billing billing = responsedata.getBilling();
        txt_address_oda.setText(billing.getFirstName() + " " + billing.getLastName() + "\n" +
                (billing.getCompany().isEmpty() ? "" : (billing.getCompany() + "\n")) +
                billing.getAddress1() + "\n" +
                (billing.getAddress2().isEmpty() ? "" : (billing.getAddress2() + "\n")) +
                billing.getCity() + "\n" +
                billing.getState() + " " +
                billing.getPostcode() + "\n" +
                billing.getCountry() + "\n" +
                billing.getPhone() + "\n" +
                billing.getEmail()
        );

        String heading = "Order #" + responsedata.getId() + " was placed on " +
                responsedata.getDateCreated() + " and is currently " + responsedata.getStatus() + " payment.";
        current_order_info.setText(heading);

        double grandTotal = Double.parseDouble(responsedata.getTotal()) + Double.parseDouble(responsedata.getShippingTotal())
                - Double.parseDouble(responsedata.getDiscountTotal());
        txt_total_oda.setText("$" + responsedata.getTotal());
        txt_discount_oda.setText("$" + responsedata.getDiscountTotal());
        txt_shipping_oda.setText("$" + responsedata.getShippingTotal());
        txt_subtotal_oda.setText("$" + new DecimalFormat("##.##").format(grandTotal));
    }

    private MyOrderInfoAdapter adapter;

    private void manageAdapter() {
        List<OrderDetailsResponse.Lineitems> lineList = responsedata.getLineitems();
        if (lineList == null || lineList.size() == 0)
            return;
        adapter = new MyOrderInfoAdapter(this, lineList, (object, position) -> {

        });
        rcv_orderlist_oda.setAdapter(adapter);
    }
}

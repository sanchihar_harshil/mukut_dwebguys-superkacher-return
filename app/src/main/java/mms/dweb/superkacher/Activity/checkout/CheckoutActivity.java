package mms.dweb.superkacher.Activity.checkout;

import android.content.Intent;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.stripe.android.CustomerSession;
import com.stripe.android.PaymentConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.base.OnTaskCompletedListener;
import mms.dweb.base.PlaceDetailsJSONParser;
import mms.dweb.stripe.SampleStoreEphemeralKeyProvider;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Activity.Stripe.StripeMainActivity;
import mms.dweb.superkacher.Activity.Stripe.StripePaymentActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.base.FragmentNames;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.LatLong.PlacesAutoCompleteAdapter;
import mms.dweb.superkacher.adapter.OrderInfoAdapter;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.utilities.Constant;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.CouponRequest;
import mms.dweb.web_services.requestBean.CreateOrderRequest;
import mms.dweb.web_services.requestBean.UpsShippingRequest;
import mms.dweb.web_services.responseBean.CheckoutInfoResponse;
import mms.dweb.web_services.responseBean.CouponResponse;
import mms.dweb.web_services.responseBean.CreateOrderResponse;
import mms.dweb.web_services.responseBean.SubmitAddBean;
import mms.dweb.web_services.responseBean.UpsShippingResponse;
import retrofit2.Response;

public class CheckoutActivity extends BaseAppCompatActivity {

    private SubmitAddBean bean = new SubmitAddBean();

    @BindViews({R.id.edit_textFirstName,
            R.id.edit_textLastName,
            R.id.edit_textCompanyName,
//            R.id.edit_textStreetAddress1,
            R.id.edit_textStreetAddress2,
            R.id.edit_textCity,
            R.id.edit_textZip,
            R.id.edit_textPhone,
            R.id.edit_textEmail})
    List<EditText> edit_shippintInfoList;

    @BindView(R.id.txt_textState)
    TextView txt_textState;

    @BindView(R.id.txt_placeOrder_ca)
    TextView txt_placeOrder;

    @BindView(R.id.txtCountry)
    TextView txtCountry;

    @BindView(R.id.edit_textAdditionalInformation)
    EditText edit_textAdditionalInformation;

    @BindView(R.id.edit_textCouponCode)
    TextView edit_textCouponCode;
    @BindView(R.id.txt_ShippingMethod)
    TextView txt_ShippingMethod;
    @BindView(R.id.txt_loc)
    AutoCompleteTextView txt_loc;
    @BindViews({R.id.txt_subtotal_ca,
            R.id.txt_shipping_ca,
            R.id.txt_discount_ca,
            R.id.txt_total_ca
    })
    List<TextView> edit_orderInfoList;

    @BindView(R.id.rcv_orderlist_ca)
    RecyclerView rcv_orderlist_ca;

    @OnClick(R.id.txt_textState)
    void click_txt_textState() {
        manageStateSpinner();
    }

    @OnClick(R.id.txt_placeOrder_ca)
    void click_txt_placeOrder() {
        if (isValid())
//            postCreateOrderApicall();
            startStripActivity();
    }

    @OnClick(R.id.txt_coupon_action_ca)
    void click_txt_coupon_action_ca() {
        if (isValidCoupon())
            postCouponApiCall();
    }

    private boolean isValidCoupon() {
        if (edit_textCouponCode.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.coupon_discount), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        }
        if (couponRequest == null)
            couponRequest = new CouponRequest();
        couponRequest.setCode(edit_textCouponCode.getText().toString().trim());
        return true;
    }

    @OnClick(R.id.txt_ShippingMethod)
    void click_txt_ShippingMethod() {

        manageShippingSpinner();
    }

    private List<String> shippingLabelsList;

    private void manageShippingSpinner() {
        if (upsShippingList == null || upsShippingList.size() == 0) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.shipping_charge), getString(R.string.shipping_address_error), (object, position) -> {

            });
        } else {
            if (shippingLabelsList == null)
                shippingLabelsList = new ArrayList<>();
            shippingLabelsList.clear();
            shippingLabelsList.add(getString(R.string.select_shipping_type));
            for (UpsShippingResponse.Upscharges upscharges : upsShippingList) {
//                    if(!shippingLabelsList.contains(getString(R.string.free_shipping)))
                shippingLabelsList.add(upscharges.getLabel());
            }
//            setShippingInfo(0, "");
            PopupUtils.getInstance().showSpinnerStateDialogNoCancelable(this, getString(R.string.shipping_charge), getString(R.string.select_shipping_type), shippingLabelsList, (object, position) -> {
                if (position != -1) {
                    setShippingInfo(upsShippingList.get(position - 1).getCost(), upsShippingList.get(position - 1).getLabel());
                    shipping = upsShippingList.get(position - 1).getCost();
                    clearCouponCode(upsShippingList.get(position - 1));
                    manageOrderInfoView();
                }
            });
        }
    }

    private void setShippingInfo(double cost, String label) {
        shipping = cost;
        shippingLabel = label;
        txt_ShippingMethod.setText(shippingLabel);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_ca);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        }
        toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });
        getSupportActionBar().setTitle(getString(R.string.checkout));

        PaymentConfiguration.init(Constant.PUBLISHABLE_KEY);
//        shippingLabel = getString(R.string.free_shipping);
        setupCustomerSession();
    }

    private void setupCustomerSession() {
        // CustomerSession only needs to be initialized once per app.
        CustomerSession.initCustomerSession(
                new SampleStoreEphemeralKeyProvider(
                        new SampleStoreEphemeralKeyProvider.ProgressListener() {
                            @Override
                            public void onStringResponse(String string) {
                                if (string.startsWith("Error: ")) {
                                    new AlertDialog.Builder(CheckoutActivity.this).setMessage(string).show();
                                }
                            }
                        }));
    }


    @Override
    protected void onStart() {
        super.onStart();
        initializeView();
    }


    @Override
    public void initializeView() {
        super.initializeView();
        Utilities.getInstance().setRecyclerViewUISpanCount(this, rcv_orderlist_ca, 1, DoActionType.Vertical);
        postCheckoutInfoApiCall();
        hideKeyBoard();
//        edit_shippintInfoList.get(0).setText(PrefSetup.getInstance().getUserInfo().getResponsedata().getFirst_name());         // First Name
//        edit_shippintInfoList.get(1).setText(PrefSetup.getInstance().getUserInfo().getResponsedata().getLast_name());         // Last Name
//        edit_shippintInfoList.get(7).setText(PrefSetup.getInstance().getUserInfo().getResponsedata().getEmail());         // email id
        edit_shippintInfoList.get(4).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                shippingInfo.setBillingCity(editable.toString());
                postUpsShippingApiCall();
            }
        });

        edit_shippintInfoList.get(5).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                shippingInfo.setBillingPostcode(editable.toString());
                postUpsShippingApiCall();
            }
        });

        placeAutocompliteView();
    }

    private void placeAutocompliteView() {
        final PlacesAutoCompleteAdapter place_adapter = new PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list);
        txt_loc.setAdapter(place_adapter);
        txt_loc.setThreshold(1);
        txt_loc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                try {
                    JSONObject jObj = place_adapter.getJSON(position);
                    if (Utilities.getInstance().isOnline(CheckoutActivity.this)) {
                        DownloadTaskRG placeDetailsDownloadTask = new DownloadTaskRG(object -> {
                            bean = (SubmitAddBean) object;
                            if (bean == null)
                                return;
//                            txt_loc.setText(bean.getAddress1());
                            edit_shippintInfoList.get(3).setText(bean.getSubLocality());
                            edit_shippintInfoList.get(4).setText(bean.getLocality());
                            edit_shippintInfoList.get(5).setText(bean.getZip());
                        });
                        String url = Utilities.getInstance().getPlaceDetailsUrl(jObj.getString("place_id"));
                        placeDetailsDownloadTask.execute(url);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void hideKeyBoard() {
        Utilities.getInstance().hideKeyboard(this);
    }

    private void postCheckoutInfoApiCall() {
        ApiCall.getInstance().postCheckoutInfoApiCall(this, true, this::OnCheckoutResponse);
    }

    private CheckoutInfoResponse checkoutInfoResponse;
    private List<CheckoutInfoResponse.Cart> checkoutList;
    private List<String> statesList = new ArrayList<>();

    private void OnCheckoutResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            checkoutInfoResponse = (CheckoutInfoResponse) o;
            checkoutList = checkoutInfoResponse.getResponsedata().getCart();
            statesList = checkoutInfoResponse.getResponsedata().getStates();
            manageData();
        }
    }

    private void manageData() {
        if (checkoutList != null && checkoutList.size() != 0) {
            manageShippingInfoView();
            manageOrderInfoView();
            setAdapter();
        }
    }

    private OrderInfoAdapter adapter;

    private void setAdapter() {
        adapter = new OrderInfoAdapter(this, checkoutList, (object, position) -> {

        });
        rcv_orderlist_ca.setAdapter(adapter);
    }

    private void updateList() {
        if (adapter != null)
            adapter.setDataList(checkoutList);
    }

    private double subTotal = 0;
    private double shipping = 0;
    private double coupon = 0;
    private String couponId;
    private String shippingLabel;
    private double total = 0;

    private void manageFreeShipping() {
        setShippingInfo(0, "");
        setCouponDetails(0, "");
        if (!upsShippingList.contains(getString(R.string.free_shipping))) {
            tempShipping = new UpsShippingResponse.Upscharges();
            tempShipping.setLabel(getString(R.string.free_shipping));
            tempShipping.setCost(0.00);
            upsShippingList.add(tempShipping);
        }
        manageOrderInfoView();
    }

    private void clearCouponCode(UpsShippingResponse.Upscharges upscharges) {
        couponId = upscharges.getId();
        edit_textCouponCode.setText(
                upscharges.getLabel().equals(getString(R.string.free_shipping)) ?
                        getString(R.string.free_shipping) : "");
    }

    private void manageOrderInfoView() {
        subTotal = 0;
        for (CheckoutInfoResponse.Cart cart : checkoutList) {
            subTotal += (cart.getQuantity() * Double.parseDouble(cart.getPrice()));
        }
        total = subTotal + shipping - coupon;
        edit_orderInfoList.get(0).setText("$" + new DecimalFormat("##.##").format(subTotal));            // subtotal
        edit_orderInfoList.get(1).setText("$" + new DecimalFormat("##.##").format(shipping));            // shipping charge
        edit_orderInfoList.get(2).setText("$" + new DecimalFormat("##.##").format(coupon));            // COUPON DISCOUNT
        edit_orderInfoList.get(3).setText("$" + new DecimalFormat("##.##").format(total <= 0 ? 0 : total));            // shipping charge
    }

    private CheckoutInfoResponse.BillingDetails shippingInfo;

    private void manageShippingInfoView() {

        shippingInfo = checkoutInfoResponse.getResponsedata().getBillingDetails();
        if (shippingInfo == null)
            shippingInfo = new CheckoutInfoResponse.BillingDetails();
        edit_shippintInfoList.get(0).setText((shippingInfo.getBillingFirstName()==null
                || shippingInfo.getBillingFirstName().isEmpty()) ?
                PrefSetup.getInstance().getUserInfo().getResponsedata().getFirst_name() :
                shippingInfo.getBillingFirstName());         // First name
        edit_shippintInfoList.get(1).setText((shippingInfo.getBillingLastName()==null
                || shippingInfo.getBillingLastName().isEmpty())?
                PrefSetup.getInstance().getUserInfo().getResponsedata().getLast_name() :
                shippingInfo.getBillingLastName());         // Last name
        edit_shippintInfoList.get(2).setText(shippingInfo.getBillingCompany());         // Company name
        txt_loc.setText(shippingInfo.getBillingAddress1());
//        edit_shippintInfoList.get(3).setText(shippingInfo.getBillingAddress1());         // Address 1
        edit_shippintInfoList.get(3).setText(shippingInfo.getBillingAddress2());         // Address 2
        edit_shippintInfoList.get(4).setText(shippingInfo.getBillingCity());         // City name
        edit_shippintInfoList.get(5).setText(shippingInfo.getBillingPostcode());         // zip code
        edit_shippintInfoList.get(6).setText(shippingInfo.getBillingPhone());         // phone number
        edit_shippintInfoList.get(7).setText((shippingInfo.getBillingEmail()==null
                || shippingInfo.getBillingEmail().isEmpty() )?
                PrefSetup.getInstance().getUserInfo().getResponsedata().getEmail() :
                shippingInfo.getBillingEmail());         // email id
        txtCountry.setText(getString(R.string.unitedstate));         // Country name
        txt_textState.setText(shippingInfo.getBillingState());         // state name
        postUpsShippingApiCall();
    }

    private UpsShippingRequest upsShippingRequest;

    private void postUpsShippingApiCall() {
        if (upsShippingRequest == null)
            upsShippingRequest = new UpsShippingRequest();
        upsShippingRequest.setCity(shippingInfo.getBillingCity());
        upsShippingRequest.setPostcode(shippingInfo.getBillingPostcode());
        upsShippingRequest.setState(shippingInfo.getBillingState());
        ApiCall.getInstance().postUpsShippingApiCall(this, upsShippingRequest, false, this::onUpsShippingResponse);
    }

    private UpsShippingResponse upsShippingResponse;
    private List<UpsShippingResponse.Upscharges> upsShippingList;
    private UpsShippingResponse.Upscharges tempShipping;

    private void onUpsShippingResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            upsShippingResponse = (UpsShippingResponse) o;
            upsShippingList = upsShippingResponse.getResponsedata().getUpscharges();
            updateShipping();
            if (tempShipping != null)
                upsShippingList.add(tempShipping);
            manageUpsShippingListData();
        } else if (tempShipping != null) {
            upsShippingList.add(tempShipping);
            manageUpsShippingListData();
        }
    }

    private void updateShipping() {
        if (upsShippingList.size() == 0) {
            setShippingInfo(0, getString(R.string.free_shipping));
        } else {
            setShippingInfo(0, "");
        }
    }

    private void manageUpsShippingListData() {
        // TODO: 6/19/2018 manage ups list on spinner
    }

    private void manageStateSpinner() {
        if (statesList == null || statesList.size() == 0) {
            makeToast(getString(R.string.noDataFound));
            return;
        } else {
            PopupUtils.getInstance().showSpinnerStateDialogNoCancelable(CheckoutActivity.this,
                    getString(R.string.state), getString(R.string.select_state), statesList, (object, position) -> {
                        if (position != -1) {
                            shippingInfo.setBillingState(statesList.get(position));
                            txt_textState.setText(statesList.get(position));
                            postUpsShippingApiCall();
                        }
                    });
        }

    }

    private CouponRequest couponRequest;

    private void postCouponApiCall() {
        ApiCall.getInstance().postCouponApiCall(this, couponRequest, true, this::OnCouponResponse);
    }

    private CouponResponse couponResponse;

    private void OnCouponResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            couponResponse = (CouponResponse) o;
            if (couponResponse.getResponsedata().get(0).getDescription().equals(getString(R.string.free_shipping))) {
                manageFreeShipping();
            } else {
                tempShipping = null;
                setCouponDetails(couponResponse.getResponsedata().get(0).getCost(), String.valueOf(couponResponse.getResponsedata().get(0).getId()));
                manageOrderInfoView();
            }
        }
    }

    private void setCouponDetails(double cost, String id) {
        coupon = cost;
        couponId = id;
    }

    /**
     * A method to download json data from url
     */
    public class DownloadTaskRG extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        String data = null;
        private OnTaskCompletedListener listener;
        private double latitude;
        private double longitude;
        private List<Address> addressList;
        private LatLng temp;
        private SubmitAddBean addressBean = new SubmitAddBean();

        public DownloadTaskRG(OnTaskCompletedListener listener) {
            this.listener = listener;
        }

        @Override
        protected List<HashMap<String, String>> doInBackground(String... url) {
            JSONObject jObj = new JSONObject();
            try {
                data = Utilities.getInstance().downloadUrl(url[0]);
                jObj = new JSONObject(data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
//            List<HashMap<String, String>> obj = PlaceDetailsJSONParser.parse(jObj);
            return PlaceDetailsJSONParser.parse(jObj);
        }


        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            if (result != null) {
                try {
                    temp = new LatLng(Double.parseDouble(result.get(0).get("lat")), Double.parseDouble(result.get(0).get("lng")));
                    latitude = Double.parseDouble(result.get(0).get("lat"));
                    longitude = Double.parseDouble(result.get(0).get("lng"));
                    addressList = Utilities.getInstance().getAddressInList(CheckoutActivity.this, latitude, longitude);
                    addressBean.setLatitude(String.valueOf(latitude));
                    addressBean.setLongitude(String.valueOf(longitude));
                    addressBean.setAddress1(txt_loc.getText().toString().trim());
                    addressBean.setAddress2(addressList.get(0).getLocality());
                    addressBean.setSubLocality(addressList.get(0).getSubLocality());
                    addressBean.setLocality(addressList.get(0).getLocality());
                    addressBean.setZip(addressList.get(0).getPostalCode());
                    listener.onTaskCompleted(addressBean);
                    Log.e("LAT LONG - ", temp.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                return;
            }
        }
    }


    private void postCreateOrderApicall() {
        ApiCall.getInstance().postCreateOrderApicall(this, createOrderRequest, true, this::onOrderCreate);
    }

    CreateOrderResponse createOrderResponse;

    private void onOrderCreate(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType type, Object o) {
        if (isSuccess) {
            createOrderResponse = (CreateOrderResponse) o;
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.checkout),
                    createOrderResponse.getResponsemessage(), (object, position) -> {
                        PrefSetup.getInstance().setOrderInfo(createOrderResponse);
                        Log.e(ApplicationContext.TAG, PrefSetup.getInstance().getOrderInfo().toString());
                        startActivity();
                    });
        }
    }

    private void startActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("fragmentName", FragmentNames.MyOrdersFragment);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private CreateOrderRequest createOrderRequest;

    private boolean isValid() {
        String Fname = edit_shippintInfoList.get(0).getText().toString().trim();
        String Lname = edit_shippintInfoList.get(1).getText().toString().trim();
        String Cname = edit_shippintInfoList.get(2).getText().toString().trim();
        String Address1 = txt_loc.getText().toString().trim();
        String Address2 = edit_shippintInfoList.get(3).getText().toString().trim();
        String City = edit_shippintInfoList.get(4).getText().toString().trim();
        String MyState = txt_textState.getText().toString().trim();
        String ZipCode = edit_shippintInfoList.get(5).getText().toString().trim();
        String Country = txtCountry.getText().toString().trim();
        String MyPhone = edit_shippintInfoList.get(6).getText().toString().trim();
        String Email = edit_shippintInfoList.get(7).getText().toString().trim();
        String Additional = edit_textAdditionalInformation.getText().toString().trim();

        if (Fname.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.first_name),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;
        } else if (Lname.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.last_name),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;

        /*} else if (Cname.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.company_name),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;*/

        } else if (Country.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.country),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;

        } else if (Address1.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.street_address_1),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;

        } else if (City.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.town_city),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;

        } else if (MyState.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.mystate),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;

        } else if (ZipCode.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.zip),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;

        } else if (MyPhone.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.phone_number),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;

        } else if (Email.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.email_address),
                    getString(R.string.noDataFound), (object, position) -> {

                    });
            return false;

        } else if (!Utilities.getInstance().isValidEmail(Email)) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.email_address),
                    getString(R.string.mail_error), (object, position) -> {

                    });
            return false;

        } else if (upsShippingList != null && upsShippingList.size() > 0 && shippingLabel.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.title_shipping),
                    getString(R.string.select_shipping_type), (object, position) -> {

                    });
            return false;
        }

        if (createOrderRequest == null)
            createOrderRequest = new CreateOrderRequest();
        createOrderRequest.setFirstName(Fname);
        createOrderRequest.setLastName(Lname);
        createOrderRequest.setBillingCompany(Cname);
        createOrderRequest.setAddress1(Address1);
        createOrderRequest.setAddress2(Address2);
        createOrderRequest.setCity(City);
        createOrderRequest.setState(MyState);
        createOrderRequest.setPostcode(ZipCode);
        createOrderRequest.setCountry(Country);
        createOrderRequest.setPhone(MyPhone);
        createOrderRequest.setEmail(Email);
        createOrderRequest.setAdditionalInfo(Additional);
        createOrderRequest.setTransactionId("");              // TODO: 7/6/2018 create transection id
        createOrderRequest.setCouponCode(couponId);
        createOrderRequest.setMethodName(shippingLabel);
        createOrderRequest.setCost(String.valueOf(shipping));
//        createOrderRequest.setFee();
        return true;
    }

    private void startStripActivity() {
        Intent intent = new Intent(this, PaymentActivity.class);
//        Intent intent = new Intent(this, StripePaymentActivity.class);
        intent.putExtra("Order", createOrderRequest);
        intent.putExtra("total", total);
        startActivity(intent);
    }
}

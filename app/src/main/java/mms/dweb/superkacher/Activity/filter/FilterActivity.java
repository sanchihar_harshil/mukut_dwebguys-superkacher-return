package mms.dweb.superkacher.Activity.filter;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.responseBean.ShortingProductResponse;
import retrofit2.Response;

public class FilterActivity extends BaseAppCompatActivity {
    @BindView(R.id.cat_spinner)
    Spinner cat_spinner;
    @BindView(R.id.tag_spinner)
    Spinner tag_spinner;

    @OnClick(R.id.txt_reset_action_fa)
    void click_txt_reset_action_fa() {
        PrefSetup.getInstance().setFilterCategory(selectedCat);
        PrefSetup.getInstance().setFilterTag(selectedTag);
        Log.e(ApplicationContext.TAG, PrefSetup.getInstance().getFilterCategory().toString());
        Log.e(ApplicationContext.TAG, PrefSetup.getInstance().getFilterTag().toString());
    }

    @OnClick(R.id.txt_apply_action_fa)
    void click_txt_apply_action_fa() {
        PrefSetup.getInstance().setFilterCategory(new ShortingProductResponse.Categories());
        PrefSetup.getInstance().setFilterTag(new ShortingProductResponse.Tags());
        Log.e(ApplicationContext.TAG, PrefSetup.getInstance().getFilterCategory().toString());
        Log.e(ApplicationContext.TAG, PrefSetup.getInstance().getFilterTag().toString());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_ca);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        }
        toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });
        getSupportActionBar().setTitle(getString(R.string.checkout));
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void initializeView() {
        super.initializeView();
        postShortingProductApiCall();

        tag_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position > 0) {
                    selectedTag = tagsList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        cat_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position > 0) {
                    selectedCat = categoriesList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void postShortingProductApiCall() {
        ApiCall.getInstance().postShortingProductApiCall(this, true, this::OnShortingResponse);
    }

    private ShortingProductResponse shortingProductResponse;
    private List<ShortingProductResponse.Categories> categoriesList;
    private List<ShortingProductResponse.Tags> tagsList;
    private ShortingProductResponse.Tags selectedTag;
    private ShortingProductResponse.Categories selectedCat;

    private void OnShortingResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        initializeList();
        if (isSuccess) {
            shortingProductResponse = (ShortingProductResponse) o;
            categoriesList.addAll(shortingProductResponse.getCategories());
            tagsList.addAll(shortingProductResponse.getTags());
            manageData();
        }
    }

    private void initializeList() {
        if (categoriesList == null)
            categoriesList = new ArrayList<>();
        categoriesList.clear();
        ShortingProductResponse.Categories categories = new ShortingProductResponse.Categories();
        categories.setName("--Select Category--");
        categoriesList.add(categories);
        if (tagsList == null)
            tagsList = new ArrayList<>();
        tagsList.clear();
        ShortingProductResponse.Tags tags = new ShortingProductResponse.Tags();
        tags.setName("--Select Tag--");
        tagsList.add(tags);

    }

    private void manageData() {
        if (categoriesList != null) {
            ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, categoriesList);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            cat_spinner.setAdapter(aa);
        }
        if (tagsList != null) {
            ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tagsList);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            tag_spinner.setAdapter(aa);
        }
    }
}

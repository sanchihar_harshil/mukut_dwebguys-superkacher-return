package mms.dweb.superkacher.Activity.Login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Arrays;

import mms.dweb.superkacher.R;

import static android.widget.Toast.LENGTH_SHORT;


public class LoginActivity extends AppCompatActivity {

    //a constant for detecting the login intent result
    private static final int RC_SIGN_IN = 234;
    //creating a GoogleSignInClient object
    GoogleSignInClient mGoogleSignInClient;

    Button btnfbin,btnfbout,btnGin,btnGout;
    TextView name,status;
    private FirebaseAuth mAuth;
    private CallbackManager mCallbackManager;
    private int switcher=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnfbin=findViewById(R.id.btnfbsignin);
        btnfbout=findViewById(R.id.btnfbsignout);
        btnGin=findViewById(R.id.btnglogin);
        btnGout=findViewById(R.id.btnglogout);
        name=findViewById(R.id.txtname);
        status=findViewById(R.id.txtstatus);

        mAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();

        //Then we need a GoogleSignInOptions object And we need to build it as below
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build();
        //Then we will get the GoogleSignInClient object from GoogleSignIn class
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        iniFB();

        btnfbin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fblogin();
                switcher=1;
            }
        });

        btnfbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fblogout();
                switcher=0;
            }
        });

        btnGin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glogin();
                switcher=2;
            }
        });

        btnGout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glogout();
                switcher=0;
            }
        });


    }



    @Override
    protected void onStart() {
        super.onStart();
        startupuserchecking();
    }

    public void iniFB(){

        LoginManager.getInstance().registerCallback(
                mCallbackManager,
                new FacebookCallback< LoginResult >() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // Handle success
                        Log.d("facebook:onSuccess", " : " + loginResult);
                        handleFacebookAccessToken(loginResult.getAccessToken());

                    }

                    @Override
                    public void onCancel() {

                        Log.d("Login Class : ", "facebook:onCancel");
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }

                    @Override
                    public void onError(FacebookException exception) {

                        Log.d("Login Class : ", "facebook:onError", exception);
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }
                }
        );


    }

    public void fblogin(){

        LoginManager.getInstance().logInWithReadPermissions(this,
                Arrays.asList( "email", "public_profile")
        );

    }

    public void fblogout(){
        mAuth.signOut();
        LoginManager.getInstance().logOut();
        updateUI(null);
    }

    public void startupuserchecking(){
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

        updateUI(currentUser);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // START on_activity_result
        super.onActivityResult(requestCode, resultCode, data);
        if (switcher==1) {


            // Pass the activity result back to the Facebook SDK
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        else if (switcher==2){


            //if the requestCode is the Google Sign In code that we defined at starting
            if (requestCode == RC_SIGN_IN) {

                //Getting the GoogleSignIn Task
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    //Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    if (account !=null) {
                        name.setText(account.getDisplayName());
                        status.setText("Logged In");
                        btnfbin.setVisibility(View.GONE);
                        btnfbout.setVisibility(View.GONE);
                        btnGin.setVisibility(View.GONE);
                        btnGout.setVisibility(View.VISIBLE);
                    }

                    //authenticating with firebase
//                    firebaseAuthWithGoogle(account);

                } catch (ApiException e) {
                    Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        }
    }


    private void handleFacebookAccessToken(AccessToken token) {
        // START auth_with_facebook

        Log.d("Login Class : ", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Login Class : ", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Login Class : ", "signInWithCredential:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Authentication failed.",
                                    LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            if (switcher==1){
                name.setText(user.getDisplayName());
                status.setText("Logged In");
                btnfbin.setVisibility(View.GONE);
                btnfbout.setVisibility(View.VISIBLE);
                btnGin.setVisibility(View.GONE);
                btnGout.setVisibility(View.GONE);
            }

        }
        else {

            btnGin.setVisibility(View.VISIBLE);
            btnGout.setVisibility(View.GONE);
            btnfbout.setVisibility(View.GONE);
            btnfbin.setVisibility(View.VISIBLE);
        }
    }




    private void Glogin() {

        //getting the google signin intent
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        //starting the activity for result
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private void Glogout(){
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mAuth.signOut();
                        Toast.makeText(LoginActivity.this, "Sign Out Sucess", LENGTH_SHORT).show();
                    }
                });
        updateUI(null);
    }



}

package mms.dweb.superkacher.Activity.wishlist;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.Login.LoginActivity;
import mms.dweb.superkacher.Activity.Login.LoginUserActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.WishListAdapter;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.utilities.Systemout;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.AddToCartRequest;
import mms.dweb.web_services.requestBean.AddWishlistProductRequest;
import mms.dweb.web_services.responseBean.ProductListResponse;
import mms.dweb.web_services.responseBean.WishlistResponse;
import retrofit2.Response;

public class WishlistActivity extends BaseAppCompatActivity {

    @BindView(R.id.rcv_wishlist_wa)
    RecyclerView rcv_wishlist_wa;
    @BindView(R.id.ll_null_data)
    LinearLayout ll_null_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_ca);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        }
        toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });
        getSupportActionBar().setTitle(getString(R.string.wishlist));
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void initializeView() {
        super.initializeView();
        Utilities.getInstance().setRecyclerViewUISpanCount(this, rcv_wishlist_wa, 1, DoActionType.Vertical);
        postWishlistProductApiCall();
    }

    private void postWishlistProductApiCall() {
        ApiCall.getInstance().postWishlistProductApiCall(this, true, this::OnWishlistResponse);
    }

    private WishlistResponse wishlistResponse;
    private List<WishlistResponse.Responsedata> wishList; 

    private void OnWishlistResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            wishlistResponse = (WishlistResponse) o;
            wishList = wishlistResponse.getResponsedata();
            manageData();
        }
    }

    private void manageData() {
        if (wishList == null || wishList.size() == 0) {
            hideListView();
        } else {
            showListView();
            setAdapter();
        }
    }

    private WishListAdapter adapter;

    private void setAdapter() {
        adapter = new WishListAdapter(wishList, this, (object, position, type) -> {
            switch (type) {
                case Cart:
                    AddTocartApiCall(wishList.get(position));
                    break;
                case Whishlist:
                    postDeleteWishlistProductApiCall(wishList.get(position), position);
                    break;
            }
        });
        rcv_wishlist_wa.setAdapter(adapter);
    }

    private void updateList() {
        adapter.setData(wishList);
    }


    private void hideListView() {
        ll_null_data.setVisibility(View.VISIBLE);
    }

    private void showListView() {
        ll_null_data.setVisibility(View.GONE);
    }

    private AddWishlistProductRequest addWishlistProductRequest;

    private void postDeleteWishlistProductApiCall(WishlistResponse.Responsedata product, int position) {
        if (addWishlistProductRequest == null)
            addWishlistProductRequest = new AddWishlistProductRequest();
        addWishlistProductRequest.setProductId(String.valueOf(product.getId()));
        ApiCall.getInstance().postDeleteWishlistProductApiCall(this, addWishlistProductRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                makeToast(product.getName() + " has been remove to your Wishlist.");
                wishList.remove(position);
                updateList();
            }
        });
    }

    private void AddTocartApiCall(WishlistResponse.Responsedata responsedata) {
        if (!isValidUser())
            return;
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        addToCartRequest.setProductId(responsedata.getId());
        addToCartRequest.setProductCount(String.valueOf(responsedata.getQuantity()));
        ApiCall.getInstance().postAddToCartApiCall(this, addToCartRequest, true, (isSuccess, response, t, errorType, o) -> {
            Systemout.println(o);
        });
    }

    public boolean isValidUser() {
        if (PrefSetup.getInstance().getUserInfo() != null)
            return true;
        PopupUtils.getInstance().showTwoButtonDialogNoCancelable(this, getString(R.string.alert),
                getString(R.string.login_error), getString(R.string.cancel), getString(R.string.login), (object, position) -> {
                    if (position == 1)
                        startLoginActivity();
                });
        return false;
    }

    private void startLoginActivity() {
        Intent i = new Intent(this, LoginUserActivity.class);
// set the new task and clear flags
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

}

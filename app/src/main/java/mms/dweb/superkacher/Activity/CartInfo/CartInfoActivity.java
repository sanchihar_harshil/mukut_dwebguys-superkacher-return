package mms.dweb.superkacher.Activity.CartInfo;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.checkout.CheckoutActivity;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.CartProductListAdapter;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.AddWishlistProductRequest;
import mms.dweb.web_services.requestBean.RemoveItemRequest;
import mms.dweb.web_services.requestBean.UpdateCartQuaRequest;
import mms.dweb.web_services.responseBean.CartInfoResponse;
import mms.dweb.web_services.responseBean.ProductListResponse;
import retrofit2.Response;

public class CartInfoActivity extends BaseAppCompatActivity {

    @BindView(R.id.ll_footer_action)
    LinearLayout ll_footer_action;
    @BindView(R.id.ll_null_data)
    LinearLayout ll_null_data;
    @BindView(R.id.rcv_cart_cia)
    RecyclerView rcv_cart_cia;
    View fragmentView;
    @BindView(R.id.txt_total_cia)
    TextView txt_total_cia;
    @BindView(R.id.txt_subtotal_cia)
    TextView txt_subtotal_cia;

    @OnClick(R.id.txt_checkout_action_cia)
    void click_txt_checkout_action_cia() {
        postUpdateCartQuantityApiCall(true);
//        startCheckoutActivity();
    }

    @OnClick(R.id.txt_save_action_cia)
    void click_txt_save_action_cia() {
        postUpdateCartQuantityApiCall(false);
    }

    private UpdateCartQuaRequest updateCartQuaRequest;

    private void postUpdateCartQuantityApiCall(boolean startCheckoutActivity) {
        if (updateCartQuaRequest == null)
            updateCartQuaRequest = new UpdateCartQuaRequest();

        List<UpdateCartQuaRequest.Updatecart> updateCartList = new ArrayList<>();
        for (CartInfoResponse.Cart cart : cartList) {
            UpdateCartQuaRequest.Updatecart updatecart = new UpdateCartQuaRequest.Updatecart();
            updatecart.setId(String.valueOf(cart.getId()));
            updatecart.setQuantity(String.valueOf(cart.getQuantity()));
            updateCartList.add(updatecart);
        }
        updateCartQuaRequest.setUpdatecart(updateCartList);

        ApiCall.getInstance().postUpdateCartQuantityApiCall(this, updateCartQuaRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess && startCheckoutActivity)
                startCheckoutActivity();
        });
    }

    private void startCheckoutActivity() {
        startActivity(new Intent(this, CheckoutActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_info);
        ButterKnife.bind(this);

        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_ca);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        }
        toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void initializeView() {
        super.initializeView();
        Utilities.getInstance().setRecyclerViewUISpanCount(this, rcv_cart_cia, 1, DoActionType.Vertical);
        postCartApiCall();
        updateTotals();
    }

    private void postCartApiCall() {
        ApiCall.getInstance().postCartApiCall(this, true, this::onCartResponse);
    }

    private CartInfoResponse cartInfoResponse;
    private List<CartInfoResponse.Cart> cartList;

    private void onCartResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            cartInfoResponse = (CartInfoResponse) o;
            cartList = cartInfoResponse.getResponsedata().getCart();
        }
        manageData();
    }

    private void manageData() {
        if (cartList == null || cartList.size() == 0) {
            hideListView();
        } else {
            showListView();
            setAdapter();
        }
        updateTotals();
    }

    private CartProductListAdapter adapter;

    private void setAdapter() {
        adapter = new CartProductListAdapter(cartList, this, (object, position, type) -> {
            switch (type) {
                case Remove:
                    postRemoveProductFromCartApiCall(String.valueOf(cartList.get(position).getId()), position);
                    break;
                case ADD:
                    updateTotals();
                    break;
                case Whishlist:
                    manageWishList(cartList.get(position), position);
                    break;
                case SUB:
                    updateTotals();
                    break;
            }
        });
        rcv_cart_cia.setAdapter(adapter);
    }

    private void updateList() {
        adapter.setData(cartList);
    }

    private RemoveItemRequest removeItemRequest;

    private void postRemoveProductFromCartApiCall(String product_id, int position) {
        if (removeItemRequest == null)
            removeItemRequest = new RemoveItemRequest();
        removeItemRequest.setProductId(product_id);
        ApiCall.getInstance().postRemoveProductFromCartApiCall(this, removeItemRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                cartList.remove(position);
                updateData();
            }
        });
    }

    private void updateData() {
        if (cartList.size() == 0)
            hideListView();
        adapter.setData(cartList);
        updateTotals();
    }

    private void showListView() {
        ll_null_data.setVisibility(View.GONE);
        ll_footer_action.setVisibility(View.VISIBLE);
    }

    private void hideListView() {
        ll_null_data.setVisibility(View.VISIBLE);
        ll_footer_action.setVisibility(View.GONE);
    }

    private void updateTotals() {
        double subTotal = 0.00;
        if (cartList != null) {
            for (CartInfoResponse.Cart cart : cartList) {
                subTotal += Double.parseDouble(cart.getPrice()) * cart.getQuantity();
            }
        }
        txt_subtotal_cia.setText("$" + new DecimalFormat("##.##").format(subTotal));
        txt_total_cia.setText("$" + new DecimalFormat("##.##").format(subTotal));
    }

    private void manageWishList(CartInfoResponse.Cart responsedata, int position) {
        if (responsedata.getWishlist().equals("0")) {
            postAddWishlistProductApiCall(responsedata, position);
        } else if (responsedata.getWishlist().equals("1")) {
            postDeleteWishlistProductApiCall(responsedata, position);
        }
    }

    private void postDeleteWishlistProductApiCall(CartInfoResponse.Cart product, int position) {
        if (addWishlistProductRequest == null)
            addWishlistProductRequest = new AddWishlistProductRequest();
        addWishlistProductRequest.setProductId(String.valueOf(product.getId()));
        ApiCall.getInstance().postDeleteWishlistProductApiCall(this, addWishlistProductRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                makeToast(product.getName() + " has been remove to your Wishlist.");
                product.setWishlist("0");
                cartList.set(position, product);
                updateList();
            }
        });
    }

    private AddWishlistProductRequest addWishlistProductRequest;

    private void postAddWishlistProductApiCall(CartInfoResponse.Cart product, int position) {
        if (addWishlistProductRequest == null)
            addWishlistProductRequest = new AddWishlistProductRequest();
        addWishlistProductRequest.setProductId(String.valueOf(product.getId()));
        ApiCall.getInstance().postAddWishlistProductApiCall(this, addWishlistProductRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                makeToast(product.getName() + " has been added in your Wishlist.");
                product.setWishlist("1");
                cartList.set(position, product);
                updateList();
            }
        });
    }
}

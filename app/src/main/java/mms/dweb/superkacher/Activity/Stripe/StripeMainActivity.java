package mms.dweb.superkacher.Activity.Stripe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.superkacher.R;
import mms.dweb.web_services.requestBean.CreateOrderRequest;

public class StripeMainActivity extends BaseAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripe_main);
    }

    private CreateOrderRequest createOrderRequest;

    @Override
    protected void onStart() {
        super.onStart();
        Bundle bundle = getIntent().getExtras();
        if (bundle.getSerializable("Order") != null) {
            createOrderRequest = (CreateOrderRequest) bundle.getSerializable("Order");
        }

        initializeView();
    }

    @Override
    public void initializeView() {
        super.initializeView();

    }

}

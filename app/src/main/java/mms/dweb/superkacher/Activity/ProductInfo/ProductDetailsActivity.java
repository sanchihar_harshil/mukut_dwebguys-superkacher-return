package mms.dweb.superkacher.Activity.ProductInfo;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.Login.LoginUserActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.RelatedProductListAdapter;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.utilities.Systemout;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.AddToCartRequest;
import mms.dweb.web_services.requestBean.AddToCartWithOptionRequest;
import mms.dweb.web_services.requestBean.AddWishlistProductRequest;
import mms.dweb.web_services.requestBean.ProductDetailsRequest;
import mms.dweb.web_services.responseBean.ProductDetailsResponse;
import mms.dweb.web_services.responseBean.ProductListResponse;
import mms.dweb.web_services.responseBean.RelatedProductResponse;
import retrofit2.Response;

public class ProductDetailsActivity extends BaseAppCompatActivity {
    @BindView(R.id.img_product_pda)
    ImageView img_product_pda;

    @BindView(R.id.ll_categories_pda)
    LinearLayout ll_categories_pda;

    @BindView(R.id.txt_short_dis_pda)
    TextView txt_short_dis_pda;

    @BindView(R.id.ll_tags_pda)
    LinearLayout ll_tags_pda;

    @BindView(R.id.txt_description_pda)
    TextView txt_description_pda;

    @BindView(R.id.ll_additional_info_pda)
    LinearLayout ll_additional_info_pda;

    @BindView(R.id.rcv_related_pda)
    RecyclerView rcv_related_pda;

    @BindView(R.id.img_wishlist_pda)
    ImageView img_wishlist_pda;
    @BindView(R.id.option_spineer)
    Spinner option_spineer;
    @BindView(R.id.tv_option)
    TextView tv_option;
    @BindView(R.id.ll_option)
    LinearLayout ll_option;

    @BindView(R.id.txt_name_pda)
    TextView txt_name_pda;

    @BindView(R.id.txt_price_pda)
    TextView txt_price_pda;
    @BindView(R.id.txt_quantity_pda)
    TextView txt_quantity_pda;

    @OnClick(R.id.txt_add_action_pda)
    void click_txt_add_action_pda() {
        AddTocartApiCall(product_id, quantity);
    }

    @OnClick(R.id.txt_share_action_pda)
    void click_txt_share_action_pda() {
        Utilities.getInstance().shareApp(this);
    }

    @OnClick(R.id.img_add_pda)
    void click_img_add_pda() {
        quantity++;
        txt_quantity_pda.setText(String.valueOf(quantity));
    }

    @OnClick(R.id.img_sub_pda)
    void click_img_sub_pda() {
        if (quantity > 1)
            quantity--;
        txt_quantity_pda.setText(String.valueOf(quantity));
    }

    private int quantity = 1;

    @OnClick(R.id.img_wishlist_pda)
    void click_img_wishlist_pda() {
        if (productDetailsResponse != null)
            manageWishList(productDetailsResponse.getResponsedata());
    }

    private ProductListResponse.Responsedata productData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar_ca);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_chevron_left_black_24dp));
        } else {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        }
        toolbar.setNavigationOnClickListener(view -> {
            onBackPressed();
        });
        getSupportActionBar().setTitle(getString(R.string.product_details));

        intent = this.getIntent();
        productData = (ProductListResponse.Responsedata) intent.getSerializableExtra("Product");

    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeView();
    }

    private Intent intent;
    private int product_id;
    private int selectedOption;

    @Override
    public void initializeView() {
        super.initializeView();
        Utilities.getInstance().setRecyclerViewUISpanCount(this, rcv_related_pda, 1, DoActionType.Vertical);
        if (productData != null) {
            product_id = productData.getId();
            callApis();
        }

    }


    private void manageWishList(ProductDetailsResponse.Responsedata responsedata) {
        if (responsedata.getWishlist().equals("0")) {
            postAddWishlistProductApiCall(responsedata);
        } else if (responsedata.getWishlist().equals("1")) {
            postDeleteWishlistProductApiCall(responsedata);
        }
    }

    private void postDeleteWishlistProductApiCall(ProductDetailsResponse.Responsedata product) {
        if (addWishlistProductRequest == null)
            addWishlistProductRequest = new AddWishlistProductRequest();
        addWishlistProductRequest.setProductId(String.valueOf(product.getId()));
        ApiCall.getInstance().postDeleteWishlistProductApiCall(this, addWishlistProductRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                makeToast(product.getName() + " has been remove to your Wishlist.");
                product.setWishlist("0");
                updateMainUI();
            }
        });
    }

    private void postAddWishlistProductApiCall(ProductDetailsResponse.Responsedata product) {
        if (addWishlistProductRequest == null)
            addWishlistProductRequest = new AddWishlistProductRequest();
        addWishlistProductRequest.setProductId(String.valueOf(product.getId()));
        ApiCall.getInstance().postAddWishlistProductApiCall(this, addWishlistProductRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                makeToast(product.getName() + " has been added in your Wishlist.");
                product.setWishlist("1");
                updateMainUI();
            }
        });
    }

    /**
     * this method use to call ProductDetailsApiCall and RelatedProductsApiCall
     */
    private void callApis() {
        postProductDetailsApiCall();
        postRelatedProductsApiCall();
    }

    private ProductDetailsRequest request;

    private void postProductDetailsApiCall() {
        if (request == null)
            request = new ProductDetailsRequest();
        request.setProductId(String.valueOf(product_id));
        ApiCall.getInstance().postProductDetailsApiCall(this, request, true, this::OnProductDetailsResponse);
    }

    private ProductDetailsResponse productDetailsResponse;

    private void OnProductDetailsResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            productDetailsResponse = (ProductDetailsResponse) o;
            managDataView();
        }
    }

    private void managDataView() {
        updateMainUI();
        AddAllCategories();
        AddAllTags();
        AddAllAdditionalInfo();
        SetDiscription();

    }

    private void updateMainUI() {
        ApplicationContext.getInstance()
                .loadImage(productDetailsResponse.getResponsedata()
                                .getImages().get(0).getSrc(),
                        img_product_pda,
                        null,
                        R.drawable.ic_launcher);
        img_wishlist_pda.setImageResource(productDetailsResponse.getResponsedata().getWishlist().equals("1") ? R.drawable.ic_favorite : R.drawable.ic_favorite_border_prime_24dp);
        txt_name_pda.setText(productDetailsResponse.getResponsedata().getName());
        txt_price_pda.setText("$" + productDetailsResponse.getResponsedata().getPrice());
        txt_quantity_pda.setText(String.valueOf(quantity));
        txt_short_dis_pda.setText(Html.fromHtml(productDetailsResponse.getResponsedata().getShortDescription()));
        setOptionData();

    }

    protected List<String> optionList;

    private void setOptionData() {
        if (productDetailsResponse.getResponsedata().getAttributes().isEmpty())
            return;
        ll_option.setVisibility(productDetailsResponse.getResponsedata().getAttributes().get(0).getVisible() ?
                View.VISIBLE : View.GONE);
        tv_option.setText(productDetailsResponse.getResponsedata().getAttributes().get(0).getName());
        optionList = productDetailsResponse.getResponsedata().getAttributes().get(0).getOptions();
       // optionList.add(0,"Choose an Option");
        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item,
                optionList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        option_spineer.setAdapter(aa);

      /*  productDetailsResponse.getResponsedata().getVariationImages()
                .add(0,productDetailsResponse.getResponsedata().getImages().get(0).getSrc());
*/
        option_spineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (optionList != null) {
                    selectedOption = productDetailsResponse.getResponsedata()
                            .getVariations().get(position);
                    
                     if (productDetailsResponse != null
                            && productDetailsResponse.getResponsedata().getVariationImages().size() > 0) {
                        ApplicationContext.getInstance()
                                .loadImage(productDetailsResponse.getResponsedata()
                                                .getVariationImages().get(position),
                                        img_product_pda,
                                        null,
                                        R.drawable.ic_launcher);
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void SetDiscription() {
        txt_description_pda.setText(Html.fromHtml(productDetailsResponse.getResponsedata().getDescription()));
    }

    private void AddAllAdditionalInfo() {
        ProductDetailsResponse.Dimensions dimensions = productDetailsResponse.getResponsedata().getDimensions();
        TextView textView = new TextView(this);
        textView.setText("Weight            -" + productDetailsResponse.getResponsedata().getWeight() + "lbs");
        ll_additional_info_pda.addView(textView);
        TextView textView2 = new TextView(this);
        textView2.setText("Dimensions       -" + dimensions.getLength() + " x " + dimensions.getWidth() + " x " + dimensions.getHeight() + "in");
        ll_additional_info_pda.addView(textView2);
    }

    private void AddAllTags() {
        List<ProductDetailsResponse.Tags> tagList = productDetailsResponse.getResponsedata().getTags();
        for (ProductDetailsResponse.Tags tags : tagList) {
            TextView textView = new TextView(this);
            textView.setText(tags.getName().trim());
            ll_tags_pda.addView(textView);
        }

    }

    private void AddAllCategories() {
        List<ProductDetailsResponse.Categories> catList = productDetailsResponse.getResponsedata().getCategories();
        for (ProductDetailsResponse.Categories categories : catList) {
            TextView textView = new TextView(this);
            textView.setText(categories.getName());
            textView.setTag(categories);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProductDetailsResponse.Categories categories1 = (ProductDetailsResponse.Categories) textView.getTag();
                    ApplicationContext.getInstance().setCategory(categories1.getId());
                    finish();
                }
            });
            ll_categories_pda.addView(textView);
        }
    }

    private void postRelatedProductsApiCall() {
        if (request == null)
            request = new ProductDetailsRequest();
        request.setProductId(String.valueOf(product_id));
        ApiCall.getInstance().postRelatedProductsApiCall(this, request, true, this::OnRelatedProductsResponse);
    }

    private RelatedProductResponse relatedProductResponse;
    private List<RelatedProductResponse.Responsedata> relatedProductList;

    private void OnRelatedProductsResponse(boolean isSuccess, Response<Object> objectResponse, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            relatedProductResponse = (RelatedProductResponse) o;
            relatedProductList = relatedProductResponse.getResponsedata();
            manageRelatedData();
        }
    }

    private void manageRelatedData() {
        if (relatedProductList != null && relatedProductList.size() > 0) {
            setAdapter();
        }
    }

    private RelatedProductListAdapter adapter;

    private void updateList() {
        adapter.setData(relatedProductList);
    }

    private void setAdapter() {
        adapter = new RelatedProductListAdapter(relatedProductList, this, (object, position, type) -> {
            switch (type) {
                case Cart:
                    AddTocartApiCall(relatedProductList.get(position).getId(),
                            relatedProductList.get(position).getQuantity());
                    break;
                case Whishlist:
                    manageWishList(relatedProductList.get(position), position);
                    break;
                case Ok:
                    product_id = relatedProductList.get(position).getId();
                    callApis();
                    break;
            }
        });
        rcv_related_pda.setAdapter(adapter);
    }

    public boolean isValidUser() {
        if (PrefSetup.getInstance().getUserInfo() != null)
            return true;
        PopupUtils.getInstance().showTwoButtonDialogNoCancelable(this, getString(R.string.alert),
                getString(R.string.login_error), getString(R.string.cancel), getString(R.string.login), (object, position) -> {
                    if (position == 1)
                        startLoginActivity();
                });
        return false;
    }

    private void startLoginActivity() {
        Intent i = new Intent(this, LoginUserActivity.class);
// set the new task and clear flags
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    private void AddTocartApiCall(int product_id, int quantity) {
        if (!isValidUser())
            return;
        AddToCartWithOptionRequest addToCartRequest = new AddToCartWithOptionRequest();
        addToCartRequest.setProductId(product_id);
        addToCartRequest.setProductCount(String.valueOf(quantity));
        addToCartRequest.setOption(String.valueOf(selectedOption));
        ApiCall.getInstance().postAddToCartWithOptionApiCall(this, addToCartRequest, true, this::OnAddToCartResponse);
    }

    private void OnAddToCartResponse(boolean isSuccess, Response<Object> objectResponse, Throwable throwable, ErrorType errorType, Object o) {
        Systemout.println(o);
    }

    private void manageWishList(RelatedProductResponse.Responsedata responsedata, int position) {
        if (responsedata.getWishlist().equals("0")) {
            postAddWishlistProductApiCall(responsedata, position);
        } else if (responsedata.getWishlist().equals("1")) {
            postDeleteWishlistProductApiCall(responsedata, position);
        }
    }

    private void postDeleteWishlistProductApiCall(RelatedProductResponse.Responsedata product, int position) {
        if (addWishlistProductRequest == null)
            addWishlistProductRequest = new AddWishlistProductRequest();
        addWishlistProductRequest.setProductId(String.valueOf(product.getId()));
        ApiCall.getInstance().postDeleteWishlistProductApiCall(this, addWishlistProductRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                makeToast(product.getName() + " has been remove to your Wishlist.");
                product.setWishlist("0");
                relatedProductList.set(position, product);
                updateList();
            }
        });
    }

    private AddWishlistProductRequest addWishlistProductRequest;

    private void postAddWishlistProductApiCall(RelatedProductResponse.Responsedata product, int position) {
        if (addWishlistProductRequest == null)
            addWishlistProductRequest = new AddWishlistProductRequest();
        addWishlistProductRequest.setProductId(String.valueOf(product.getId()));
        ApiCall.getInstance().postAddWishlistProductApiCall(this, addWishlistProductRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                makeToast(product.getName() + " has been added in your Wishlist.");
                product.setWishlist("1");
                relatedProductList.set(position, product);
                updateList();
            }
        });
    }
}

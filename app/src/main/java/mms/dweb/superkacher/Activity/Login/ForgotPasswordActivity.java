package mms.dweb.superkacher.Activity.Login;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.PasswordResetRequest;
import mms.dweb.web_services.responseBean.PasswordResetResponse;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseAppCompatActivity {
    @BindView(R.id.et_username_FPA)
    EditText et_username_FPA;
    @BindView(R.id.txt_forgot_action_FPA)
    TextView txt_forgot_action_FPA;

    @OnClick(R.id.txt_forgot_action_FPA)
    void click_txt_forgot_action_FPA() {
        if (isValid())
            postPasswordResetApiCall();

    }

    private void postPasswordResetApiCall() {
        ApiCall.getInstance().postPasswordResetApiCall(this, passwordResetRequest, true, this::OnresetPasswordResponse);
    }

    private void OnresetPasswordResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            PasswordResetResponse passwordResetResponse = (PasswordResetResponse) o;
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.forgot_password), passwordResetResponse.getResponsemessage(), (object, position) -> {

            });
        }
    }

    private PasswordResetRequest passwordResetRequest;

    private boolean isValid() {
        if (et_username_FPA.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.alert), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        }
        if (passwordResetRequest == null)
            passwordResetRequest = new PasswordResetRequest();
        passwordResetRequest.setUser_login(et_username_FPA.getText().toString().trim());
        return true;

    }

    @OnClick(R.id.layout_signIn_FPA)
    void click_layout_signIn_FPA() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        initializeView();
    }


    @Override
    public void initializeView() {
        super.initializeView();

    }
}

package mms.dweb.superkacher.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mms.dweb.base.BaseAppCompatActivity;
import mms.dweb.base.CameraListener;
import mms.dweb.base.ErrorType;
import mms.dweb.base.OnListItemClickListener;
import mms.dweb.base.OnTwoSpinnerSelectListener;
import mms.dweb.base.RtyFuntionalInterface;
import mms.dweb.superkacher.Activity.filter.FilterActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.responseBean.ShortingProductResponse;
import retrofit2.Response;

/**
 * Created by mormukutsinghji@gmail.com on 3/14/2018.
 */

public class PopupUtils {

    private PopupUtils() {
    }

    private static PopupUtils instance;

    public static PopupUtils getInstance() {
        if (null == instance) {
            instance = new PopupUtils();
        }
        return instance;
    }

    public void showRetryDialogNoCancelable(final Context activity, String title, String message, final RtyFuntionalInterface anInterface) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_rty, null, false);
        dialog.setContentView(view);

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_message = (TextView) view.findViewById(R.id.tv_message);
        ImageView img_refresh = (ImageView) view.findViewById(R.id.img_refresh);
        LinearLayout ll_cancelButton = (LinearLayout) view.findViewById(R.id.ll_cancelButton);

        tv_title.setText(title);
        tv_message.setText(message);
        img_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                anInterface.onResponce(true);
            }
        });

        ll_cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                anInterface.onResponce(false);
            }
        });

    }

    public void showOkButtonDialogNoCancelable(BaseAppCompatActivity activity, String title, String message, final OnListItemClickListener listener) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_ok_button, null, false);
        dialog.setContentView(view);

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_message = (TextView) view.findViewById(R.id.tv_message);
        tv_message.setText(message);
        tv_title.setText(title);
        TextView txt_done_FPD = (TextView) view.findViewById(R.id.txt_done_FPD);

        txt_done_FPD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(true, 1);
                dialog.dismiss();
            }
        });
    }

    public void showSpinnerDialogNoCancelable(BaseAppCompatActivity activity, String title, String message, List<String> list, final OnListItemClickListener listener) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_spinner, null, false);
        dialog.setContentView(view);

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_message = (TextView) view.findViewById(R.id.tv_message);
        Spinner dialog_spineer = view.findViewById(R.id.dialog_spineer);
        tv_message.setText(message);
        tv_title.setText(title);
        TextView txt_done_FPD = (TextView) view.findViewById(R.id.txt_done_FPD);

//Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, list);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        dialog_spineer.setAdapter(aa);

        dialog_spineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0)
                    return;
                listener.onItemClick(list.get(position-1), position-1);
                dialog.dismiss();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        txt_done_FPD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(null, -1);
                dialog.dismiss();
            }
        });
    }

    public void showSpinnerStateDialogNoCancelable(BaseAppCompatActivity activity, String title, String message, List<String> list, final OnListItemClickListener listener) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_spinner, null, false);
        dialog.setContentView(view);

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_message = (TextView) view.findViewById(R.id.tv_message);
        Spinner dialog_spineer = view.findViewById(R.id.dialog_spineer);
        tv_message.setText(message);
        tv_title.setText(title);
        TextView txt_done_FPD = (TextView) view.findViewById(R.id.txt_done_FPD);

//Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, list);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        dialog_spineer.setAdapter(aa);

        dialog_spineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0)
                    return;
                listener.onItemClick(list.get(position), position);
                dialog.dismiss();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        txt_done_FPD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(null, -1);
                dialog.dismiss();
            }
        });
    }

    public void showTwoSpinnerDialogNoCancelable(BaseAppCompatActivity activity, String title, String message, List<String> list, List<String> list2, final OnTwoSpinnerSelectListener listener) {
//        final int[] selected = new int[2];
//        final int[] selectedTag = {0};
//        List<Integer> selected = new ArrayList<>();
        final int[] cat = new int[1];
        final int[] tag = new int[1];
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_two_spinner, null, false);
        dialog.setContentView(view);

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_message = (TextView) view.findViewById(R.id.tv_message);
        Spinner dialog_spineer = view.findViewById(R.id.dialog_spineer);
        Spinner second_dialog_spineer = view.findViewById(R.id.second_dialog_spineer);
        tv_message.setText(message);
        tv_title.setText(title);
        TextView txt_done_FPD = (TextView) view.findViewById(R.id.txt_done_FPD);
        TextView txt_cancle_FPD = (TextView) view.findViewById(R.id.txt_cancle_FPD);
        TextView txt_reset_FPD = (TextView) view.findViewById(R.id.txt_reset_FPD);

//Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, list);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter aa2 = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, list2);
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        dialog_spineer.setAdapter(aa);
        second_dialog_spineer.setAdapter(aa2);

        dialog_spineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                cat[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        second_dialog_spineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                tag[0] = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        txt_done_FPD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(null, 1, cat[0], tag[0]);
                dialog.dismiss();
            }
        });
        txt_cancle_FPD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(null, -1, -1, -1);
                dialog.dismiss();
            }
        });

        txt_reset_FPD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(null, 2, -1, -1);
                dialog.dismiss();
            }
        });
    }


    public void showTwoButtonDialogNoCancelable(BaseAppCompatActivity activity, String title, String message, String cancel, String done, final OnListItemClickListener listener) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_two_button, null, false);
        dialog.setContentView(view);

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_message = (TextView) view.findViewById(R.id.tv_message);
        TextView txt_cancle = (TextView) view.findViewById(R.id.txt_cancle);
        TextView txt_done = (TextView) view.findViewById(R.id.txt_done);
        txt_cancle.setText(cancel);
        txt_done.setText(done);
        tv_message.setText(message);
        tv_title.setText(title);
//        TextView txt_done_FPD = (TextView) view.findViewById(R.id.txt_done_FPD);

        txt_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(true, 1);
                dialog.dismiss();
            }
        });
        txt_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(false, 0);
                dialog.dismiss();
            }
        });
    }


    public void showCameraGelleryDailog(final Context activity, CameraListener cameraListener) {
        try {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.popup_camera_gallery, null, false);
            dialog.setContentView(view);

            final Window window = dialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


            TextView tv_camera = (TextView) view.findViewById(R.id.tv_camera);
            TextView tv_gallery = (TextView) view.findViewById(R.id.tv_gallery);
            TextView tv_cancel = (TextView) view.findViewById(R.id.tv_cancel);
            tv_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cameraListener.onCameraSelect();
                    dialog.dismiss();
                }
            });

            tv_gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cameraListener.onGallerySelect();
                    dialog.dismiss();
                }
            });

            tv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

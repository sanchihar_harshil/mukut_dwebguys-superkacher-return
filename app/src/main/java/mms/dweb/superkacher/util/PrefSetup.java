package mms.dweb.superkacher.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.utilities.Constant;
import mms.dweb.web_services.responseBean.CreateOrderResponse;
import mms.dweb.web_services.responseBean.LoginResponse;
import mms.dweb.web_services.responseBean.ShortingProductResponse;

/**
 * Created by mormukutsinghji@gmail.com on 3/14/2018.
 */

public class PrefSetup {

    private static PrefSetup instance;

    private PrefSetup(Context context) {
        sp = context.getSharedPreferences(Constant.PREF_FILE, Context.MODE_PRIVATE);
    }

    public static synchronized PrefSetup getInstance() {
        if (instance != null)
            return instance;

        instance = new PrefSetup(ApplicationContext.getInstance());
        return instance;
    }

    public SharedPreferences sp;

    private String UserDetails = "userdetails";
    private String UserInfo = "userInfo";
    private String UserId = "userId";
    private String ProfileImage = "profileImage";
    private String UserName = "userName";
    private String FilterCategory = "filterCategory";
    private String FilterTag = "filterTag";
    private String IsVirgin = "IsVirgin";
    String json;

    private String OrderDetails = "orderdetails";
    private String OrderInfo = "orderInfo";


    public CreateOrderResponse getOrderInfo() {
        Gson gson = new Gson();
        CreateOrderResponse orderDetail = new CreateOrderResponse();
        if (sp.getString(OrderDetails, "") != null) {
            json = sp.getString(OrderInfo, "");
            return gson.fromJson(json, CreateOrderResponse.class);
        } else {
            return orderDetail;
        }
    }

    public LoginResponse getUserInfo() {
        Gson gson = new Gson();
        LoginResponse userDetail = new LoginResponse();
        if (sp.getString(UserDetails, "") != null) {
            json = sp.getString(UserInfo, "");
            return gson.fromJson(json, LoginResponse.class);
        } else {
            return userDetail;
        }
    }


    public ShortingProductResponse.Categories getFilterCategory() {
        Gson gson = new Gson();
        ShortingProductResponse.Categories categories = new ShortingProductResponse.Categories();
        if (sp.getString(UserDetails, "") != null) {
            json = sp.getString(FilterCategory, "");
            return gson.fromJson(json, ShortingProductResponse.Categories.class);
        } else {
            return categories;
        }
    }

    public ShortingProductResponse.Tags getFilterTag() {
        Gson gson = new Gson();
        ShortingProductResponse.Tags tags = new ShortingProductResponse.Tags();
        if (sp.getString(UserDetails, "") != null) {
            json = sp.getString(FilterTag, "");
            return gson.fromJson(json, ShortingProductResponse.Tags.class);
        } else {
            return tags;
        }
    }
//    public UserLoginResponse getUserDetail() {
//        Gson gson = new Gson();
//        UserLoginResponse userDetail = new UserLoginResponse();
//        if (sp.getString(UserDetails, "") != null) {
//            json = sp.getString(UserDetails, "");
//            return gson.fromJson(json, UserLoginResponse.class);
//        } else {
//            return userDetail;
//        }
//    }

    public String getUserId() {
        return sp.getString(UserId, null);
    }

    public boolean getIsVirgin() {
        return sp.getBoolean(IsVirgin, true);
    }

    public String getUserName() {
        return sp.getString(UserName, null);
    }

    public String getProfileImage() {
        return sp.getString(ProfileImage, null);
    }


    public void setUserInfo(LoginResponse userInfo) {
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(userInfo);
        editor.putString(UserInfo, json);
        editor.commit();
    }

    public void setOrderInfo(CreateOrderResponse response) {
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(response);
        editor.putString(OrderInfo, json);
        editor.commit();
    }

//    public void setUserDetail(UserLoginResponse userDetail) {
//        SharedPreferences.Editor editor = sp.edit();
//        Gson gson = new Gson();
//        String json = gson.toJson(userDetail);
//        editor.putString(UserDetails, json);
//        editor.commit();
//    }

    public void setFilterCategory(ShortingProductResponse.Categories filterCategory) {
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(filterCategory);
        editor.putString(FilterCategory, json);
    }

    public void setFilterTag(ShortingProductResponse.Tags filterTag) {
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(filterTag);
        editor.putString(FilterTag, json);
    }

    public void setUserId(String userId) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(this.UserId, userId);
        editor.commit();
    }

    public void setProfileImage(String userId) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(this.ProfileImage, userId);
        editor.commit();
    }

    public void setUserName(String userName) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(this.UserName, userName);
        editor.commit();
    }

    public void IsVirgin(boolean Virgin) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(this.IsVirgin, Virgin);
        editor.commit();
    }

    public void clearPrefSetup() {
        sp.edit().clear().commit();
    }

}

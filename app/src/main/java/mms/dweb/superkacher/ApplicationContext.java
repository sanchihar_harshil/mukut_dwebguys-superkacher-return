package mms.dweb.superkacher;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.File;

import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.utilities.Constant;

/**
 * Created by mormukutsinghji@gmail.com on 3/14/2018.
 */

public class ApplicationContext extends Application {
    private static ApplicationContext mInstance;
    public static final String TAG = ApplicationContext.class.getSimpleName();
    File ROOT_DIR;
    File CACHE_DIR;
    ImageLoader imageLoader;
    int category;

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        mInstance = this;
        ROOT_DIR = new File(Environment.getExternalStorageDirectory(), Constant.APP_NAME);
        CACHE_DIR = new File(ROOT_DIR, "Cache");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mInstance = null;
    }

    public static synchronized ApplicationContext getInstance() {
        return mInstance;
    }

    public File getCACHE_DIR() {
        return CACHE_DIR;
    }

    private BaseFragment liveFragment;

    public void setLiveFragment(BaseFragment liveFragment) {
        this.liveFragment = liveFragment;
    }

    public BaseFragment getLiveFragment() {
        return liveFragment;
    }

    private ImageLoader getImageLoader() {
        if (imageLoader == null) {
            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                    .cacheOnDisc(true).cacheInMemory(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .showImageForEmptyUri(R.drawable.no_image)
                    .showImageOnFail(R.drawable.no_image)
                    .displayer(new FadeInBitmapDisplayer(2000)).build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(ApplicationContext.this).defaultDisplayImageOptions(defaultOptions)
                    .memoryCache(new WeakMemoryCache())
                    .discCacheSize(100 * 1024 * 1024).build();

            ImageLoader.getInstance().init(config);
            imageLoader = ImageLoader.getInstance();
        }
        return imageLoader;
    }

    public void loadImage(String imageUrl, ImageView imageView, final ProgressBar progressBar, final int defaultImageResource) {
        loadImage(imageUrl, imageView, progressBar, defaultImageResource, null);
    }

    public void loadImage(String imageUrl, ImageView imageView, final ProgressBar progressBar,
                          final int defaultImageResource, final ImageLoadingListener imageLoadingListener) {

//        imageUrl = imageUrl.replace("E:/xampp/htdocs/super-app/", "http://192.168.1.2/super-app/");
        if (imageView == null) {
            return;
        }
        if (imageUrl == null) {
            if (defaultImageResource != 0) {
                imageView.setImageResource(defaultImageResource);
            } else {
                imageView.setImageResource(R.drawable.no_image);
            }
            if (progressBar != null)
                progressBar.setVisibility(View.GONE);
            return;
        }
        ApplicationContext.getInstance().getImageLoader().displayImage(imageUrl, imageView, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
                if (progressBar != null)
                    progressBar.setVisibility(View.VISIBLE);
                if (view != null) {
                    view.setVisibility(View.INVISIBLE);
                }
                if (imageLoadingListener != null) {
                    imageLoadingListener.onLoadingStarted(s, view);
                }
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                if (view != null) {
                    view.setVisibility(View.VISIBLE);
                    if (defaultImageResource != 0) {
                        ((ImageView) view).setImageResource(defaultImageResource);
                    }
                }
                if (imageLoadingListener != null) {
                    imageLoadingListener.onLoadingFailed(s, view, failReason);
                }
            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                if (view != null) {
                    view.setVisibility(View.VISIBLE);
                    ((ImageView) view).setImageBitmap(bitmap);
                }
                if (imageLoadingListener != null) {
                    imageLoadingListener.onLoadingComplete(s, view, bitmap);
                }
            }

            @Override
            public void onLoadingCancelled(String s, View view) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                if (view != null) {
                    view.setVisibility(View.VISIBLE);
                    if (defaultImageResource != 0) {
                        ((ImageView) view).setImageResource(defaultImageResource);
                    }
                }
                if (imageLoadingListener != null) {
                    imageLoadingListener.onLoadingCancelled(s, view);
                }
            }
        });

    }

    public void catchException(Exception e) {
        System.err.println("***************Error***************");
        e.printStackTrace();
        System.err.println("***************Error***************");
    }

    AppCompatDialog progressDialog;

    public void progressON(Activity activity, String message) {

        if (activity == null || activity.isFinishing()) {
            return;
        }


        if (progressDialog != null && progressDialog.isShowing()) {
            progressSET(message);
        } else {

            progressDialog = new AppCompatDialog(activity);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.progress_loading);
            progressDialog.show();

        }


//        final ImageView img_loading_frame = (ImageView) progressDialog.findViewById(R.id.iv_frame_loading);
//        final AnimationDrawable frameAnimation = (AnimationDrawable) img_loading_frame.getBackground();
//        img_loading_frame.post(new Runnable() {
//            @Override
//            public void run() {
//                frameAnimation.start();
//            }
//        });

        TextView tv_progress_message = (TextView) progressDialog.findViewById(R.id.tv_progress_message);
        if (!TextUtils.isEmpty(message)) {
            tv_progress_message.setText(message);
        }


    }

    public void progressSET(String message) {

        if (progressDialog == null || !progressDialog.isShowing()) {
            return;
        }


        TextView tv_progress_message = (TextView) progressDialog.findViewById(R.id.tv_progress_message);
        if (!TextUtils.isEmpty(message)) {
            tv_progress_message.setText(message);
        }

    }

    public void progressOFF() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void clearFragment() {
        liveFragment = null;
    }
}

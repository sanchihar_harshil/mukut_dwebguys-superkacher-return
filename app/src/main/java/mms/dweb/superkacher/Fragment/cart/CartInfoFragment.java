package mms.dweb.superkacher.Fragment.cart;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Activity.checkout.CheckoutActivity;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.CartProductListAdapter;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.RemoveItemRequest;
import mms.dweb.web_services.responseBean.CartInfoResponse;
import retrofit2.Response;

public class CartInfoFragment extends BaseFragment {

    @BindView(R.id.ll_null_data)
    LinearLayout ll_null_data;
    @BindView(R.id.rcv_cart_cif)
    RecyclerView rcv_cart_cif;
    View fragmentView;
    @BindView(R.id.txt_total_cif)
    TextView txt_total_cif;
    @BindView(R.id.txt_subtotal_cif)
    TextView txt_subtotal_cif;

    @OnClick(R.id.txt_checkout_action_cif)
    void click_txt_checkout_action_cif() {
        startCheckoutActivity();
    }

    private void startCheckoutActivity() {
        startActivity(new Intent(activity, CheckoutActivity.class));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_cart_info, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity)
            mainActivity = (MainActivity) getActivity();
        mainActivity.setHeader(0, getString(R.string.cart));
        Utilities.getInstance().setRecyclerViewUISpanCount(activity, rcv_cart_cif, 1, DoActionType.Vertical);
        postCartApiCall();
        updateTotals();
    }

    private void postCartApiCall() {
        ApiCall.getInstance().postCartApiCall(activity, true, this::onCartResponse);
    }

    private CartInfoResponse cartInfoResponse;
    private List<CartInfoResponse.Cart> cartList;

    private void onCartResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            cartInfoResponse = (CartInfoResponse) o;
            cartList = cartInfoResponse.getResponsedata().getCart();
            manageData();
        }
    }

    private void manageData() {
        if (cartList == null || cartList.size() == 0) {
            hideListView();
        } else {
            showListView();
            setAdapter();
        }
        updateTotals();
    }

    private CartProductListAdapter adapter;

    private void setAdapter() {
        adapter = new CartProductListAdapter(cartList, activity, (object, position, type) -> {
            switch (type) {
                case Remove:
                    postRemoveProductFromCartApiCall(String.valueOf(cartList.get(position).getId()), position);
                    break;
                case ADD:
                    updateTotals();
                    break;
                case SUB:
                    updateTotals();
                    break;
            }
        });
        rcv_cart_cif.setAdapter(adapter);
    }

    private RemoveItemRequest removeItemRequest;

    private void postRemoveProductFromCartApiCall(String product_id, int position) {
        if (removeItemRequest == null)
            removeItemRequest = new RemoveItemRequest();
        removeItemRequest.setProductId(product_id);
        ApiCall.getInstance().postRemoveProductFromCartApiCall(activity, removeItemRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                cartList.remove(position);
                updateData();
            }
        });
    }

    private void updateData() {
        if (cartList.size() == 0)
            hideListView();
        adapter.setData(cartList);
        updateTotals();
    }

    private void showListView() {
        ll_null_data.setVisibility(View.GONE);
    }

    private void hideListView() {
        ll_null_data.setVisibility(View.VISIBLE);
    }

    private void updateTotals() {
        double subTotal = 0.00;
        if (cartList != null) {
            for (CartInfoResponse.Cart cart : cartList) {
                subTotal += Double.parseDouble(cart.getPrice()) * cart.getQuantity();
            }
        }
        txt_subtotal_cif.setText("$" + new DecimalFormat("##.##").format(subTotal));
        txt_total_cif.setText("$" + new DecimalFormat("##.##").format(subTotal));
    }

}

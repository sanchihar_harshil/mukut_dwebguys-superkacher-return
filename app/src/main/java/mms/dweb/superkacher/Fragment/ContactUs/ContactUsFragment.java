package mms.dweb.superkacher.Fragment.ContactUs;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.ContactUsRequest;
import mms.dweb.web_services.responseBean.ContactusResponse;
import retrofit2.Response;

public class ContactUsFragment extends BaseFragment {

    View fragmentView;

    @BindView(R.id.et_username_cuf)
    EditText et_username_cuf;
    @BindView(R.id.et_mail_cuf)
    EditText et_mail_cuf;
    @BindView(R.id.et_phone_cuf)
    EditText et_phone_cuf;
    @BindView(R.id.et_subject_cuf)
    EditText et_subject_cuf;
    @BindView(R.id.et_msg_cuf)
    EditText et_msg_cuf;
    @BindView(R.id.txt_save_cuf)
    TextView txt_save_cuf;

    @OnClick(R.id.txt_save_cuf)
    void click_txt_save_cuf() {
        if (isValid())
            postContactUsApicall();
    }

    private void postContactUsApicall() {
        ApiCall.getInstance().postContactUsApicall(activity, request, true, this::onContactUsResponse);
    }

    private void onContactUsResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType type, Object o) {
        if (isSuccess) {
            ContactusResponse contactusResponse = (ContactusResponse) o;
            showOutPut(contactusResponse);
            et_username_cuf.setText("");
            et_mail_cuf.setText("");
            et_phone_cuf.setText("");
            et_subject_cuf.setText("");
            et_msg_cuf.setText("");
        }
    }

    private void showOutPut(ContactusResponse contactusResponse) {
        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.contact_us), contactusResponse.getResponsemessage(), (object, position) -> {

        });
    }

    private ContactUsRequest request;

    private boolean isValid() {
        String name = et_username_cuf.getText().toString().trim();
        String email = et_mail_cuf.getText().toString().trim();
        String phone = et_phone_cuf.getText().toString().trim();
        String subject = et_subject_cuf.getText().toString().trim();
        String msg = et_msg_cuf.getText().toString().trim();

        if (name.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.name), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (email.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.emailid), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (!Utilities.getInstance().isValidEmail(email)) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.emailid), getString(R.string.invalid_data), (object, position) -> {

            });
            return false;
        } else if (Utilities.getInstance().isValidNumber(phone)) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.phone_number), getString(R.string.invalid_data), (object, position) -> {

            });
            return false;
        } else if (subject.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.subject), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (msg.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.message), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;

        }

        if (request == null)
            request = new ContactUsRequest();
        request.setName(name);
        request.setEmail(email);
        request.setPhone_no(phone);
        request.setSubject(subject);
        request.setMessage(msg);
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, fragmentView);
        initializeView();
        return fragmentView;
    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity)
            mainActivity = (MainActivity) getActivity();

        mainActivity.setHeader(0, getString(R.string.contact_us));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, false);

    }
}

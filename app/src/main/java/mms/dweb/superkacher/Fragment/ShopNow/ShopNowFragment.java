package mms.dweb.superkacher.Fragment.ShopNow;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.Fragment.base.FragmentNames;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.CategoriesListAdapter;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.responseBean.CategoriesListResponse;
import retrofit2.Response;

public class ShopNowFragment extends BaseFragment {

    @BindView(R.id.rcv_categories_SNF)
    RecyclerView rcv_categories_SNF;
    @BindView(R.id.ll_null_data)
    LinearLayout ll_null_data;
    @BindView(R.id.swipe_refrsh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_shop_now, container, false);
        ButterKnife.bind(this, fragmentView);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                postCategoriesApiCall(false);

            }
        });

        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, true);

    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }

        mainActivity.setHeader(0, getString(R.string.shop_now));
        Utilities.getInstance().setRecyclerViewUISpanCount(activity, rcv_categories_SNF, 2, DoActionType.Vertical);
        postCategoriesApiCall(true);
    }

    private void postCategoriesApiCall(boolean showDialog) {
        ApiCall.getInstance().postCategoriesApiCall(activity, showDialog, this::onCategoriesResponse);
    }

    private CategoriesListResponse model;
    private List<CategoriesListResponse.Responsedata> categoriesList;

    private void onCategoriesResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        swipeRefreshLayout.setRefreshing(false);
        if (isSuccess) {
            model = (CategoriesListResponse) o;
            categoriesList = model.getResponsedata();
            manageData();
        }
    }

    private void manageData() {
        if (categoriesList == null || categoriesList.size() == 0) {
            hideListView();
        } else {
            showListView();
            setAdapter();
        }
    }

    private CategoriesListAdapter adapter;

    private void setAdapterData() {
        adapter.setData(categoriesList);
    }

    private void setAdapter() {
        adapter = new CategoriesListAdapter(activity, categoriesList, (object, position) -> {
            Bundle bundle = new Bundle();
            bundle.putInt("data", categoriesList.get(position).getCategory_id());
            ApplicationContext.getInstance().setCategory(categoriesList.get(position).getCategory_id());
            mainActivity.replaceFragment(FragmentNames.ProductListFragment, false, bundle, false, false);
        });
        rcv_categories_SNF.setAdapter(adapter);
    }

    public void hideListView() {
        ll_null_data.setVisibility(View.VISIBLE);
    }

    public void showListView() {
        ll_null_data.setVisibility(View.GONE);
    }
}

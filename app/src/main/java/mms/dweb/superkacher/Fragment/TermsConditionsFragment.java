package mms.dweb.superkacher.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.R;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.responseBean.SecurePaymentResponse;
import mms.dweb.web_services.responseBean.TermsResponse;
import okio.Utf8;
import retrofit2.Response;

public class TermsConditionsFragment extends BaseFragment {

    @BindView(R.id.webview_tcf)
    WebView webview_tcf;

    View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_terms_conditions, container, false);
        ButterKnife.bind(this, fragmentView);
        initializeView();
        return fragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, false);

    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();

        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }
        mainActivity.setHeader(0, getString(R.string.terms_conditions));
        ApiCall.getInstance().postTermsApiCall(activity, true, this::OnTermResponce);

//        webview_tcf.loadUrl("https://superkacher.com/terms-and-conditions/");
//        webview_tcf.loadData("<div id=\"section1\">\n" +
//                "<p style=\"text-align: center;\"><strong>Terms and Conditions</strong></p>\n" +
//                "<p><strong>Agreement between User and www.SuperKacher.com</strong></p>\n" +
//                "<p>Welcome to www.SuperKacher.com. The www.SuperKacher.com website (the “Site”) is comprised of various web pages operated by Super Kacher LLC (“Super Kacher”). www.SuperKacher.com is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein (the “Terms”). Your use of www.SuperKacher.com constitutes your agreement to all such Terms. Please read these terms carefully, and keep a copy of them for your reference.</p>\n" +
//                "<p>www.SuperKacher.com is an E-Commerce Site.This website is operated by Super Kacher- Your one-stop-shop for Israeli &amp; Kosher Products. Throughout the site, the terms “we”, “us” and “our” refer to Super Kacher- Your one-stop-shop for Israeli &amp; Kosher Products. Super Kacher- Your one-stop-shop for Israeli &amp; Kosher Products offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.<br>\n" +
//                "By visiting our site and/ or purchasing something from us, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content.<br>\n" +
//                "Please read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. If these Terms of Service are considered an offer, acceptance is expressly limited to these Terms of Service.<br>\n" +
//                "Any new features or tools which are added to the current store shall also be subject to the Terms of Service. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.</p>\n" +
//                "<p><strong>Privacy</strong></p>\n" +
//                "<p>Your use of www.SuperKacher.com is subject to Super Kacher’s Privacy Policy. Please review our Privacy Policy, which also governs the Site and informs users of our data collection practices.</p>\n" +
//                "<p><strong>Electronic Communications</strong></p>\n" +
//                "<p>Visiting www.SuperKacher.com or sending emails to Super Kacher constitutes electronic communications. You consent to receive electronic communications and you agree that all agreements, notices, disclosures and other communications that we provide to you electronically, via email and on the Site, satisfy any legal requirement that such communications be in writing.</p>\n" +
//                "<p><strong>Your Account</strong></p>\n" +
//                "<p>If you use this site, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and you agree to accept responsibility for all activities that occur under your account or password. You may not assign or otherwise transfer your account to any other person or entity. You acknowledge that Super Kacher is not responsible for third party access to your account that results from theft or misappropriation of your account. Super Kacher and its associates reserve the right to refuse or cancel service, terminate accounts, or remove or edit content in our sole discretion.</p>\n" +
//                "<p><strong>Children Under Thirteen</strong></p>\n" +
//                "<p>Super Kacher does not knowingly collect, either online or offline, personal information from persons under the age of thirteen. If you are under 18, you may use www.SuperKacher.com only with permission of a parent or guardian.</p>\n" +
//                "<p><strong>Cancellation/Refund Policy</strong></p>\n" +
//                "<p><strong>Returns</strong><br>\n" +
//                "Our policy lasts 7 days. If 7 days have gone by since your purchase, unfortunately, we can’t offer you a refund or exchange.<br>\n" +
//                "To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging.<br>\n" +
//                "Several types of goods are exempt from being returned. Perishable goods such as food, flowers, newspapers or magazines cannot be returned. We also do not accept products that are intimate or sanitary goods, hazardous materials, or flammable liquids or gases.</p>\n" +
//                "<p>Additional non-returnable items:<br>\n" +
//                "Gift cards<br>\n" +
//                "Downloadable software products<br>\n" +
//                "Some health and personal care items</p>\n" +
//                "<p><strong>Links to Third Party Sites/Third Party Services</strong></p>\n" +
//                "<p>www.SuperKacher.com may contain links to other websites (“Linked Sites”). The Linked Sites are not under the control of Super Kacher and Super Kacher is not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. Super Kacher is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by Super Kacher of the site or any association with its operators.</p>\n" +
//                "<p>Certain services made available via www.SuperKacher.com are delivered by third party sites and organizations. By using any product, service or functionality originating from the www.SuperKacher.com domain, you hereby acknowledge and consent that Super Kacher may share such information and data with any third party with whom Super Kacher has a contractual relationship to provide the requested product, service or functionality on behalf of www.SuperKacher.com users and customers.</p>\n" +
//                "<p><strong>No Unlawful or Prohibited Use/Intellectual Property</strong></p>\n" +
//                "<p>You are granted a non-exclusive, non-transferable, revocable license to access and use www.SuperKacher.com strictly in accordance with these terms of use. As a condition of your use of the Site, you warrant to Super Kacher that you will not use the Site for any purpose that is unlawful or prohibited by these Terms. You may not use the Site in any manner which could damage, disable, overburden, or impair the Site or interfere with any other party’s use and enjoyment of the Site. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through the Site.</p>\n" +
//                "<p>All content included as part of the Service, such as text, graphics, logos, images, as well as the compilation thereof, and any software used on the Site, is the property of Super Kacher or its suppliers and protected by copyright and other laws that protect intellectual property and proprietary rights. You agree to observe and abide by all copyright and other proprietary notices, legends or other restrictions contained in any such content and will not make any changes thereto.</p>\n" +
//                "<p>You will not modify, publish, transmit, reverse engineer, participate in the transfer or sale, create derivative works, or in any way exploit any of the content, in whole or in part, found on the Site. Super Kacher content is not for resale. Your use of the Site does not entitle you to make any unauthorized use of any protected content, and in particular you will not delete or alter any proprietary rights or attribution notices in any content. You will use protected content solely for your personal use, and will make no other use of the content without the express written permission of Super Kacher and the copyright owner. You agree that you do not acquire any ownership rights in any protected content. We do not grant you any licenses, express or implied, to the intellectual property of Super Kacher or our licensors except as expressly authorized by these Terms.</p>\n" +
//                "<p><strong>Third Party Accounts</strong></p>\n" +
//                "<p>You will be able to connect your Super Kacher account to third party accounts. By connecting your Super Kacher account to your third party account, you acknowledge and agree that you are consenting to the continuous release of information about you to others (in accordance with your privacy settings on those third party sites). If you do not want information about you to be shared in this manner, do not use this feature.</p>\n" +
//                "<p><strong>International Users</strong></p>\n" +
//                "<p>The Service is controlled, operated and administered by Super Kacher from our offices within the USA. If you access the Service from a location outside the USA, you are responsible for compliance with all local laws. You agree that you will not use the Super Kacher Content accessed through www.SuperKacher.com in any country or in any manner prohibited by any applicable laws, restrictions or regulations.</p>\n" +
//                "<p><strong>Indemnification</strong></p>\n" +
//                "<p>You agree to indemnify, defend and hold harmless Super Kacher, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorney’s fees) relating to or arising out of your use of or inability to use the Site or services, any user postings made by you, your violation of any terms of this Agreement or your violation of any rights of a third party, or your violation of any applicable laws, rules or regulations. Super Kacher reserves the right, at its own cost, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, in which event you will fully cooperate with Super Kacher in asserting any available defenses.</p>\n" +
//                "<p><strong>Arbitration</strong></p>\n" +
//                "<p>In the event the parties are not able to resolve any dispute between them arising out of or concerning these Terms and Conditions, or any provisions hereof, whether in contract, tort, or otherwise at law or in equity for damages or any other relief, then such dispute shall be resolved only by final and binding arbitration pursuant to the Federal Arbitration Act, conducted by a single neutral arbitrator and administered by the American Arbitration Association, or a similar arbitration service selected by the parties, in a location mutually agreed upon by the parties. The arbitrator’s award shall be final, and judgment may be entered upon it in any court having jurisdiction. In the event that any legal or equitable action, proceeding or arbitration arises out of or concerns these Terms and Conditions, the prevailing party shall be entitled to recover its costs and reasonable attorney’s fees. The parties agree to arbitrate all disputes and claims in regards to these Terms and Conditions or any disputes arising as a result of these Terms and Conditions, whether directly or indirectly, including Tort claims that are a result of these Terms and Conditions. The parties agree that the Federal Arbitration Act governs the interpretation and enforcement of this provision. The entire dispute, including the scope and enforceability of this arbitration provision shall be determined by the Arbitrator. This arbitration provision shall survive the termination of these Terms and Conditions.</p>\n" +
//                "<p><strong>Class Action Waiver</strong></p>\n" +
//                "<p>Any arbitration under these Terms and Conditions will take place on an individual basis; class arbitrations and class/representative/collective actions are not permitted. THE PARTIES AGREE THAT A PARTY MAY BRING CLAIMS AGAINST THE OTHER ONLY IN EACH’S INDIVIDUAL CAPACITY, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PUTATIVE CLASS, COLLECTIVE AND/ OR REPRESENTATIVE PROCEEDING, SUCH AS IN THE FORM OF A PRIVATE ATTORNEY GENERAL ACTION AGAINST THE OTHER. Further, unless both you and Super Kacher agree otherwise, the arbitrator may not consolidate more than one person’s claims, and may not otherwise preside over any form of a representative or class proceeding.</p>\n" +
//                "<p><strong>Liability Disclaimer</strong></p>\n" +
//                "<p>THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH THE SITE MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. SUPER KACHER LLC AND/OR ITS SUPPLIERS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE SITE AT ANY TIME.</p>\n" +
//                "<p>SUPER KACHER LLC AND/OR ITS SUPPLIERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS CONTAINED ON THE SITE FOR ANY PURPOSE. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS ARE PROVIDED “AS IS” WITHOUT WARRANTY OR CONDITION OF ANY KIND. SUPER KACHER LLC AND/OR ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT.</p>\n" +
//                "<p>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL SUPER KACHER LLC AND/OR ITS SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE SITE, WITH THE DELAY OR INABILITY TO USE THE SITE OR RELATED SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH THE SITE, OR OTHERWISE ARISING OUT OF THE USE OF THE SITE, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF SUPER KACHER LLC OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THE SITE, OR WITH ANY OF THESE TERMS OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE.</p>\n" +
//                "<p>on is our priority. This Statement of Privacy applies to www.SuperKacher.com and Super Kacher LLC and governs data collection and usage. For the purposes of this Privacy Policy, unless otherwise noted, all references to Super Kacher LLC include www.SuperKacher.com and Super Kacher. The Super Kacher website is a online ecommerce site. By using the Super Kacher website, you consent to the data practices described in this statement.</p>\n" +
//                "</div>", "text/html","UTF-8");
    }

    private void OnTermResponce(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType type, Object o) {
        if(isSuccess){
            TermsResponse termsResponse = (TermsResponse) o;
            String response1=termsResponse.getResponsedata();
            Log.e("data", response1);
            webview_tcf.loadData(response1,"text/html","UTF-8");

        }
    }
}

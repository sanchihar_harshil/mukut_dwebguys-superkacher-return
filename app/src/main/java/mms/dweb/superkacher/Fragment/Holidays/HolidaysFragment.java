package mms.dweb.superkacher.Fragment.Holidays;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.Fragment.base.FragmentNames;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.HolidaysListAdapter;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.responseBean.HolidaysListResponse;
import retrofit2.Response;

public class HolidaysFragment extends BaseFragment {

    @BindView(R.id.rcv_holidays_item_hf)
    RecyclerView rcv_holidays_item_hf;
    @BindView(R.id.ll_null_data)
    LinearLayout ll_null_data;
    @BindView(R.id.swipe_refrsh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_holidays, container, false);
        ButterKnife.bind(this, fragmentView);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                postHolidaysListApiCall(false);
            }
        });


        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, true);

    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }
        mainActivity.setHeader(0, getString(R.string.holidays));
        Utilities.getInstance().setRecyclerViewUISpanCount(activity, rcv_holidays_item_hf, 2, DoActionType.Vertical);
        postHolidaysListApiCall(true);
    }

    private void postHolidaysListApiCall(boolean showDialog) {
        ApiCall.getInstance().postHolidaysListApiCall(activity, showDialog, this::OnHolidaysListReponse);
    }

    private HolidaysListResponse holidaysListResponse;
    private List<HolidaysListResponse.Responsedata> holidaysList;

    private void OnHolidaysListReponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        swipeRefreshLayout.setRefreshing(false);
        if (isSuccess) {
            holidaysListResponse = (HolidaysListResponse) o;
            holidaysList = holidaysListResponse.getResponsedata();
            manageData();
        }
    }

    private void manageData() {
        if (holidaysList == null || holidaysList.size() == 0) {
            hideListView();
        } else {
            showListView();
            setAdapter();
        }
    }

    private HolidaysListAdapter adapter;

    private void setAdapter() {
        adapter = new HolidaysListAdapter(activity, holidaysList, (object, position) -> {
            Bundle bundle = new Bundle();
            bundle.putInt("data", holidaysList.get(position).getCategoryId());
            ApplicationContext.getInstance().setCategory(holidaysList.get(position).getCategoryId());
            mainActivity.replaceFragment(FragmentNames.ProductListFragment, false, bundle, false, false);
        });
        rcv_holidays_item_hf.setAdapter(adapter);
    }

    private void showListView() {
        ll_null_data.setVisibility(View.GONE);
    }

    private void hideListView() {
        ll_null_data.setVisibility(View.VISIBLE);
    }
}

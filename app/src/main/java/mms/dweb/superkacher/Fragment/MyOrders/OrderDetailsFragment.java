package mms.dweb.superkacher.Fragment.MyOrders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.MyOrderInfoAdapter;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.OrderDetailsRequest;
import mms.dweb.web_services.responseBean.CreateOrderResponse;
import mms.dweb.web_services.responseBean.MyOrdersResponse;
import mms.dweb.web_services.responseBean.OrderDetailsResponse;
import retrofit2.Response;

public class OrderDetailsFragment extends BaseFragment {


    @BindView(R.id.txt_address_odf)
    TextView txt_address_odf;

    @BindView(R.id.rcv_orderlist_oda)
    RecyclerView rcv_orderlist_oda;

    @BindView(R.id.current_order_info_odf)
    TextView current_order_info_odf;

    @BindView(R.id.txt_total_oda)
    TextView txt_total_oda;
    @BindView(R.id.txt_discount_oda)
    TextView txt_discount_oda;
    @BindView(R.id.txt_shipping_oda)
    TextView txt_shipping_oda;
    @BindView(R.id.txt_subtotal_oda)
    TextView txt_subtotal_oda;

/*
    @BindView(R.id.txt_demo)
    TextView txt_demo;
*/
    View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_order_details, container, false);
        ButterKnife.bind(this, fragmentView);
        initializeView();
        return fragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, false);

    }


    private CreateOrderResponse productData;

    @Override
    public void initializeView() {
        super.initializeView();
        Utilities.getInstance().setRecyclerViewUISpanCount(activity, rcv_orderlist_oda, 1, DoActionType.Vertical);
//        intent = this.getIntent();
        productData = PrefSetup.getInstance().getOrderInfo();
        if (productData != null)
            postOrdersDetailsApiCall();
        clearOrderInfo();
    }

    private void clearOrderInfo() {
        PrefSetup.getInstance().setOrderInfo(null);
    }

    private void postOrdersDetailsApiCall() {
        OrderDetailsRequest orderDetailsRequest = new OrderDetailsRequest();
        orderDetailsRequest.setOrderId(String.valueOf(productData.getResponsedata().getId()));
        ApiCall.getInstance().postOrdersDetailsApiCall(activity, orderDetailsRequest, true, this::OnOrderDetailsResponse);
    }

    private OrderDetailsResponse orderDetailsResponse;

    private OrderDetailsResponse.Responsedata responsedata;

    private void OnOrderDetailsResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            orderDetailsResponse = (OrderDetailsResponse) o;
            manageData();
        }
    }

    private void manageData() {
        if (orderDetailsResponse == null)
            return;
        responsedata = orderDetailsResponse.getResponsedata();
        manageAdapter();
        OrderDetailsResponse.Billing billing = responsedata.getBilling();
        txt_address_odf.setText(billing.getFirstName() + " " + billing.getLastName() + "\n" +
                (billing.getCompany().isEmpty() ? "" : (billing.getCompany() + "\n")) +
                billing.getAddress1() + "\n" +
                (billing.getAddress2().isEmpty() ? "" : (billing.getAddress2() + "\n")) +
                billing.getCity() + "\n" +
                billing.getState() + " " +
                billing.getPostcode() + "\n" +
                billing.getCountry() + "\n" +
                billing.getPhone() + "\n" +
                billing.getEmail()
        );

        String heading = "Order #" + responsedata.getId() + " was placed on " +
                responsedata.getDateCreated() + " and is currently " + responsedata.getStatus() + " payment.";
        current_order_info_odf.setText(heading);

        double grandTotal = Double.parseDouble(responsedata.getTotal()) + Double.parseDouble(responsedata.getShippingTotal())
                - Double.parseDouble(responsedata.getDiscountTotal());
        txt_total_oda.setText("$" + responsedata.getTotal());
        txt_discount_oda.setText("$" + responsedata.getDiscountTotal());
        txt_shipping_oda.setText("$" + responsedata.getShippingTotal());
        txt_subtotal_oda.setText("$" + new DecimalFormat("##.##").format(grandTotal));
    }

    private MyOrderInfoAdapter adapter;

    private void manageAdapter() {
        List<OrderDetailsResponse.Lineitems> lineList = responsedata.getLineitems();
        if (lineList == null || lineList.size() == 0)
            return;
        adapter = new MyOrderInfoAdapter(activity, lineList, (object, position) -> {

        });
        rcv_orderlist_oda.setAdapter(adapter);
    }
}

package mms.dweb.superkacher.Fragment.Testimonial;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.baselib.view.CircleImageView;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.CameraListener;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Fragment.MyAccount.RealPathUtil;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.TestimonialRequest;
import mms.dweb.web_services.responseBean.TestimonialResponse;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static mms.dweb.base.BaseAppCompatSecondaryActivity.CAPTURE_PHOTO;
import static mms.dweb.base.BaseAppCompatSecondaryActivity.SELECT_PHOTO;

public class TestimonialFragment extends BaseFragment {

    @BindView(R.id.img_user_tf)
    CircleImageView img_user_tf;

    @OnClick(R.id.img_user_tf)
    void click_img_profile_maa() {
        if (Utilities.getInstance().checkCameraPermission(activity)) {
            opencameraPopup();
        }
    }

    @BindView(R.id.et_firstname_tf)
    EditText et_firstname_tf;
    @BindView(R.id.et_lastname_tf)
    EditText et_lastname_tf;
    @BindView(R.id.et_email_tf)
    EditText et_email_tf;
    @BindView(R.id.et_title_tf)
    EditText et_title_tf;
    @BindView(R.id.et_description)
    EditText et_description;

    @OnClick(R.id.txt_save_tf)
    void click_txt_save_tf() {
        if (isValid()) {
            postTestProfileImage();
        }
    }

    private Bitmap bitmap;
    private TestimonialRequest request;

    private boolean isValid() {
//        et_firstname_tf.setText("Admin@gmail.com");
//        et_lastname_tf.setText("Admin@gmail.com");
//        et_email_tf.setText("Admin@gmail.com");
//        et_title_tf.setText("Admin@gmail.com");
//        et_description.setText("Admin@gmail.com");
        String Fname = et_firstname_tf.getText().toString().trim();
        String Lname = et_lastname_tf.getText().toString().trim();
        String email = et_email_tf.getText().toString().trim();
        String title = et_title_tf.getText().toString().trim();
        String description = et_description.getText().toString().trim();

        if (Fname.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.name), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (Lname.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.name), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (email.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.emailid), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (!Utilities.getInstance().isValidEmail(email)) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.emailid), getString(R.string.invalid_data), (object, position) -> {

            });
            return false;
        } else if (title.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.subject), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (description.isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.message), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;

        }

        if (request == null)
            request = new TestimonialRequest();
        request.setAdmin("ann");
        request.setUser_name("ann");
        request.setRecv_email("my.maor@gmail.com");
        request.setTitle("ann");
        request.setDesc("ann");
        request.setRating("5");
        return true;
    }

    View fragmentView;
    private String realPath;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_testimonial, container, false);
        ButterKnife.bind(this, fragmentView);
        initializeView();
        return fragmentView;
    }

    File ImageFile = null;
    MainActivity mainActivity;
    Intent intentData;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity)
            mainActivity = (MainActivity) getActivity();

        mainActivity.setHeader(0, getString(R.string.testimonial));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void opencameraPopup() {
        PopupUtils.getInstance().showCameraGelleryDailog(activity, new CameraListener() {
            @Override
            public void onCameraSelect() {
                mainActivity.openCamera();
            }

            @Override
            public void onGallerySelect() {
                mainActivity.openGallery();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_PHOTO && resultCode == RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data);
            realPath = activity.file.getAbsolutePath();
            setCoverImage();

        } else if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && null != data) {
            intentData = data;
            setGalleryView();
        }

    }

    private void setCoverImage() {
        img_user_tf.setImageURI(Uri.parse(realPath));
//         postTestProfileImage();

    }

    private void postTestProfileImage() {
        setImageINRequest();
        ApiCall.getInstance().postTestApiCall(activity, request, true, this::onTestApiResponse);
    }

    private void setImageINRequest() {
        try {
            ImageFile = new File(realPath);
            RequestBody ImageBody = RequestBody.create(MediaType.parse("multipart/form-data"), ImageFile);
            MultipartBody.Part iFile = MultipartBody.Part.createFormData("profile_pic", ImageFile.getName(), ImageBody);
            request.setImage(iFile);
        } catch (Exception e) {

        }

    }

    TestimonialResponse response = new TestimonialResponse();

    private void onTestApiResponse(boolean isSuccess, Response<Object> objectResponse, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            response = (TestimonialResponse) o;
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.testimonial), response.getResponsemessage(),
                    (object, position) -> {

                    });


        }


    }


    private void setGalleryView() {
        realPath = RealPathUtil.getRealPathFromURI_API19(activity, intentData.getData());
        try {
            bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), Uri.parse(realPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setCoverImage();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, false);

    }
}

package mms.dweb.superkacher.Fragment.SecurePayment;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.R;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.responseBean.AboutUsResponse;
import mms.dweb.web_services.responseBean.SecurePaymentResponse;
import retrofit2.Response;

/**
 * Created by Admin on 6/29/2018.
 */

public class SecurePaymentFragment extends BaseFragment{

    @BindView(R.id.webview)
    WebView webview;

    private View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_secure_payment, container, false);
        ButterKnife.bind(this, fragmentView);

        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        WebSettings webSettings = webview.getSettings();
        webSettings.setTextSize(WebSettings.TextSize.SMALLER);
        Resources res= getResources();

        webSettings.setJavaScriptEnabled(true);

        return fragmentView;
    }




    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, false);

    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }
        mainActivity.setHeader(0, getString(R.string.secure));
       // postAboutUsApiCall(true);
        postSecurePaymentResponse(true);
    }

    private void postSecurePaymentResponse(boolean showDialog) {
        ApiCall.getInstance().postSecurePaymentResponse(activity, showDialog, this::OnSecurePaymentResponse);
    }

    private SecurePaymentResponse securePaymentResponse;
    private void OnSecurePaymentResponse(boolean isSuccess, Response<Object> objectResponse, Throwable throwable, ErrorType errorType, Object o) {
        if(isSuccess){
            securePaymentResponse = (SecurePaymentResponse) o;
            String response=securePaymentResponse.getResponsedata();
            Log.e("data", response);
            webview.loadData(response,"text/html","UTF-8");

        }
    }

}

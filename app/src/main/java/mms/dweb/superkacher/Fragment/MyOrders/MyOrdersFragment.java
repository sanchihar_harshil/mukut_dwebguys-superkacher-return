package mms.dweb.superkacher.Fragment.MyOrders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Activity.OrderDetails.OrderDetailsActivity;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.MyOrdersAdapter;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.MyOrdersRequest;
import mms.dweb.web_services.requestBean.ProductListRequest;
import mms.dweb.web_services.responseBean.MyOrdersResponse;
import mms.dweb.web_services.responseBean.ProductListResponse;
import retrofit2.Response;

public class MyOrdersFragment extends BaseFragment {
    @BindView(R.id.rcv_myorder_mof)
    RecyclerView rcv_myorder_mof;

    @BindView(R.id.ll_null_data)
    LinearLayout ll_null_data;


    View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_my_orders, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, false);

    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }

        mainActivity.setHeader(0, getString(R.string.my_orders));
        Utilities.getInstance().setRecyclerViewUISpanCount(activity, rcv_myorder_mof, 1, DoActionType.Vertical);
        postMyOrdersApiCall(true);

    }


    private void showListView() {
        ll_null_data.setVisibility(View.GONE);
    }

    private void hideListView() {
        ll_null_data.setVisibility(View.VISIBLE);
    }

    private MyOrdersRequest myOrdersRequest;


    private void postMyOrdersApiCall(boolean showDialog) {
        if (myOrdersRequest == null)
            myOrdersRequest = new MyOrdersRequest();
        myOrdersRequest.setPage("1");
        ApiCall.getInstance().postMyOrdersApiCall(activity, myOrdersRequest, showDialog, this::OnMyOrderResponse);

    }

    private MyOrdersResponse myOrdersResponse;
    private List<MyOrdersResponse.Responsedata> myorderList;

    private void OnMyOrderResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (myorderList == null)
            myorderList = new ArrayList<>();
        myorderList.clear();
        if (isSuccess) {
            myOrdersResponse = (MyOrdersResponse) o;
            myorderList = myOrdersResponse.getResponsedata();
            manageData();
        }
    }


    private void manageData() {
        if (myorderList == null || myorderList.size() == 0) {
            hideListView();
        } else {
            showListView();
            setAdapter();
        }
    }


    MyOrdersAdapter myOrdersAdapter;

    private void setAdapter() {
        myOrdersAdapter = new MyOrdersAdapter(activity, myorderList, (object, position) -> {
            startOrderDetailsActivity(myorderList.get(position));
        });

        rcv_myorder_mof.setAdapter(myOrdersAdapter);
    }

    private void startOrderDetailsActivity(MyOrdersResponse.Responsedata responsedata) {
        Intent intent = new Intent(activity, OrderDetailsActivity.class);
        intent.putExtra("order", responsedata);
        startActivity(intent);
    }

}

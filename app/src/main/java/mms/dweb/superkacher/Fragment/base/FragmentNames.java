package mms.dweb.superkacher.Fragment.base;

import mms.dweb.superkacher.Fragment.ShippingPolicy.ShippingPolicy;

/**
 * Created by admin on 23/7/2015.
 */
public enum FragmentNames {
    MainFragment(0),
    ShopNowFragment(1),
    BrandsFragment(2),
    HolidaysFragment(3),
    MyAccountFragment(4),
    ProductListFragment(5),
    CartInfoFragment(6),
    MyOrdersFragment(7),
    AboutUsFragment(8),
    ShippingPolicy(9),
    SecurityPayment(10),
    PrivacyPolicy(11),
    ContactUsFragment(12),
    TestimonialFragment(13),
    OrderDetailsFragment(14),
    TermsConditionsFragment(15);
    private final int value;

    FragmentNames(final int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }

}

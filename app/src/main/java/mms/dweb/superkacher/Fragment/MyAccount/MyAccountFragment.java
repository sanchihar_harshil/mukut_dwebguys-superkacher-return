package mms.dweb.superkacher.Fragment.MyAccount;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.CameraListener;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.ChangePassword.ChangePasswordActivity;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.Manifest;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.EditProfileRequest;
import mms.dweb.web_services.responseBean.LoginResponse;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static mms.dweb.base.BaseAppCompatSecondaryActivity.CAPTURE_PHOTO;
import static mms.dweb.base.BaseAppCompatSecondaryActivity.SELECT_PHOTO;


import com.baselib.view.CircleImageView;

import java.io.File;

import mms.dweb.web_services.requestBean.UploadImageProfile;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MyAccountFragment extends BaseFragment {

    View fragmentView;
    @BindView(R.id.txt_login_action_maa)
    TextView txt_login_action_maa;
    @BindView(R.id.ll_userInfo_maa)
    LinearLayout ll_userInfo_maa;
    @BindView(R.id.et_mail_maa)
    TextView et_mail_maa;
    @BindView(R.id.et_username_maa)
    TextView et_username_maa;
    @BindView(R.id.et_lastname_maa)
    EditText et_lastname_maa;
    @BindView(R.id.et_firstname_maa)
    EditText et_firstname_maa;
    @BindView(R.id.img_profile_maa)
    CircleImageView img_profile_maa;


    @OnClick(R.id.img_profile_maa)
    void click_img_profile_maa() {
        if (Utilities.getInstance().checkCameraPermission(activity)) {
            opencameraPopup();
        }
    }

    @OnClick(R.id.txt_forgot_action_maa)
    void click_txt_forgot_action_maa() {
        startChangePasswordActivity();
    }

    @OnClick(R.id.txt_login_action_maa)
    void click_txt_login_action_maa() {
        mainActivity.Logout();
    }

    @OnClick(R.id.txt_logout_action_maa)
    void click_txt_logout_action_maa() {
        mainActivity.Logout();
    }

    @OnClick(R.id.txt_save_action_maa)
    void click_txt_save_action_maa() {
        if (isValid())
            postEditProfileApiCall();
    }

    private void postEditProfileApiCall() {
        ApiCall.getInstance().postEditProfileApiCall(activity, editProfileRequest, true, this::onEditResponse);
    }

    private void onEditResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            updateUserInfo();
        }
    }

    private EditProfileRequest editProfileRequest;

    private boolean isValid() {
        if (et_firstname_maa.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.first_name), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        } else if (et_lastname_maa.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, getString(R.string.last_name), getString(R.string.noDataFound), (object, position) -> {

            });
            return false;
        }
        if (editProfileRequest == null)
            editProfileRequest = new EditProfileRequest();
        editProfileRequest.setFirstname(et_firstname_maa.getText().toString().trim());
        editProfileRequest.setLastname(et_lastname_maa.getText().toString().trim());
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_my_account, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, false);

    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }
        mainActivity.setHeader(0, getString(R.string.my_account));

        updateUserInfo();
    }

    private void startChangePasswordActivity() {
        startActivity(new Intent(activity, ChangePasswordActivity.class));
    }

    private void updateUserInfo() {
        MainActivity mainActivity=new MainActivity();
        LoginResponse userInfo = PrefSetup.getInstance().getUserInfo();
        if (userInfo == null) {
            hideUserInfoView();
        } else {
            showUserInfoView();
            et_firstname_maa.setText(userInfo.getResponsedata().getFirst_name());
            et_lastname_maa.setText(userInfo.getResponsedata().getLast_name());
            et_username_maa.setText(userInfo.getResponsedata().getUser_name());
            et_mail_maa.setText(userInfo.getResponsedata().getEmail());
//            img_profile_maa.setImageResource(0);
//            img_profile_maa.setBackground(null);

//            mainActivity.setNavigationHeader(userInfo.getResponsedata().getProfile_pic(),userInfo.getResponsedata().getFirst_name()+" "+userInfo.getResponsedata().getLast_name(),userInfo.getResponsedata().getEmail());

            ApplicationContext.getInstance().loadImage(userInfo.getResponsedata().getProfile_pic(), img_profile_maa, null, R.drawable.ic_person_black_24dp);

        }
    }

    private void showUserInfoView() {
        ll_userInfo_maa.setVisibility(View.VISIBLE);
        txt_login_action_maa.setVisibility(View.GONE);
    }

    private void hideUserInfoView() {
        ll_userInfo_maa.setVisibility(View.GONE);
        txt_login_action_maa.setVisibility(View.VISIBLE);
    }


    private void opencameraPopup() {
        PopupUtils.getInstance().showCameraGelleryDailog(activity, new CameraListener() {
            @Override
            public void onCameraSelect() {
                mainActivity.openCamera();
            }

            @Override
            public void onGallerySelect() {
                mainActivity.openGallery();
            }
        });
    }

    Intent intentData;

    private Bitmap bitmap;
    private String realPath;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_PHOTO && resultCode == RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data);
//            bitmap = (Bitmap)data.getExtras().get("data");
            realPath = activity.file.getAbsolutePath();
            setCoverImage();

        } else if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && null != data) {
            intentData = data;
            setGalleryView();
        }

    }

    private void setGalleryView() {
        realPath = RealPathUtil.getRealPathFromURI_API19(activity, intentData.getData());
        try {
            bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), Uri.parse(realPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setCoverImage();
    }


    private void setCoverImage() {
        img_profile_maa.setImageURI(Uri.parse(realPath));
        postEditProfileImage();

    }

    UploadImageProfile uploadImageProfile = new UploadImageProfile();
    File ImageFile = null;

    private void postEditProfileImage() {

        ImageFile = new File(realPath);
        RequestBody ImageBody = RequestBody.create(MediaType.parse("multipart/form-data"), ImageFile);
        MultipartBody.Part iFile = MultipartBody.Part.createFormData("profile_pic", ImageFile.getName(), ImageBody);
        uploadImageProfile.setProfilePic(iFile);
        ApiCall.getInstance().postaddProfileImage(activity, uploadImageProfile, true, this::onProfileImageGenrate);
    }

    private void onProfileImageGenrate(boolean isSuccess, Response<Object> objectResponse, Throwable throwable, ErrorType errorType, Object o) {
        if (!isSuccess)
            return;
        mainActivity.setNavigationHeader();
        updateUserInfo();
    }

}



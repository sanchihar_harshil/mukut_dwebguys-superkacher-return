package mms.dweb.superkacher.Fragment.ProductList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.Login.LoginActivity;
import mms.dweb.superkacher.Activity.Login.LoginUserActivity;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Activity.ProductInfo.ProductDetailsActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.ProductListAdapter;
import mms.dweb.superkacher.util.PopupUtils;
import mms.dweb.superkacher.util.PrefSetup;
import mms.dweb.utilities.Systemout;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.AddToCartRequest;
import mms.dweb.web_services.requestBean.AddWishlistProductRequest;
import mms.dweb.web_services.requestBean.ProductListRequest;
import mms.dweb.web_services.responseBean.CategoriesListResponse;
import mms.dweb.web_services.responseBean.ProductListResponse;
import mms.dweb.web_services.responseBean.ShortingProductResponse;
import retrofit2.Response;

public class ProductListFragment extends BaseFragment {

    @OnClick(R.id.ll_shortby_action)
    void click_ll_shortby_action() {
        showSortedByPopup();
    }

    @BindView(R.id.et_search)
    EditText et_search;
    @BindView(R.id.txt_sortType)
    TextView txt_sortType;
    @BindView(R.id.ll_null_data)
    LinearLayout ll_null_data;
    @BindView(R.id.rcv_product_list_plf)
    RecyclerView rcv_product_list_plf;
    //    @BindView(R.id.swipe_refrsh_layout)
//    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.progressBar_footer_plf)
    ProgressBar progressBar_footer_plf;

    private View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_product_list, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, true, true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_filter_main:
                postShortingProductApiCall();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //    private CategoriesListResponse.Responsedata categoriesListResponse;
    private MainActivity mainActivity;
    //    private int category;
    private int tempcategory;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }

        mainActivity.setHeader(0, getString(R.string.products));
        sortByList = Arrays.asList(getResources().getStringArray(R.array.sorted_array));
        Utilities.getInstance().setRecyclerViewUISpanCount(activity, rcv_product_list_plf, 1, DoActionType.Vertical);
        /*Bundle bundle = this.getArguments();
        if (bundle != null) {
            tempcategory = bundle.getInt("data");
            ApplicationContext.getInstance().setCategory(tempcategory);
            manageSortBy();
        }*/

        tempcategory = ApplicationContext.getInstance().getCategory();
        manageSortBy();

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                // user typed: start the timer
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        searchBy = editable.toString();
                        postProductApiCall(false);
                    }
                }, 600); // 600ms delay before the timer executes the „run“ method from TimerTask


            }
        });

        Bundle bundle = this.getArguments();
        if (bundle != null && bundle.getString("search") != null) {
            et_search.setText(bundle.getString("search"));
//            manageSortBy();
        }

    }

    private ProductListRequest productListRequest;
    private String searchBy;
    private Timer timer;
    private String searchTag;
    private int pageNo = 1;

    private void postProductApiCall(boolean showDialog) {
        if (productListRequest == null)
            productListRequest = new ProductListRequest();
        productListRequest.setPage(String.valueOf(pageNo));
        productListRequest.setCategory_id(ApplicationContext.getInstance().getCategory() == 0 ? "" : String.valueOf(ApplicationContext.getInstance().getCategory()));
        productListRequest.setMax_price("");
        productListRequest.setMin_price("");
        productListRequest.setSearch(searchBy);
        productListRequest.setOrderby(String.valueOf(mainActivity.getOrderBy()));
        productListRequest.setTag(searchTag);
//        productListRequest.setTag(String.valueOf(PrefSetup.getInstance().getFilterTag().getTermId()));
        ApiCall.getInstance().postProductApiCall(activity, productListRequest, showDialog, this::OnProductListResponse);
    }

    private ProductListResponse productListResponse;
    private List<ProductListResponse.Responsedata> productList;

    private void OnProductListResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {

        if (pageNo == 1) {
            if (productList != null)
                productList.clear();
            if (isSuccess) {
                productListResponse = (ProductListResponse) o;
                productList = productListResponse.getResponsedata();
            }
            manageData();
        } else if (isSuccess) {
            productListResponse = (ProductListResponse) o;
            productList.addAll(productListResponse.getResponsedata());
            adapter.setData(productList);
        } else if (pageNo > 1 && !isSuccess) {
            setActiveFooterLoader(false);
        }

        hideFooterProgressBar();
    }

    boolean activeFooterLoader = true;

    public boolean isActiveFooterLoader() {
        return activeFooterLoader;
    }

    public void setActiveFooterLoader(boolean activeFooterLoader) {
        this.activeFooterLoader = activeFooterLoader;
    }

    private void manageData() {
        if (productList == null || productList.size() == 0) {
            hideListView();
        } else {
            showListView();
            setAdapter();
        }
    }

    private ProductListAdapter adapter;

    private void setAdapter() {
        adapter = new ProductListAdapter(productList, activity, (object, position, type) -> {
            switch (type) {
                case Cart:
                    if (productList.get(position).getProductType().equals("variable")) {
                        startProductDetailsActivity(productList.get(position), position);
                    } else {
                        AddTocartApiCall(productList.get(position));
                    }

                    break;
                case Whishlist:
                    manageWishList(productList.get(position), position);
                    break;
                case Ok:
                    startProductDetailsActivity(productList.get(position), position);
                    break;
                case Scroll:
                    if (isActiveFooterLoader())
                        addNewProductInList();
                    break;
            }
        });
        rcv_product_list_plf.setAdapter(adapter);
    }

    private void addNewProductInList() {
        pageNo += pageNo;
        showFooterProgressBar();
        postProductApiCall(false);
    }

    private void manageWishList(ProductListResponse.Responsedata responsedata, int position) {
        if (responsedata.getWishlist() == 0) {
            postAddWishlistProductApiCall(responsedata, position);
        } else if (responsedata.getWishlist() == 1) {
            postDeleteWishlistProductApiCall(responsedata, position);
        }
    }

    private void postDeleteWishlistProductApiCall(ProductListResponse.Responsedata product, int position) {
        if (addWishlistProductRequest == null)
            addWishlistProductRequest = new AddWishlistProductRequest();
        addWishlistProductRequest.setProductId(String.valueOf(product.getId()));
        ApiCall.getInstance().postDeleteWishlistProductApiCall(activity, addWishlistProductRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                makeToast(product.getName() + " has been remove to your Wishlist.");
                product.setWishlist(0);
                productList.set(position, product);
                updateList();
            }
        });
    }

    private AddWishlistProductRequest addWishlistProductRequest;

    private void postAddWishlistProductApiCall(ProductListResponse.Responsedata product, int position) {
        if (addWishlistProductRequest == null)
            addWishlistProductRequest = new AddWishlistProductRequest();
        addWishlistProductRequest.setProductId(String.valueOf(product.getId()));
        ApiCall.getInstance().postAddWishlistProductApiCall(activity, addWishlistProductRequest, true, (isSuccess, response, t, errorType, o) -> {
            if (isSuccess) {
                makeToast(product.getName() + " has been added in your Wishlist.");
                product.setWishlist(1);
                productList.set(position, product);
                updateList();
            }
        });
    }

    private void AddTocartApiCall(ProductListResponse.Responsedata responsedata) {
        if (!mainActivity.isValidUser())
            return;
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        addToCartRequest.setProductId(responsedata.getId());
        addToCartRequest.setProductCount(String.valueOf(responsedata.getQuantity()));
        ApiCall.getInstance().postAddToCartApiCall(activity, addToCartRequest, true, this::OnAddToCartResponse);
    }

    private void OnAddToCartResponse(boolean isSuccess, Response<Object> objectResponse, Throwable throwable, ErrorType errorType, Object o) {
        Systemout.println(o);
        if (isSuccess)
            mainActivity.postCartApiCall();
    }

    private void updateList() {
        adapter.setData(productList);
    }

    private void showListView() {
        ll_null_data.setVisibility(View.GONE);
        rcv_product_list_plf.setVisibility(View.VISIBLE);
    }

    private void hideListView() {
        ll_null_data.setVisibility(View.VISIBLE);
        rcv_product_list_plf.setVisibility(View.GONE);
    }

    private List<String> sortByList;

    private void showSortedByPopup() {

        PopupUtils.getInstance().showSpinnerDialogNoCancelable(activity, getString(R.string.products), getString(R.string.select_sort_type),
                sortByList, (object, position) -> {
                    if (position == -1)
                        return;
                    mainActivity.setOrderBy(position);
                    manageSortBy();
                });
    }

    private void manageSortBy() {
        txt_sortType.setText(sortByList.get(mainActivity.getOrderBy() + 1));
        postProductApiCall(true);
    }

    private void startProductDetailsActivity(ProductListResponse.Responsedata responsedata, int position) {
        Intent intent = new Intent(mainActivity, ProductDetailsActivity.class);
        intent.putExtra("Product", responsedata);
        activity.startActivity(intent);
    }


    private void postShortingProductApiCall() {
        ApiCall.getInstance().postShortingProductApiCall(activity, true, this::OnShortingResponse);
    }

    private ShortingProductResponse shortingProductResponse;
    private List<ShortingProductResponse.Categories> categoriesList;
    private List<ShortingProductResponse.Tags> tagsList;
//    private ShortingProductResponse.Tags selectedTag;
//    private ShortingProductResponse.Categories selectedCat;

    private void OnShortingResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        initializeList();
        if (isSuccess) {
            shortingProductResponse = (ShortingProductResponse) o;
            categoriesList.addAll(shortingProductResponse.getCategories());
            tagsList.addAll(shortingProductResponse.getTags());
            manageFilterSpinnerData();
        }
    }

    List<String> catList;
    List<String> tagList;

    private void manageFilterSpinnerData() {
        if (catList == null)
            catList = new ArrayList<>();
        if (tagList == null)
            tagList = new ArrayList<>();
        catList.clear();
        tagList.clear();

        for (ShortingProductResponse.Categories categories : categoriesList) {
            catList.add(categories.getName());
        }
        for (ShortingProductResponse.Tags tags : tagsList) {
            tagList.add(tags.getName());
        }
        PopupUtils.getInstance().showTwoSpinnerDialogNoCancelable(activity, getString(R.string.filter),
                getString(R.string.select_sort_type), catList, tagList, (object, position, cat, tag) -> {
                    if (position == 1) {
                        searchTag = String.valueOf(tagsList.get(tag).getTermId());
                        ApplicationContext.getInstance().setCategory(categoriesList.get(cat).getTermId());
                        postProductApiCall(true);
                    } else if (position == 2) {
                        searchTag = "";
                        ApplicationContext.getInstance().setCategory(tempcategory);
                        postProductApiCall(true);
                    }
                });
    }


    private void initializeList() {
        if (categoriesList == null)
            categoriesList = new ArrayList<>();
        categoriesList.clear();
        ShortingProductResponse.Categories categories = new ShortingProductResponse.Categories();
        categories.setName("--Select Category--");
        categoriesList.add(categories);
        if (tagsList == null)
            tagsList = new ArrayList<>();
        tagsList.clear();
        ShortingProductResponse.Tags tags = new ShortingProductResponse.Tags();
        tags.setName("--Select Tag--");
        tagsList.add(tags);

    }

    @Override
    public void getNewProduct() {
        super.getNewProduct();
        manageSortBy();
    }

    @Override
    public void getShopNowProduct() {
        super.getShopNowProduct();
        searchBy = "";
        mainActivity.setOrderBy(0);
        pageNo = 1;
        ApplicationContext.getInstance().setCategory(0);
        manageSortBy();
    }

    public void showFooterProgressBar() {
        progressBar_footer_plf.setVisibility(View.VISIBLE);
    }

    public void hideFooterProgressBar() {
        progressBar_footer_plf.setVisibility(View.GONE);
    }

}

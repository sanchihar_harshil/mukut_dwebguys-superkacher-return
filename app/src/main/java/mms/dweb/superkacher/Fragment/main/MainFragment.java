package mms.dweb.superkacher.Fragment.main;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.Fragment.base.FragmentNames;
import mms.dweb.superkacher.R;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.responseBean.SliderResponse;
import retrofit2.Response;

public class MainFragment extends BaseFragment {

    @BindView(R.id.img_home_FM)
    ImageView img_home_FM;
    View fragmentView;

    @BindView(R.id.txt_footer_mf)
    TextView txt_footer_mf;
    @BindView(R.id.txt_heading_mf)
    TextView txt_heading_mf;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, true, false, true);
    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }
        mainActivity.setHeader(R.drawable.ic_launcher, "");
        postSliderApiCall();
        fragmentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("data", 0);
                ApplicationContext.getInstance().setCategory(0);
//                ApplicationContext.getInstance().setCategory(0);
                mainActivity.replaceFragment(FragmentNames.ProductListFragment, false, bundle, false, false);
            }
        });
    }

    private void postSliderApiCall() {
        ApiCall.getInstance().postSliderApiCall(activity, true, this::OnSliderResponse);
    }

    private SliderResponse sliderResponse;
    private List<SliderResponse.Responsedata> sliderList;

    private void OnSliderResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            sliderResponse = (SliderResponse) o;
            sliderList = sliderResponse.getResponsedata();

            manageData();
        }
    }

    private void manageData() {
        if (sliderList == null || sliderList.size() == 0) {
            ApplicationContext.getInstance().loadImage("https://superkacher.coma/wp-content/uploads/2018/03/Depositphotos_100253310_s-2015.jpg", img_home_FM, null, R.drawable.depositphotos);
            txt_heading_mf.setText("");
            txt_footer_mf.setText("");
        } else {
            ApplicationContext.getInstance().loadImage(sliderList.get(0).getImage(), img_home_FM, null, R.drawable.depositphotos);
            txt_heading_mf.setText(sliderList.get(0).getMainText());
            txt_footer_mf.setText(sliderList.get(0).getDescription());

        }
    }
}

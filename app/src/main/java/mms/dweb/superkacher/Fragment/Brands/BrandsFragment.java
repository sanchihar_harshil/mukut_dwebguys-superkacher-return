package mms.dweb.superkacher.Fragment.Brands;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.DoActionType;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.Fragment.base.FragmentNames;
import mms.dweb.superkacher.R;
import mms.dweb.superkacher.adapter.BrandsListAdapter;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.requestBean.BrandsListRequest;
import mms.dweb.web_services.responseBean.BrandsListResponse;
import retrofit2.Response;

public class BrandsFragment extends BaseFragment {

    @BindView(R.id.rcv_brands_bf)
    RecyclerView rcv_brands_bf;
    @BindView(R.id.ll_null_data)
    LinearLayout ll_null_data;
    @BindView(R.id.swipe_refrsh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_brands, container, false);
        ButterKnife.bind(this, fragmentView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                postBrandsListApiCall(false);
            }
        });

        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, true);

    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }
        mainActivity.setHeader(0, getString(R.string.brands));
        Utilities.getInstance().setRecyclerViewUISpanCount(activity, rcv_brands_bf, 2, DoActionType.Vertical);
        postBrandsListApiCall(true);
    }

    private BrandsListRequest brandsListRequest;

    private void postBrandsListApiCall(boolean showDialog) {
        brandsListRequest = new BrandsListRequest();
        brandsListRequest.setHome("0");
        ApiCall.getInstance().postBrandsListApiCall(activity, brandsListRequest, showDialog, this::OnBrandsResponse);
    }

    private BrandsListResponse brandsListResponse;
    private List<BrandsListResponse.Responsedata> brandList;

    private void OnBrandsResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        swipeRefreshLayout.setRefreshing(false);
        if (isSuccess) {
            brandsListResponse = (BrandsListResponse) o;
            brandList = brandsListResponse.getResponsedata();
            manageData();
        }
    }

    private void manageData() {
        if (brandList == null || brandList.size() == 0) {
            hideListView();
        } else {
            showListView();
            setAdapter();
        }
    }

    private BrandsListAdapter adapter;

    private void setAdapter() {
        adapter = new BrandsListAdapter(activity, brandList, (object, position) -> {
            Bundle bundle = new Bundle();
            int catId = 0;
            if(!brandList.get(position).getCategoryId().equals(""))
            catId = Float.valueOf(brandList.get(position).getCategoryId()).intValue();
            bundle.putInt("data", catId);
            if (brandList.get(position).getCategoryId().equals("")) {
                bundle.putString("search", brandList.get(position).getCategoryName());
            }
            ApplicationContext.getInstance().setCategory(catId);

            mainActivity.replaceFragment(FragmentNames.ProductListFragment, false, bundle, false, false);
        });
        rcv_brands_bf.setAdapter(adapter);
    }

    private void showListView() {
        ll_null_data.setVisibility(View.GONE);
    }

    private void hideListView() {
        ll_null_data.setVisibility(View.VISIBLE);
    }
}

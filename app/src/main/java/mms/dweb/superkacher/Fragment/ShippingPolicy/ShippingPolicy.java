package mms.dweb.superkacher.Fragment.ShippingPolicy;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.ErrorType;
import mms.dweb.superkacher.Activity.MainActivity;
import mms.dweb.superkacher.Fragment.base.BaseFragment;
import mms.dweb.superkacher.R;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.ApiCall;
import mms.dweb.web_services.responseBean.ShippingPolicyResponse;
import retrofit2.Response;

/**
 * Created by Admin on 6/29/2018.
 */

public class ShippingPolicy extends BaseFragment{

    @BindView(R.id.webview)
    WebView webview;
    View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.shipping_policy_fragment, container, false);
        ButterKnife.bind(this, fragmentView);

        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        WebSettings webSettings = webview.getSettings();
        webSettings.setTextSize(WebSettings.TextSize.SMALLER);
        Resources res= getResources();

        webSettings.setJavaScriptEnabled(true);



        return fragmentView;
    }




    @Override
    public void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utilities.getInstance().setMenu(menu, false, false, false);

    }

    MainActivity mainActivity;

    @Override
    public void initializeView() {
        super.initializeView();
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity) getActivity();
        }
        mainActivity.setHeader(0, getString(R.string.shipping));
        postShippingPolicyApicall(true);
    }

    private void postShippingPolicyApicall(boolean showDialog) {
        ApiCall.getInstance().postShippingPolicyApicall(activity, showDialog, this::OnShippingResponse);
    }

    private ShippingPolicyResponse shippingPolicyResponse;
    private void OnShippingResponse(boolean isSuccess, Response<Object> objectResponse, Throwable throwable, ErrorType errorType, Object o) {
        if(isSuccess){
            shippingPolicyResponse = (ShippingPolicyResponse) o;
            String response=shippingPolicyResponse.getResponsedata();
            Log.e("data", response);
            webview.loadData(response,"text/html","UTF-8");
        }
    }

}

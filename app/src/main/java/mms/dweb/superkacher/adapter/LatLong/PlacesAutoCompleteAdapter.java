package mms.dweb.superkacher.adapter.LatLong;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mms.dweb.utilities.Utilities;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
	// private ArrayList<String> resultList;
	JSONArray _objList;
	public PlacesAutoCompleteAdapter(Context context, int resource) {
		super(context, resource);
	}

	@Override
	public int getCount() {
		return _objList.length();
	}

	
	
	@Override
	public String getItem(int position) {
		try {
			return _objList.getJSONObject(position).getString("description");
		} catch (JSONException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public JSONObject getJSON(int pos) throws JSONException{
		return _objList.getJSONObject(pos);
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				if (results != null && results.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				if (constraint != null) {
					_objList = Utilities.getInstance().autocomplete(constraint.toString());
					filterResults.values = _objList;
					filterResults.count = _objList.length();
				}
				return filterResults;
			}
		};
		return filter;
	}
}

package mms.dweb.superkacher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import mms.dweb.base.OnListItemClickListener;
import mms.dweb.superkacher.R;
import mms.dweb.web_services.responseBean.CheckoutInfoResponse;

/**
 * Created by Mormukut singh R@J@W@T on 6/19/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class OrderInfoAdapter extends RecyclerView.Adapter<OrderInfoAdapter.MyHolder> {
    Context context;
    OnListItemClickListener listener;
    List<CheckoutInfoResponse.Cart> itemList;

    public OrderInfoAdapter(Context context, List<CheckoutInfoResponse.Cart> itemList, OnListItemClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.itemList = itemList;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_info_row, parent, false);
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        holder.txt_product_name.setText(itemList.get(position).getPostTitle());
        int quantity = itemList.get(position).getQuantity();
        double price = Double.parseDouble(itemList.get(position).getPrice());
        holder.txt_product_qty.setText(""+itemList.get(position).getQuantity());
//        holder.txt_product_value.setText("  x  " + itemList.get(position).getQuantity() + "   =  $" + (quantity*price));
        holder.txt_product_value.setText("$" + (quantity*price));

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setDataList(List<CheckoutInfoResponse.Cart> itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_product_value)
        TextView txt_product_value;

        @BindView(R.id.txt_product_qty)
        TextView txt_product_qty;
        @BindView(R.id.txt_product_name)
        TextView txt_product_name;

        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

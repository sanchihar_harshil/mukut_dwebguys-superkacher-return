package mms.dweb.superkacher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.OnListItemClickListener;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.web_services.responseBean.BrandsListResponse;
import mms.dweb.web_services.responseBean.MyOrdersResponse;

/**
 * Created by Mormukut singh R@J@W@T on 6/22/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.MyHolder> {

    Context context;
    OnListItemClickListener listener;
    List<MyOrdersResponse.Responsedata> itemList;

    public MyOrdersAdapter(Context context, List<MyOrdersResponse.Responsedata> itemList, OnListItemClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.itemList = itemList;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myorders_list_row, parent, false);
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        holder.txt_order_moa.setText(String.valueOf(itemList.get(position).getId()));
        holder.txt_date_moa.setText(itemList.get(position).getPostDate());
        holder.txt_status_moa.setText(itemList.get(position).getStatus().replace("wc-",""));
        holder.txt_quantity_moa.setText(String.valueOf(itemList.get(position).getProductCount()));
        holder.txt_total_moa.setText("$" + itemList.get(position).getTotal());

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setData(List<MyOrdersResponse.Responsedata> categoriesList) {
        this.itemList = categoriesList;
        notifyDataSetChanged();
    }


    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_order_moa)
        TextView txt_order_moa;
        @BindView(R.id.txt_date_moa)
        TextView txt_date_moa;
        @BindView(R.id.txt_status_moa)
        TextView txt_status_moa;
        @BindView(R.id.txt_quantity_moa)
        TextView txt_quantity_moa;
        @BindView(R.id.txt_total_moa)
        TextView txt_total_moa;


        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(itemList.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }
    }

}

package mms.dweb.superkacher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.DoActionType;
import mms.dweb.base.OnListItemClickListener;
import mms.dweb.base.OnListItemClickWithTypeListener;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.responseBean.CartInfoResponse;
import mms.dweb.web_services.responseBean.ProductListResponse;

/**
 * Created by Mormukut singh R@J@W@T on 6/14/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class CartProductListAdapter extends RecyclerView.Adapter<CartProductListAdapter.MyHolder> {
    List<CartInfoResponse.Cart> itemList;
    Context context;
    OnListItemClickWithTypeListener listener;

    public CartProductListAdapter(List<CartInfoResponse.Cart> itemList, Context context, OnListItemClickWithTypeListener listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_row, parent, false);
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        Utilities.getInstance().setHTMLText(holder.txt_description_pr, itemList.get(position).getShortDescription());
        if (itemList.get(position).getOption() == null) {
            holder.txt_heading_pr.setText(itemList.get(position).getName());
        } else {
            holder.txt_heading_pr.setText(itemList.get(position).getName()
                    + "\n" + itemList.get(position).getOption());
        }
        holder.txt_add_pr.setText(context.getString(R.string.remove));
        holder.txt_price_pr.setText("$" + itemList.get(position).getPrice());
        holder.txt_quantity_pr.setText(String.valueOf(itemList.get(position).getQuantity()));
        ApplicationContext.getInstance().loadImage(itemList.get(position).getImages().get(0).getSrc(), holder.img_product_pr, holder.progressBar_img_pr, R.drawable.depositphotos);
        holder.img_wishlist_pr.setImageResource(itemList.get(position).getWishlist().equals("1") ? R.drawable.ic_favorite : R.drawable.ic_favorite_border_prime_24dp);
        holder.img_wishlist_pr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(itemList.get(position), position, DoActionType.Whishlist);
            }
        });
        holder.txt_add_pr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(itemList.get(position), position, DoActionType.Remove);
            }
        });
        holder.img_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: 6/15/2018
                if (itemList.get(position).getQuantity() == 1)
                    return;
                updateQuantity(itemList.get(position).getQuantity() - 1, position, DoActionType.SUB);
            }
        });

        holder.img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateQuantity(itemList.get(position).getQuantity() + 1, position, DoActionType.SUB);
            }
        });


    }

    private void updateQuantity(int quantity, int position, DoActionType type) {
        CartInfoResponse.Cart model = itemList.get(position);
        model.setQuantity(quantity);
        notifyItemChanged(position);
        listener.onItemClick(itemList.get(position), position, type);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setData(List<CartInfoResponse.Cart> productList) {
        this.itemList = productList;
        notifyDataSetChanged();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_heading_pr)
        TextView txt_heading_pr;
        @BindView(R.id.txt_price_pr)
        TextView txt_price_pr;
        @BindView(R.id.txt_description_pr)
        TextView txt_description_pr;
        @BindView(R.id.img_product_pr)
        ImageView img_product_pr;
        @BindView(R.id.progressBar_img_pr)
        ProgressBar progressBar_img_pr;
        @BindView(R.id.txt_add_pr)
        TextView txt_add_pr;
        @BindView(R.id.img_add)
        ImageView img_add;
        @BindView(R.id.img_sub)
        ImageView img_sub;
        @BindView(R.id.img_wishlist_pr)
        ImageView img_wishlist_pr;
        @BindView(R.id.txt_quantity_pr)
        TextView txt_quantity_pr;

        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

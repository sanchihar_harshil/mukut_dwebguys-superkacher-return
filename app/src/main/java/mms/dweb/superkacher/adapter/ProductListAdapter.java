package mms.dweb.superkacher.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.DoActionType;
import mms.dweb.base.OnListItemClickListener;
import mms.dweb.base.OnListItemClickWithTypeListener;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.utilities.Utilities;
import mms.dweb.web_services.responseBean.ProductListResponse;

/**
 * Created by Mormukut singh R@J@W@T on 6/14/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyHolder> {
    List<ProductListResponse.Responsedata> itemList;
    Context context;
    OnListItemClickWithTypeListener listener;

    public ProductListAdapter(List<ProductListResponse.Responsedata> itemList, Context context, OnListItemClickWithTypeListener listener) {
        this.itemList = itemList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_row, parent, false);
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        Utilities.getInstance().setHTMLText(holder.txt_description_pr, itemList.get(position).getShort_description());
        holder.txt_heading_pr.setText(itemList.get(position).getName());
        holder.txt_price_pr.setText("$" + itemList.get(position).getPrice());
        holder.txt_quantity_pr.setText(String.valueOf(itemList.get(position).getQuantity()));
        holder.img_wishlist_pr.setImageResource(itemList.get(position).getWishlist() == 1 ? R.drawable.ic_favorite : R.drawable.ic_favorite_border_prime_24dp);
        ApplicationContext.getInstance().loadImage(itemList.get(position).getImage(), holder.img_product_pr, holder.progressBar_img_pr, R.drawable.depositphotos);

        if (itemList.get(position).getProductType().equals("variable")) {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT); // or set height to any fixed value you want
            holder.txt_add_pr.setText(context.getString(R.string.view_product));
            holder.ll_quantityview_pr.setVisibility(View.GONE);
            holder.spacer.setVisibility(View.GONE);
            holder.ll_add_to_cart_pla.setLayoutParams(lp);
//            holder.txt_add_pr.setLayoutParams(lp);
            holder.ll_add_to_cart_pla.setGravity(Gravity.NO_GRAVITY);
//            holder.ll_add_to_cart_pla.setPadding(0,0,240,0);
//            holder.txt_add_pr.setPadding(0,0,10,0);

        } else {
            holder.ll_quantityview_pr.setVisibility(View.VISIBLE);
            holder.txt_add_pr.setText(context.getString(R.string.add_to_cart));
        }


        holder.txt_add_pr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(itemList.get(position), position, DoActionType.Cart);
            }
        });
        holder.img_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: 6/15/2018
                if (itemList.get(position).getQuantity() == 1)
                    return;
                updateQuantity(itemList.get(position).getQuantity() - 1, position);
            }
        });

        holder.img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateQuantity(itemList.get(position).getQuantity() + 1, position);
            }
        });

        holder.img_wishlist_pr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(itemList.get(position), position, DoActionType.Whishlist);
            }
        });

        if (position == (itemList.size() - 1))
            listener.onItemClick(itemList.get(position), position, DoActionType.Scroll);
    }

    private void updateQuantity(int quantity, int position) {
        ProductListResponse.Responsedata model = itemList.get(position);
        model.setQuantity(quantity);
        notifyItemChanged(position);

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setData(List<ProductListResponse.Responsedata> productList) {
        this.itemList = productList;
        notifyDataSetChanged();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_heading_pr)
        TextView txt_heading_pr;
        @BindView(R.id.txt_price_pr)
        TextView txt_price_pr;
        @BindView(R.id.txt_description_pr)
        TextView txt_description_pr;
        @BindView(R.id.img_product_pr)
        ImageView img_product_pr;
        @BindView(R.id.progressBar_img_pr)
        ProgressBar progressBar_img_pr;
        @BindView(R.id.txt_add_pr)
        TextView txt_add_pr;
        @BindView(R.id.img_add)
        ImageView img_add;
        @BindView(R.id.img_sub)
        ImageView img_sub;
        @BindView(R.id.txt_quantity_pr)
        TextView txt_quantity_pr;
        @BindView(R.id.img_wishlist_pr)
        ImageView img_wishlist_pr;
        @BindView(R.id.quantityLayout)
        LinearLayout ll_quantityview_pr;
        @BindView(R.id.spacer)
        Space spacer;
        @BindView(R.id.ll_add_to_cart_pla)
        LinearLayout ll_add_to_cart_pla;

        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(itemList.get(getAdapterPosition()),
                            getAdapterPosition(), DoActionType.Ok);
                }
            });
        }
    }
}

package mms.dweb.superkacher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mms.dweb.base.OnListItemClickListener;
import mms.dweb.superkacher.ApplicationContext;
import mms.dweb.superkacher.R;
import mms.dweb.web_services.responseBean.BrandsListResponse;
import mms.dweb.web_services.responseBean.HolidaysListResponse;

/**
 * Created by Mormukut singh R@J@W@T on 6/13/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : superkacher
 */

public class HolidaysListAdapter extends RecyclerView.Adapter<HolidaysListAdapter.MyHolder> {
    Context context;
    OnListItemClickListener listener;
    List<HolidaysListResponse.Responsedata> itemList;

    public HolidaysListAdapter(Context context, List<HolidaysListResponse.Responsedata> itemList, OnListItemClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.itemList = itemList;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.categories_row, parent, false);
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HolidaysListAdapter.MyHolder holder, int position) {
        ApplicationContext.getInstance().loadImage(itemList.get(position).getCategoryImage(), holder.img_categories_CLA, null, R.drawable.no_image);
        holder.txt_categories_name_cla.setText(itemList.get(position).getCategoryName());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setData(List<HolidaysListResponse.Responsedata> categoriesList) {
        this.itemList = categoriesList;
        notifyDataSetChanged();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_categories_CLA)
        ImageView img_categories_CLA;
        @BindView(R.id.txt_categories_name_cla)
        TextView txt_categories_name_cla;

        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(itemList.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }
    }
}
